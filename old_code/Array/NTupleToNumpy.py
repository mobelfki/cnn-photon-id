#!/usr/bin/env python3

# import ROOT
import uproot as pr
# uproot updated to version 4 this year
# import uproot4 as pr
import pandas as pd
import numpy as np
from argparse import ArgumentParser
import sys
import os
from os import mkdir
from os.path import join

def getArgs():
    """
    Get arguments from command line.
    """
    args = ArgumentParser(description="Argumetns for NTupleToNumpy for ANN")
    args.add_argument('-s', '--samples', action='store', required=True, help='Samples name to process .txt splitted by ,')
    args.add_argument('-i', '--inputdir', action='store', default='/eos/user/m/mobelfki/MLPhotonNTuple/', help='Input Directory')
    args.add_argument('-f', '--features', action='store', required=True, help='Features list')
    args.add_argument('-o', '--outdir', action='store', default='output', help='Output Directory')
    args.add_argument('-l', '--toline', action='store', default=False, help= 'apply vector to line')
    return args.parse_args()

class NTupleToNumpy:
    """
    Produce numpy arrays form NTuple class
    The output is saved in Array/output directory
    Doesn't take any inputs
    """

    def __init__(self, name, line):
        self.name = name;
        self.line = line;

    def loadTree(self, path, tree):
        """
        Loads Tree data into NumpyArray

        # Args::
            path: (str), path to .root file

            tree: (str), name of the tree to read

        # Returns:: None
        """
        with pr.open(path) as rootfile:
            # print(rootfile.classnames())
            Tree = rootfile[tree];
            df = Tree.pandas.df(self.Features, flatten=False)
            data = df.to_numpy()
            self.data = data[:1000000,:]
        # self.Tree = pr.open(path)[tree];
        # self.Tree.show()

    def loadFeatures(self, txt):
        self.Features = txt;

    def Execute(self):
        """
        Rearranges data into different containers.
        X1: contains cells from layer 1 Calo
        X2: contains cells from layer 2 Calo
        X3: contains cells from layer 3 Calo
        Y: contains labels of the events
        """
        # -- Load TTree to DataFrame using
        # df = self.Tree.pandas.df(self.Features, flatten=False)

        # # NOTE: pandas.df() is an special method in uproot3
        # df = self.Tree.pandas.df(self.Features, flatten=False)
        # NOTE: the following works for uproot4
        # df = self.Tree.arrays(filter_name=self.Features,library='pd')

        # df = self.Tree.array(library='pd').df(self.Features, flatten=False)
        # print(df.columns)
        # print(df.head())
        # print()
        # # NOTE: I think that we should stay with the DataFrame, at least for the SSvars
        # data = df.to_numpy()
        # data = data[:1000000,:]

        # -- Set containers for different sizes
        X1 = []
        X2 = []
        X3 = []
        for i in range(0,self.data.shape[0]):
        # for i in range(0,data.shape[0]):
            x1 = []
            x2 = []
            x3 = []

            for j in range(0,self.data.shape[1]-1):
            # for j in range(0,data.shape[1]-1):
                # aa = data[i]
                aa = self.data[i]
                aa = np.array(aa)
                # --Select the first 9 atributes from features_file
                aa = aa[:-48]
                if j < 3:
                    x1.append(aa[j])
                if j < 6 and j >=3:
                    x2.append(aa[j])
                if j < 9 and j >=6:
                    x3.append(aa[j])

            x1 = np.array(x1)
            X1.append(x1)
            x2 = np.array(x2)
            X2.append(x2)
            x3 = np.array(x3)
            X3.append(x3)

        self.X1 = np.array(X1)
        self.X2 = np.array(X2)
        self.X3 = np.array(X3)
        self.Y = self.data[:,-48:]
        # self.Y = data[:,-48:]

    def GetOutName_Stg(self, sample):
        """
        sets strategy name for sample to output
        """
        #NOTE: TODO make this less specific, right now it's too related to IsoTight.
        if "Strategy_IsoTight_1" in sample:
                return 'Stg_1';
        if "Strategy_IsoTight_2" in sample:
                return 'Stg_2';
        if "Strategy_IsoTight_3" in sample:
                return 'Stg_3';
        if "Strategy_IsoTight_0" in sample:
                return 'Stg_0';

    def GetOutName_Selection(self, sample):
        """
        sets selection name for sample to output.
        It's use for labeling
        """
        if "DiJet" in sample:
                return 'DiJet_IsoTight';
        if "GammaJet" in sample:
                return 'GammaJet_IsoTight';

    def Save_to_npy(self,outdir,treename,sample,sample_num):
        """
        helper method to store data in NPY format into default output.
        Run this method after Execute otherwise arrays will be empty.

        # Parameters:
            outdir: (string), output dir name or path

            treename: (string), name of the TTree

            sample: the desired sample

            sample_num: sample number

        # Return:
            None
        """
        # # --Create dir outdir/stgname
        stgname = self.GetOutName_Stg(sample)
        # --Create dir outdir/stgname/treename
        fulldir = f'{outdir}/{stgname}/{treename}'
        try:
            os.makedirs(fulldir)
            print(f'new dir {fulldir}')
        except FileExistsError:
            print(f'dir {fulldir} already exists')

        selecname = self.GetOutName_Selection(sample)
        # --Def helper dict
        data = {'_X1':self.X1,'_X2':self.X2,'_X3':self.X3,'_Y':self.Y}
        for k,array in data.items():
            ofn = selecname+'.'+treename+'.'+stgname+'_'+str(sample_num)+k+'.npy'
            path = join(fulldir,ofn)
            # print(path)
            np.save(file=path,arr=array)

def main():
    """
    The main function of NTupleToNumpy
    """
    args=getArgs()
    # -- Read sample file names
    with open(args.samples,'r') as samples_file:
        samples_name = samples_file.read().splitlines();
    # -- Read features file names
    features_file = args.features
    ft = []
    with open(features_file,'r') as features_file:
        ft = features_file.read().splitlines();
    print(ft)
    # -- As default create output dir
    try:
        mkdir(args.outdir)
        print(f'--> output dir {args.outdir} created')
    # -- if exist pass
    except: pass

    trees = {'Tree','Tree_Train','Tree_Val','Tree_Test'}
    # trees = {'Tree_Train'}
    for tree in trees:
        print(tree)
        i = 0
        for sample in samples_name:
            i+=1
            print(f'--> reading sample: {sample}')
            NTuple2Numpy = NTupleToNumpy(sample, args.toline);
            NTuple2Numpy.loadFeatures(ft);
            NTuple2Numpy.loadTree(args.inputdir+'/'+sample,tree);
            NTuple2Numpy.Execute();
            NTuple2Numpy.Save_to_npy(args.outdir,tree,sample,sample_num=i)
            del NTuple2Numpy

if __name__ == '__main__':
    main()


#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
import math
import os
from os.path import join
from datetime import date
from argparse import ArgumentParser
from packaging import version
# -- cnn-photon-id libraries
from Settings.script_setup import Script
from Models.models import CNNClassifier, CNNHybridClassifier
from Models.calls import roc_on_epoch, auroc, multiIDG
from DataProcessing.DataProcessing import Data, CNNData
# -- TensorFlow libraries
import tensorflow as tf
from tensorflow.keras import optimizers as opt
from tensorflow.keras.models import model_from_json
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, LearningRateScheduler, ReduceLROnPlateau
from tensorflow.keras.utils import plot_model
from tensorflow.keras.preprocessing.image import ImageDataGenerator as IDG
from tensorflow.keras import metrics
# -- SKlearn libraries
from sklearn.utils import shuffle
from sklearn.utils import class_weight as cls_W
# ******* THE FOLLOWING ARE NOT IN USE *******
# ------- ?what was the reason not to use them? -------

# from tensorflow.keras.callbacks import Callback
# from sklearn.metrics import confusion_matrix, f1_score, precision_score, recall_score
# from sklearn.metrics import roc_curve, roc_auc_score, auc
# from sklearn.model_selection import train_test_split
# from scipy import interp
# import itertools
# from itertools import cycle
# from Models.models import Classifier, MClassifier, SSClassifier
# ********************************************

# np.set_printoptions(threshold=sys.maxsize)

# seed = 215
# np.random.seed(seed)
# --Load data from files to CNNData objects
# data = CNNData(1)
# nohealthy = CNNData(1)

if version.parse(tf.__version__) > version.parse('1.15'):
    tf.compat.v1.disable_v2_behavior()
    print('runing TF v1 compatible')

def lr_decay(epoch, lr):
    decay_rate = 0.5
    decay_step = 50
    if epoch % decay_step == 0 and epoch:
        return lr * decay_rate
    return lr

class Train(Script):
# class Train(object):

    seed = 215
    np.random.seed(seed)

    def __init__(self,new_dir):
        super().__init__(new_dir)

    # def __init__(self,new_dir=False):
        # self.new_dir = new_dir
        # self.args = self.getArgs()
        # self._set_params
        # self._set_tag
        # self._set_out_subdirs
        # # --Load data from files to CNNData objects
        # self.data = CNNData(
                            # Stg=self.Stg,
                            # Dir=self.args.input,
                            # Usage=self.usage
                            # )

    # def getArgs(self):
        # """
        # Get arguments from command line.
        # """
        # dflt={
              # 'input':None,
              # 'output':'/pbs/home/m/mbelfkir/MLPhotonID/Train/Output/',
              # 'nepochs':10,
              # }

        # # --Init parser
        # args = ArgumentParser(description="Arguments CNN Training")
        # # default action is 'store'

        # # -- I/O parameters
        # args.add_argument('-I',
                          # '--input',
                          # default=None,
                          # help='Intput Directory')
        # args.add_argument('-O',
                          # '--output',
                          # default=dflt['output'],
                          # help='Output Directory')
        # # -- Data parameters
        # args.add_argument("-S",
                          # "--strategy",
                          # default=1,
                          # help="Strategy to load data, and deal with unhealthy clusters.")
        # # -- CNN Layers parameters
        # args.add_argument('-o',
                          # '--noutput',
                          # default=1,
                          # help='Number of outputs from the model nclass')
        # args.add_argument('-d',
                          # '--dropout_rate',
                          # default=0.08,
                          # help='Dropout rate')
        # args.add_argument('-nCK',
                          # '--nConvKernels',
                          # default=256,
                          # help='Number of neurons/kernels in each Conv layer')
        # args.add_argument('-nDK',
                          # '--nDenseKernels',
                          # default=128,
                          # help='Number of neurons in each Dense layer')
        # args.add_argument('-nCL',
                          # '--nConvLayers',
                          # default=2,
                          # help='Number of Conv layers')
        # args.add_argument('-nDL',
                          # '--nDenseLayers',
                          # default=2,
                          # help='Number of Dense layers')
        # # -- Training parameters
        # args.add_argument('-nEp',
                          # '--nepochs',
                          # default=10,
                          # help='Number of epochs')
        # args.add_argument('-l',
                          # '--learning_rate',
                          # default=0.0001,
                          # help='Learning rate')
        # args.add_argument('-b',
                          # '--batch_size',
                          # default=32,
                          # help='Batch size')
        # args.add_argument('-G',
                          # '--Gen',
                          # default=False,
                          # help='Train the model on generator')
        # args.add_argument("-U",
                          # "--trainingType",
                          # default='OFFLINE',
                          # help="type of training (usage) to follow, either 'offline' or 'HLT'")
        # args.add_argument('-KT',
                          # '--isTuner',
                          # default=False,
                          # help='bool, if training is suposed to use keras tuner'
                          # )
        # return args.parse_args()

    # @property
    # def _set_out_subdirs(self):
        # # --Set global model out dir
        # self.model_dir = f'{self.OutDir}/{self.usage}/stg_{self.Stg}/{self.tag}'
        # # --Set sub dirs
        # save_model_path = f'{self.model_dir}/saved_models'
        # historypath = f'{self.model_dir}/history'
        # # If directories do not exist make them
        # if self.new_dir:
            # print('--> Creating Output Folders')
            # try:
                # os.makedirs(self.model_dir)
                # print(f'--> model main dir created it is: {self.model_dir}')
                # os.makedirs(f'{save_model_path}/checkpoints')
                # os.mkdir(historypath)
            # except FileExistsError as e:
                # print(f'!! {e}:: Failed to create new directories')

    # @property
    # def _set_params(self):
        # # -- I/O parameters
        # self.OutDir = str(self.args.output)
        # # -- Data parameters
        # self.usage = str(self.args.trainingType)
        # self.Stg = int(self.args.strategy)
        # # -- CNN Layers parameters
        # self.dr = float(self.args.dropout_rate)
        # self.nCL = int(self.args.nConvLayers)        # n Conv layers
        # self.nDL = int(self.args.nDenseLayers)       # n Dense layers
        # self.nCK = int(self.args.nConvKernels)       # n Conv kernels
        # self.nDK = int(self.args.nDenseKernels)      # n Dense kernels
        # self.Nout = int(self.args.noutput)           # OFFLINE valid only
        # # -- Training parameters
        # self.isGen = bool(self.args.Gen)
        # self.nEp = int(self.args.nepochs)
        # self.bt = int(self.args.batch_size)
        # self.lr = float(self.args.learning_rate)
        # self.KT = bool(self.args.isTuner)
        # # -- Assert inputs
        # assert (self.usage in ['OFFLINE','HLT']), '****> please select type "OFFLINE" or "HLT"'
        # assert (self.Stg in [0,1,2,3]), '****> please choose an strategy from [0,1,2,3]'

    # @property
    # def _set_tag(self):
        # """Creates tag for train model"""
        # if self.usage == 'OFFLINE':
            # if not self.isGen:
                # first='Stg{stg}_{u}CNN_Nep_{n}_CNNn_{nCK}_DNNn_{nDK}'.format(
                            # stg=self.Stg, u=self.usage, n=self.nEp, nCK=self.nCK, nDK=self.nDK
                            # )
                # second='_lr_{lr}_dr_{dr}_bt_{bt}'.format(
                            # lr=self.lr, dr=self.dr, bt=self.bt
                            # )
                # self.tag = first+second
            # else:
                # self.tag = first+second+'_Generator'

        # if self.usage == 'HLT':
            # traindate = str(date.today())
            # print(f'today is {traindate}')
            # if not self.KT:
                # arch_tag='{nck}x{ncl}CL_{ndk}x{ndl}DL_{dr:3.2f}dr'.format(
                            # nck=self.nCK, ncl=self.nCL, ndk=self.nDK, ndl=self.nDL, dr=self.dr
                            # )
                # train_tag=f'{self.nEp}Ep_{self.lr:3.1e}lr'
                # self.tag = f'{traindate}_{arch_tag}_{train_tag}'
            # else: pass

    def trainHLTCNN(self):
        """
        Loads model, compiles model, trains model

        # Args::

        # Returns::
        """
        # -- Appending cluster img to layer containers
        X2 = np.append(self.data.Img_Lr2_Train, self.data.Img_Lr2_Val, axis=0)
        Y  = np.append(self.data.Y_Train, self.data.Y_Val, axis=0)
        # -- Mask Label values to int
        Y  = np.multiply(Y, 1)
        X2, Y = shuffle(X2, Y, random_state=self.seed)
        X2 = X2.reshape(X2.shape[0], 11, 7, 1)
        Y = Y.reshape(Y.shape[0],)
        # print(f'img shape X2: {X2.shape}')
        # print(f'img shape Y: {Y.shape}')

        # -- Get class weights to balance training
        class_weights = cls_W.compute_class_weight(
                                    'balanced',
                                    np.unique(Y),
                                    Y)
        class_weights = dict(enumerate(class_weights))
        print(f'--> the class weights: {class_weights}')
        # --Init model
        # TODO: add args options
        # TODO: add HyperCNN option
        if not self.KT:
            clf = CNNClassifier(
                        'HLTCNN_Classifier',
                        N_CNN_neurons=self.nCK,
                        N_DNN_neurons=self.nDK,
                        dropout_rate=self.dr
                        ).model
        else:
            # hp_clf = Hyper_HLT_CNN_Clf()
            pass

        # -- Set compiler
        adm = opt.Adam(lr=self.lr)
        tfAUC = metrics.AUC(name='auc')
        # tfAUC = metrics.AUC(name='auc', label_weights=class_weights)
        clf.compile(
                loss=['binary_crossentropy'],
                optimizer=adm,
                # metrics=['binary_accuracy']
                # metrics=['binary_accuracy',auroc]
                # metrics=['binary_accuracy','roc']
                metrics=['binary_accuracy',tfAUC]
                )

        print(f'--> Model compiled: \n {clf.summary()}')

        # -- Set training
        es1 = EarlyStopping(
                monitor='val_loss',
                patience=5,
                verbose=1,
                mode='min',
                min_delta=0.0001
                )
        # TODO: check how this ROC is computed
        # roc = roc_on_epoch(self.usage)
        lrScheduler = LearningRateScheduler(lr_decay, verbose=1)

        # -- Set training checkpoints
        checkpoint_path = f'{self.model_dir}/saved_models/checkpoints'
        save_wgt_on_epoch = ModelCheckpoint(
                join(checkpoint_path,'Model_Ep_{epoch:02d}-{val_loss:.2f}.weights'),
                monitor='loss',
                mode='min',
                verbose=1,
                save_best_only=True
                )

        if self.isGen:
            print('*** Training on Generator NOT IMPLEMENTED ***')
        else:
            callbacks_list = [save_wgt_on_epoch]
            # callbacks_list = [save_wgt_on_epoch,tfAUC]
            # callbacks_list = [save_wgt_on_epoch,roc]

            # -- Fit model get history of training
            print(f'--> Begining Training')
            history = clf.fit(
                              x=X2,
                              y=Y,
                              epochs=self.nEp,
                              batch_size=self.bt,
                              shuffle=True,
                              validation_split=0.33,
                              callbacks=callbacks_list,
                              class_weight=class_weights
                              )
            print(f' --> history of training: {history.history.keys()}')

        return history, clf

    def trainHybridCNN(self):
        clf = CNNHybridClassifier('HybridCNN_Classifier',
                                  self.nCK,
                                  self.nDK,
                                  self.Nout,
                                  self.dr
                                  ).model

        adm = opt.Adam(lr=self.lr)
        clf.compile(loss=['binary_crossentropy'],
                    optimizer=adm,
                    metrics=['binary_accuracy'])
        print(clf.summary())

        # -- Appending cluster img to layer containers
        X1 = np.append(self.data.Img_Lr1_Train, self.data.Img_Lr1_Val, axis=0)
        X2 = np.append(self.data.Img_Lr2_Train, self.data.Img_Lr2_Val, axis=0)
        X3 = np.append(self.data.Img_Lr3_Train, self.data.Img_Lr3_Val, axis=0)
        Y  = np.append(self.data.Y_Train, self.data.Y_Val, axis=0)
        Y  = np.multiply(Y, 1)
        X1, X2, X3, Y = shuffle(X1, X2, X3, Y, random_state=self.seed)

        """
        NH_X1 = np.append(nohealthy.Img_Lr1_Train, nohealthy.Img_Lr1_Val, axis=0)[:Y.shape[0],:]
        NH_X2 = np.append(nohealthy.Img_Lr2_Train, nohealthy.Img_Lr2_Val, axis=0)[:Y.shape[0],:]
        NH_X3 = np.append(nohealthy.Img_Lr3_Train, nohealthy.Img_Lr3_Val, axis=0)[:Y.shape[0],:]
        NH_Y  = np.append(nohealthy.Y_Train, nohealthy.Y_Val, axis=0)[:Y.shape[0]]
        NH_Y  = np.multiply(NH_Y, 1)
        NH_X1, NH_X2, NH_X3, NH_Y = shuffle(NH_X1, NH_X2, NH_X3, NH_Y, random_state=seed)
        f = int(Y.shape[0]*.5)
        NH_X1 = NH_X1[:f,:];
        NH_X2 = NH_X2[:f,:];
        NH_X3 = NH_X3[:f,:];
        NH_Y = NH_Y[:f];
        X1 = np.append(X1, NH_X1, axis=0)
        X2 = np.append(X2, NH_X2, axis=0)
        X3 = np.append(X3, NH_X3, axis=0)
        Y  = np.append(Y, NH_Y, axis=0)
        """

        # NOTE: why there are 2 shuffles?
        X1, X2, X3, Y = shuffle(X1, X2, X3, Y, random_state=self.seed)

        es1 = EarlyStopping(monitor='val_loss', patience=5,
                            verbose=1, mode='min', min_delta=0.0001)
        checkpoint_path = f'{self.model_dir}/saved_models/checkpoints'
        save_wgt_on_epoch = ModelCheckpoint(
                        join(checkpoint_path,'Model_Ep_{epoch:02d}-{val_loss:.2f}.weights'),
                        # self.OutDir+'/saved_models/'+'Model_'+self.tag+'_Ep_{epoch:02d}.weights',
                        monitor='loss',
                        mode='min',
                        verbose=1,
                        save_best_only=True)
        roc = roc_on_epoch()
        lrScheduler = LearningRateScheduler(lr_decay, verbose=1)

        class_weights = cls_W.compute_class_weight('balanced', np.unique(Y), Y)

        print(f'-> the class weights: {class_weights}')

        print(f'img shape X1: {X1.shape}')
        print(f'img shape X2: {X2.shape}')
        print(f'img shape X3: {X3.shape}')
        print(f'img shape Y: {Y.shape}')
        # X1 = X1.reshape(X1.shape[0], 2, 56, 1)
        # X2 = X2.reshape(X2.shape[0], 11, 7, 1)
        # X3 = X3.reshape(X3.shape[0], 11, 4, 1)

        X1 = X1.reshape(X1.shape[0], 2, 56, 1)
        X2 = X2.reshape(X2.shape[0], 11, 7, 1)
        X3 = X3.reshape(X3.shape[0], 11, 4, 1)
        print()
        print('after reshaping')
        print(f'img shape X1: {X1.shape}')
        print(f'img shape X2: {X2.shape}')
        print(f'img shape X3: {X3.shape}')
        print(f'img shape Y: {Y.shape}')

        # history = "local"

        if self.isGen:
            print('Training on Generator ...')
            datagen = IDG(rotation_range=180, horizontal_flip=True, validation_split=0.33, featurewise_center=True, featurewise_std_normalization=True)
            train_itr = multiIDG(generator=datagen, X1=X1, X2=X2, X3=X3, Y=Y,
                                 batch_size=self.bt, img_h=128, img_W=128, subset='training')
                                 # batch_size=batch_size, img_h=128, img_W=128, subset='training')
            val_itr = multiIDG(generator=datagen, X1=X1, X2=X2, X3=X3, Y=Y,
                               batch_size=self.bt, img_h=128, img_W=128, subset='validation')
                               # batch_size=batch_size, img_h=128, img_W=128, subset='validation')

            callbacks_list = [save_wgt_on_epoch]

            history = clf.fit_generator(
                           train_itr,
                           validation_data=val_itr,
                           epochs=self.nEp,
                           shuffle=False,
                           steps_per_epoch=(Y.shape[0]*.66)/self.bt,
                           validation_steps=(Y.shape[0]*.33)/self.bt,
                           class_weight=class_weights,
                           callbacks=callbacks_list
                           )
            return history, clf
        else:
            callbacks_list = [save_wgt_on_epoch]

            history = clf.fit(
                            x={'Input1': X1, 'Input2': X2, 'Input3': X3},
                            y=Y,
                            epochs=self.nEp,
                            batch_size=self.bt,
                            shuffle=True,
                            validation_split=0.33,
                            callbacks=callbacks_list,
                            class_weight=class_weights
                            )

        return history, clf

    def save_model_data(self, model, model_history):
        """
        Saves model's weights, history and itself in OutDir

        # Args::

            model: the trained model

            history: history of the training of the model

        # Returns:: None
        """
        # TODO: save in history the summary

        modelpath = f'{self.model_dir}/saved_models'
        historypath = f'{self.model_dir}/history'

        model_json = model.to_json()
        fname= f'model.json'

        # --Save model as json
        with open(join(modelpath,fname), "w") as json_file:
            json_file.write(model_json)

        # --Save weights as .h5
        weight_fname = join(modelpath,f'model.h5')
        model.save_weights(weight_fname)

        print('history to save',model_history.history.keys())
        # --Save history - metrics
        if self.usage == 'HLT':
            np.save(f'{historypath}/AUC_Train.npy',
                    np.asarray(model_history.history['auc']))
            np.save(f'{historypath}/AUC_Val.npy',
                    np.asarray(model_history.history['val_auc']))
        np.save(f'{historypath}/Accuracy_Train.npy',
                np.asarray(model_history.history['binary_accuracy']))
        np.save(f'{historypath}/Accuracy_Val.npy',
                np.asarray(model_history.history['val_binary_accuracy']))
        # --Save history - loss
        np.save(f'{historypath}/Loss_Train.npy',
                np.asarray(model_history.history['loss']))
        np.save(f'{historypath}/Loss_Val.npy',
                np.asarray(model_history.history['val_loss']))

    def Execute(self):
        print(f'--->Training for {self.usage}...')
        print(f'Model tag : {self.tag}')
        if self.usage=='OFFLINE':
            history, model = self.trainHybridCNN()
        elif self.usage=='HLT':
            history, model = self.trainHLTCNN()

        self.save_model_data(model,history)

    @property
    def _save_recipe(self):
        pass

def main():
    """
    The main function of train
    """
    print(f'\n Using TensorFlow {tf.__version__} \n')
    train = Train(new_dir=True)
    train.Execute()
    # print(f'--->Training for {train.usage}...')
    # print(f'Model tag : {train.tag}')
    # if train.usage=='OFFLINE':
        # history, model = train.trainHybridCNN()
    # elif train.usage=='HLT':
        # history, model = train.trainHLTCNN()

    # train.save_model_data(model,history)

if __name__ == '__main__':
    main()



# def getArgs():
    # """
    # Get arguments from command line.
    # """
    # dflt={
          # 'input':None,
          # 'output':'/pbs/home/m/mbelfkir/MLPhotonID/Train/Output/',
          # 'nepochs':10,
          # }

    # # --Init parser
    # args = ArgumentParser(description="Arguments CNN Training")
    # # default action is 'store'

    # # -- I/O parameters
    # args.add_argument('-I', '--input',           default=None,
                      # help='Intput Directory')
    # args.add_argument('-O', '--output',          default=dflt['output'],
                      # help='Output Directory')
    # # -- Data parameters
    # args.add_argument("-S", "--strategy",        default=1,
                      # help="Strategy to load data, and deal with unhealthy clusters.")
    # # -- CNN Layers parameters
    # args.add_argument('-o', '--noutput',         default=1,
                      # help='Number of outputs from the model nclass')
    # args.add_argument('-d',    '--dropout_rate', default=0.08,
                      # help='Dropout rate')
    # args.add_argument('-nCK', '--nConvKernels',  default=256,
                      # help='Number of neurons/kernels in each Conv layer')
    # args.add_argument('-nDK', '--nDenseKernels', default=128,
                      # help='Number of neurons in each Dense layer')
    # args.add_argument('-nCL', '--nConvLayers',   default=2,
                      # help='Number of Conv layers')
    # args.add_argument('-nDL', '--nDenseLayers',  default=2,
                      # help='Number of Dense layers')
    # # args.add_argument('-CNNn', '--CNNn',          action='store',
                      # # default=256,     help='Number of neurons in each CNN layer')
    # # args.add_argument('-DNNn', '--DNNn',          action='store',
                      # # default=128,     help='Number of neurons in each DNN layer')

    # # -- Training parameters
    # args.add_argument('-n', '--nepochs',         default=10,
                      # help='Number of epochs')
    # args.add_argument('-l', '--learning_rate',   default=0.0001,
                      # help='Learning rate')
    # args.add_argument('-b', '--batch_size',      default=32,
                      # help='Batch size')
    # args.add_argument('-G', '--Gen',             default=False,
                      # help='Train the model on generator')
    # args.add_argument("-U", "--trainingType",    default='OFFLINE',
                      # help="type of training or usage to follow, either 'offline' or 'HLT'")
    # return args.parse_args()


# def trainHybridCNN(data, Nepochs, learing_rate, dropout_rate, batch_size, N_CNN_neurons, N_DNN_neurons, Noutputs, tag, OutDir, isGen):

    # clf = CNNHybridClassifier(
        # 'HybridCNN_Classifier', N_CNN_neurons, N_DNN_neurons, Noutputs, dropout_rate).model

    # adm = opt.Adam(lr=learing_rate)
    # clf.compile(loss=['binary_crossentropy'],
                # optimizer=adm, metrics=['binary_accuracy'])
    # print(clf.summary())

    # # -- Appending cluster img to layer containers
    # X1 = np.append(data.Img_Lr1_Train, data.Img_Lr1_Val, axis=0)
    # X2 = np.append(data.Img_Lr2_Train, data.Img_Lr2_Val, axis=0)
    # X3 = np.append(data.Img_Lr3_Train, data.Img_Lr3_Val, axis=0)
    # Y  = np.append(data.Y_Train, data.Y_Val, axis=0)
    # Y  = np.multiply(Y, 1)
    # X1, X2, X3, Y = shuffle(X1, X2, X3, Y, random_state=seed)

    # """
    # NH_X1 = np.append(nohealthy.Img_Lr1_Train, nohealthy.Img_Lr1_Val, axis=0)[:Y.shape[0],:]
    # NH_X2 = np.append(nohealthy.Img_Lr2_Train, nohealthy.Img_Lr2_Val, axis=0)[:Y.shape[0],:]
    # NH_X3 = np.append(nohealthy.Img_Lr3_Train, nohealthy.Img_Lr3_Val, axis=0)[:Y.shape[0],:]
    # NH_Y  = np.append(nohealthy.Y_Train, nohealthy.Y_Val, axis=0)[:Y.shape[0]]
    # NH_Y  = np.multiply(NH_Y, 1)
    # NH_X1, NH_X2, NH_X3, NH_Y = shuffle(NH_X1, NH_X2, NH_X3, NH_Y, random_state=seed)
    # f = int(Y.shape[0]*.5)
    # NH_X1 = NH_X1[:f,:];
    # NH_X2 = NH_X2[:f,:];
    # NH_X3 = NH_X3[:f,:];
    # NH_Y = NH_Y[:f];
    # X1 = np.append(X1, NH_X1, axis=0)
    # X2 = np.append(X2, NH_X2, axis=0)
    # X3 = np.append(X3, NH_X3, axis=0)
    # Y  = np.append(Y, NH_Y, axis=0)
    # """

    # # NOTE: why there are 2 shuffles?
    # X1, X2, X3, Y = shuffle(X1, X2, X3, Y, random_state=seed)

    # es1 = EarlyStopping(monitor='val_loss', patience=5,
                        # verbose=1, mode='min', min_delta=0.0001)
    # save_wgt_on_epoch = ModelCheckpoint(
                # OutDir+'/saved_models/'+'Model_'+tag+'_Ep_{epoch:02d}.weights',
                # monitor='loss',
                # mode='min',
                # verbose=1,
                # save_best_only=True)
    # roc = roc_on_epoch()
    # lrScheduler = LearningRateScheduler(lr_decay, verbose=1)

    # class_weights = cls_W.compute_class_weight('balanced', np.unique(Y), Y)

    # print(f'-> the class weights: {class_weights}')

    # print(f'img shape X1: {X1.shape}')
    # print(f'img shape X2: {X2.shape}')
    # print(f'img shape X3: {X3.shape}')
    # print(f'img shape Y: {Y.shape}')
    # # X1 = X1.reshape(X1.shape[0], 2, 56, 1)
    # # X2 = X2.reshape(X2.shape[0], 11, 7, 1)
    # # X3 = X3.reshape(X3.shape[0], 11, 4, 1)

    # X1 = X1.reshape(X1.shape[0], 2, 56, 1)
    # X2 = X2.reshape(X2.shape[0], 11, 7, 1)
    # X3 = X3.reshape(X3.shape[0], 11, 4, 1)
    # print()
    # print('after reshaping')
    # print(f'img shape X1: {X1.shape}')
    # print(f'img shape X2: {X2.shape}')
    # print(f'img shape X3: {X3.shape}')
    # print(f'img shape Y: {Y.shape}')

    # # history = "local"

    # if isGen:
        # print('Training on Generator ...')
        # datagen = IDG(rotation_range=180, horizontal_flip=True, validation_split=0.33, featurewise_center=True, featurewise_std_normalization=True)
        # train_itr = multiIDG(generator=datagen, X1=X1, X2=X2, X3=X3, Y=Y,
                             # batch_size=batch_size, img_h=128, img_W=128, subset='training')
        # val_itr = multiIDG(generator=datagen, X1=X1, X2=X2, X3=X3, Y=Y,
                           # batch_size=batch_size, img_h=128, img_W=128, subset='validation')

        # callbacks_list = [save_wgt_on_epoch]

        # history = clf.fit_generator(train_itr, validation_data=val_itr, epochs=Nepochs, shuffle=False, steps_per_epoch=(
            # Y.shape[0]*.66)/batch_size, validation_steps=(Y.shape[0]*.33)/batch_size, class_weight=class_weights, callbacks=callbacks_list)
        # return history, clf
    # else:
        # callbacks_list = [save_wgt_on_epoch]

        # history = clf.fit(x={'Input1': X1, 'Input2': X2, 'Input3': X3}, y=Y, epochs=Nepochs, batch_size=batch_size,
                          # shuffle=True, validation_split=0.33, callbacks=callbacks_list, class_weight=class_weights)

    # return history, clf

# def set_tag(usage,N,nCK,nDK,lr,dr,bt,isGen,nCL,nDL):
    # """Creates tag for train model"""
    # if usage == 'OFFLINE':
        # if not isGen:
            # tag = 'Stg1_'+(usage)+'CNN_Nep_'+str(N)+'_CNNn_'+str(nCK)+'_DNNn_' + \
                # str(nDK)+'_lr_'+str(lr)+'_dr_'+str(dr)+'_bt_'+str(bt)
        # else:
            # tag = 'Stg1_'+(usage)+'CNN_Nep_'+str(N)+'_CNNn_'+str(nCK)+'_DNNn_'+str(nDK) + \
                # '_lr_'+str(lr)+'_dr_'+str(dr)+'_bt_'+str(bt)+'_Generator'

    # if usage == 'HLT':
        # traindate = str(date.today())
        # print(f'today is {traindate}')
        # arch_tag=f'{nCK}x{nCL}CL_{nDK}x{nDL}DL_{dr}dr'
        # train_tag=f'{N}Ep_{lr}lr'
        # tag = f'{traindate}_{arch_tag}_{train_tag}'
    # return tag

# def trainHLTCNN(data, Nepochs, learning_rate, dropout_rate, batch_size, N_CNN_neurons, N_DNN_neurons, tag, OutDir, isGen,dynamic_training=False):
    # """
    # Loads model, compiles model, trains model

    # # Args::

    # # Returns::
    # """
    # # --Init model
    # clf = CNNClassifier(
        # 'HLTCNN_Classifier',
        # # N_CNN_neurons,
        # # N_DNN_neurons,
        # # Noutputs,
        # # dropout_rate
        # ).model

    # adm = opt.Adam(lr=learning_rate)
    # clf.compile(
            # loss=['binary_crossentropy'],
            # optimizer=adm,
            # metrics=['binary_accuracy']
            # # metrics=['binary_accuracy',auroc]
            # # metrics=['binary_accuracy','roc']
            # )

    # print(f'--> Model compiled: \n {clf.summary()}')

    # # -- Appending cluster img to layer containers
    # X2 = np.append(data.Img_Lr2_Train, data.Img_Lr2_Val, axis=0)
    # Y  = np.append(data.Y_Train, data.Y_Val, axis=0)
    # Y  = np.multiply(Y, 1)
    # X2, Y = shuffle(X2, Y, random_state=seed)
    # X2 = X2.reshape(X2.shape[0], 11, 7, 1)
    # # print(f'img shape X2: {X2.shape}')
    # # print(f'img shape Y: {Y.shape}')

    # es1 = EarlyStopping(
            # monitor='val_loss',
            # patience=5,
            # verbose=1,
            # mode='min',
            # min_delta=0.0001
            # )

    # # checkpoint_path = f'{OutDir}/saved_models/Model_{tag}/checkpoint/Ep_{epoch:02d}.weights'
    # # checkpoint_path = f'{OutDir}/saved_models/Model_{tag}_Ep_{epoch:02d}.weights'
    # # checkpoint_path = f'{OutDir}/{usage}/{Stg}/{tag}/saved_models/checkpoint/Model_Ep_{epoch:02d}.weights'
    # # checkpoint_path = f'{tag}/saved_models/checkpoint/Model_Ep_{epoch:02d}.weights'
    # checkpoint_path = f'{tag}/saved_models/checkpoint'
    # try:
        # os.mkdir(join(OutDir,checkpoint_path))
    # except: pass

    # save_wgt_on_epoch = ModelCheckpoint(
            # # checkpoint_path,
            # join(checkpoint_path,'Model_Ep_{epoch:02d}.weights'),
            # monitor='loss',
            # mode='min',
            # verbose=1,
            # save_best_only=True
            # )

    # # TODO: check how this ROC is computed
    # roc = roc_on_epoch()
    # lrScheduler = LearningRateScheduler(lr_decay, verbose=1)

    # class_weights = cls_W.compute_class_weight(
                            # 'balanced',
                            # np.unique(Y),
                            # Y
                            # )

    # print(f'--> the class weights: {class_weights}')

    # if isGen:
        # print('*** Training on Generator NOT IMPLEMENTED ***')
        # # ?? Where you trying to make trainning faster,
        # #    or implement DataAugmentation or both?
        # # print('Training on Generator ...')
        # # NOTE: following is DataAugmentation
        # # datagen = IDG(
                # # rotation_range=180,
                # # horizontal_flip=True,
                # # validation_split=0.33,
                # # featurewise_center=True,
                # # featurewise_std_normalization=True
                # # )

        # # train_itr = multiIDG(
                # # generator=datagen,
                # # X1=X1,
                # # X2=X2,
                # # X3=X3,
                # # Y=Y,
                # # batch_size=batch_size,
                # # img_h=128,
                # # img_W=128,
                # # subset='training'
                # # )
        # # val_itr = multiIDG(
                # # generator=datagen,
                # # X1=X1,
                # # X2=X2,
                # # X3=X3,
                # # Y=Y,
                # # batch_size=batch_size,
                # # img_h=128,
                # # img_W=128,
                # # subset='validation'
                # # )

        # # callbacks_list = [save_wgt_on_epoch]

        # # history = clf.fit_generator(
                # # train_itr,
                # # validation_data=val_itr,
                # # epochs=Nepochs,
                # # shuffle=False,
                # # steps_per_epoch=(Y.shape[0]*.66)/batch_size,
                # # validation_steps=(Y.shape[0]*.33)/batch_size,
                # # class_weight=class_weights,
                # # callbacks=callbacks_list
                # # )
        # # return history, clf
    # else:
        # # TODO: use AUC as metric
        # callbacks_list = [save_wgt_on_epoch,roc]
        # # callbacks_list = [save_wgt_on_epoch]

        # # --Fit model get history of training
        # print(f'--> Begining Training')
        # history = clf.fit(
                # x=X2,
                # y=Y,
                # epochs=Nepochs,
                # batch_size=batch_size,
                # shuffle=True,
                # validation_split=0.33,
                # callbacks=callbacks_list,
                # class_weight=class_weights
                # )
        # print(history.history.keys())

    # return history, clf

# def save_model_data(OutDir,tag,model,model_history):
    # """
    # Saves model's weights, history and itself in OutDir

    # # Args::

        # OutDir: (str), dir to store output.
                # Will create sub directories to store model history and weights

        # tag:

        # model:

        # history:

    # # Returns:: None
    # """
    # # TODO: save in history the summary
    # OutDir=f'{OutDir}/{usage}/{Stg}/{tag}'
    # modelpath = f'{OutDir}/saved_models'
    # historypath = f'{OutDir}/history'
    # loss_path=f'{historypath}/loss'
    # metrics_path=f'{historypath}/metrics'
    # try:
        # os.mkdir(OutDir)
        # os.mkdir(modelpath)
        # os.mkdir(historypath)
        # os.mkdir(loss_path)
        # os.mkdir(metrics_path)
    # except: pass

    # model_json = model.to_json()
    # fname= f'model.json'

    # # --Save model as json
    # with open(join(modelpath,fname), "w") as json_file:
        # json_file.write(model_json)

    # # --Save weights as .h5
    # weight_fname = join(modelpath,f'model.h5')
    # model.save_weights(weight_fname)

    # # model.save_weights(modelpath+tag+"_model.h5")
    # # --Save history
    # np.save(f'{metrics_path}/Accuracy_Train_.npy',
            # np.asarray(model_history.history['binary_accuracy']))
    # np.save(f'{metrics_path}/Accuracy_Val_.npy',
            # np.asarray(model_history.history['val_binary_accuracy']))
    # np.save(f'{loss_path}/Train.npy',
            # np.asarray(model_history.history['loss']))
    # np.save(f'{loss_path}/Val.npy',
            # np.asarray(model_history.history['val_loss']))

# NOTE: keeping for back-compatibility
# def save_model_history(model_history, tag, OutDir):
    # """Save model history in NPY format"""
    # OutDir+='/history/'
    # np.save(OutDir+'Accuracy_Train_'+tag+'.npy',
            # np.asarray(model_history.history['binary_accuracy']))
    # np.save(OutDir+'Accuracy_Val_'+tag+'.npy',
            # np.asarray(model_history.history['val_binary_accuracy']))
    # np.save(OutDir+'Loss_Train_'+tag+'.npy',
            # np.asarray(model_history.history['loss']))
    # np.save(OutDir+'Loss_Val_'+tag+'.npy',
            # np.asarray(model_history.history['val_loss']))


"""  ========================================== OLD MAIN
def main():
    train = Train(new_dir=True)

    # args = getArgs()
    # # -- I/O parameters
    # OutDir = str(args.output)
    # # -- Data parameters
    # usage = str(args.trainingType)
    # Stg = int(args.strategy)
    # # -- CNN Layers parameters
    # # nEp = int(args.nepochs)
    # dr = float(args.dropout_rate)
    # nCL = int(args.nConvLayers)        # n Conv layers
    # nDL = int(args.nDenseLayers)       # n Dense layers
    # nCK = int(args.nConvKernels)       # n Conv kernels
    # nDK = int(args.nDenseKernels)      # n Dense kernels
    # # CNNn = int(args.CNNn)
    # # DNNn = int(args.DNNn)
    # Nout = int(args.noutput)           # OFFLINE valid only
    # # -- Training parameters
    # isGen = bool(args.Gen)
    # N = int(args.nepochs)
    # lr = float(args.learning_rate)
    # bt = int(args.batch_size)
    # # -- Assert inputs
    # assert (usage in ['OFFLINE','HLT']), '****> please select type "OFFLINE" or "HLT"'
    # assert (Stg in [0,1,2,3,4]), '****> please choose an strategy from [0,1,2,3,4]'


    # # --Load data from files to CNNData objects
    # data = CNNData(
                # Stg=Stg,
                # Dir=args.input,
                # Usage=usage
                # )
    # # nohealthy = CNNData(1,args.input)
    # tag = set_tag(usage,N,nCK,nDK,lr,dr,bt,isGen,nCL,nDL)

    # model = "local"
    train.Execute()

    # print(f'--->Training for {usage}...')
    # print('Model tag : ', tag)
    # if usage=='OFFLINE':
        # history, model = trainHybridCNN(
                # data, N, lr, dr, bt, nCK, nDK, Nout, tag, OutDir, isGen)
        # # NOTE: keeping for back-compatibility
        # # history, model = trainHybridCNN(
                # # data, N, lr, dr, bt, CNNn, DNNn, Nout, tag, OutDir, isGen)
        # # save_model_history(history, tag, OutDir)
        # # model_json = model.to_json()
        # # with open(OutDir+tag+"_model.json", "w") as json_file:
            # # json_file.write(model_json)
        # # model.save_weights(OutDir+tag+"_model.h5")

        # save_model_data(OutDir,tag,model,history)

    # elif usage=='HLT':
        # history, model = trainHLTCNN(
                                # data,
                                # Nepochs=N,
                                # learning_rate=lr,
                                # dropout_rate=dr,
                                # batch_size=bt,
                                # N_CNN_neurons=nCK,
                                # N_DNN_neurons=nDK,
                                # tag=tag,
                                # OutDir=f'{OutDir}/{usage}/{Stg}/',
                                # isGen=isGen
                                # )
        # save_model_data(OutDir,tag,model,history)


if __name__ == '__main__':
    main()
"""

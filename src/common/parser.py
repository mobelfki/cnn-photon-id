#!/usr/bin/env python3
"""
File: Parser
Author: Noacco Santiago
Email:
Github:
Description: Parser object to use across the package
"""
import configargparse
import yaml

def set_parser():
    parser_ini = configargparse.ArgParser(
            config_file_parser_class=configargparse.ConfigparserConfigFileParser
            )
    global_parser = parser_ini
    # -- Set arguments for command line
    global_parser.add('-c',
            '--config',
            is_config_file=True,
            required=True)

    global_parser.add('-V',
            '--verbose',
            required=False,
            default=1,
            help="verbosity level, default 1 == true.\
                    If true, prints std output on screen.")

    # not implemented yet
    global_parser.add(
            '--loglevel',
            required=False,
            # default='warning',
            help="Log level (info/debug/warning)")

    global_parser.add('-gO',
            '--g_outdir',
            env_var='GLOBAL_OUTDIR',
            required=True,
            help='Output directory for the whole package')

    global_parser.add('--dp_in',
            required=False,
            default=None,
            help='Input directory for DataProcessing/')

    global_parser.add('--dp_out',
            required=False,
            default=None,
            help='Output directory for DataProcessing/')

    global_parser.add('--train_in',
            required=False,
            default=None,
            help='Input directory for NeuralNetworks/Train/')

    global_parser.add('--train_out',
            required=False,
            default=None,
            help='Output directory for NeuralNetworks/Train/')

    global_parser.add('--val_in',
            required=False,
            default=None,
            help='Input directory for NeuralNetworks/Train/')

    global_parser.add('--val_out',
            required=False,
            default=None,
            help='Output directory for NeuralNetworks/Train/')

    global_parser.add('-S',
            '--samples_file',
            required=False,
            help='path to file containing name of files to read as samples.')

    global_parser.add('-F',
            '--features_file',
            required=False,
            help='path to file containing features to read in NTupleToImage.')

    global_parser.add('-U',
            '--usage',
            type=str,
            help="Intended use of the package, {'HLT','OFFLINE'}"
            )

    global_parser.add('-Stg',
            '--strategy',
            env_var='STRATEGY',
            type=int,
            help="Strategy used for getting Clusters {0,1,2,3}"
            )

    global_parser.add('-nChk',
            '--num_chunks',
            required=False,
            type=int,
            default=1,
            help="Total number of chunks to divide data.")

    global_parser.add('-nF2R',
            '--num_files2read',
            required=False,
            default=None,
            help="Total number of files to read data for validation."
            )

    global_parser.add('-Gen',
            '--use_generator',
            action='store_true',
            required=False,
            help="Option to use Image Generators. NOT augmentation!",
            )

    global_parser.add(
            '--use_multiprocessing',
            action='store_true',
            required=False,
            help="Option to use multiprocessing",
            )

    global_parser.add('-KT',
            '--use_kerastuner',
            action='store_true',
            required=False,
            default=False,
            help="(bool) use the Keras Tuner to train."
            )

    global_parser.add('--use_tuner',
            required=False,
            # default='RandomSearch',
            help="(str) name of the tuner class to use (RandomSearch/Hyperband/BayesianOptimization)"
            )

    global_parser.add('--load_best_hp',
            required=False,
            default=None,
            help="pickle File to read best hyperparameters from."
            )

    global_parser.add(
            '--use_img_augmentation',
            action='store_true',
            required=False,
            help="Option to use Image Augmentation Generators",
            )

    global_parser.add('-Swgt',
            '--save_wgts_only',
            action='store_true',
            required=False,
            help="(bool), wheather to save only model weights and architecture (True),\
                    or (False) to save everything using SavedModel TF format.\
                    Carefull support for TF_v1 is not ensured!"
            )

    global_parser.add('-Parch',
            '--path_2arch',
            required=False,
            default=None,
            help='(path)'
            )

    global_parser.add('-Pwgt',
            '--path_2weights',
            required=False,
            default=None,
            help='(path)'
            )

    global_parser.add('--max_kt_models',
            required=False,
            default=None,
            help="number of different hyperperameters configurations the Keras Tuner will try."
            )

    global_parser.add('--exe_per_trial',
            required=False,
            default=1,
            type=int,
            help=" Number of trainings of one hyperperameters config to train. "
            )

    global_parser.add('--kt_num_best_hp',
            required=False,
            default=None,
            type=int,
            help="number of best hyperperameters configurations to save."
            )

    global_parser.add('-TF_v',
            '--tensorflow_version',
            default=2,
            help='TensorFlow version for training, either 1 or 2')

    global_parser.add('-nEp',
            '--num_epochs',
            help="",)

    global_parser.add('-lr',
            '--learning_rate',
            help="")

    global_parser.add('-bt',
            '--batch_size',
            required=False,
            type=int,
            default=128,
            help="")

    global_parser.add('-nO',
            '--num_output',
            required=False,
            default=None,
            help="")

    global_parser.add_argument('--calo',
            type=yaml.safe_load,
            required=False,
            default=None,
            help="dictionary to set clusters parametres."
            )

    global_parser.add_argument('--arch',
            type=yaml.safe_load,
            required=False,
            default=None,
            help="dictionary to set model architecture config."
            )

    global_parser.add_argument('--metrics',
            type=yaml.safe_load,
            required=False,
            default=None,
            help="metrics list to implement"
            )

    global_parser.add_argument('--callbacks',
            type=yaml.safe_load,
            required=False,
            default=None,
            help="callbacks dictionary")

    return global_parser.parse_args()

# def set_ini_parser(config_file):
    # cfg = configparser.ConfigParser()
    # cfg.read(config_file)
    # return cfg

#!/usr/bin/env python3

import os
import sys
import logging
# sys.path.append('../')
# sys.path.append('../../')
from os.path import join
from datetime import date
# from argparse import ArgumentParser
from src.DataProcessing.data_handler import Data
# from DataProcessing.DataProcessing import Data
# from cnn_photon_id_parser import set_parser
from .parser import set_parser

# TODO: add logging
# TODO: combine this with the argument parser

class Script(object):
    """
    Class to set basic scripting option for the Training and Validation of the NeuralNetwork such as the I/O, arguments in parse option, etc.
    """

    def __init__(self,stage=None,**kwargs):
        """
        Description

        # Args::
            stage: (`str`), set what stage of the training is going on,
            this helpst to optimize the loading of data. ("TRAIN","TEST","ALL")

        # Returns:: None
        """
        if stage is not None:
            assert isinstance(stage,str)
            self.stage = stage.upper()
            assert self.stage in ['TRAIN','TEST','PREPROCESS','ALL']
        else:
            self.stage = "ALL"
        self.args = set_parser()
        self._set_params
        self._set_tag
        # self._set_out_subdirs
        # --Load data from files to CNNData objects
        print("input dir is:",self.InDir)

    @property
    def _set_params(self):
        self._set_default_IO()

        # -- Data parameters
        self.usage = self.args.usage
        # print(type(self.usage),self.usage)
        self.stg = int(self.args.strategy)

        # -- CNN Layers parameters
        # NOTE: the following sets all calo shapes as (ETA,PHI)
        self.CaloLr1Shape = (
                int(self.args.calo["Lr1"]["num_eta_cells"]),
                int(self.args.calo["Lr1"]["num_phi_cells"])
                )
        self.CaloLr2Shape = (
                int(self.args.calo["Lr2"]["num_eta_cells"]),
                int(self.args.calo["Lr2"]["num_phi_cells"])
                )
        self.CaloLr3Shape = (
                int(self.args.calo["Lr3"]["num_eta_cells"]),
                int(self.args.calo["Lr3"]["num_phi_cells"])
                )
        self.dr = float(self.args.arch["dropout_rate"])
        self.nCB = int(self.args.arch["maxConvBlocks"])        # n Conv  blocks
        self.nDB = int(self.args.arch["maxDenseBlocks"])       # n Dense blocks
        self.nCL = int(self.args.arch["maxConvLayers"])        # n Conv  layers
        self.nDL = int(self.args.arch["maxDenseLayers"])       # n Dense layers
        self.nCK = int(self.args.arch["maxConvKernels"])       # n Conv  kernels
        self.nDK = int(self.args.arch["maxDenseKernels"])      # n Dense kernels
        if self.usage == "OFFLINE":
            self.Nout = int(self.args.num_output)           # OFFLINE valid only

        # -- Training parameters
        self.isGen = bool(self.args.use_generator)
        self.nEp = int(self.args.num_epochs)
        self.bt = int(self.args.batch_size)
        self.nChk = int(self.args.num_chunks)
        self.lr = float(self.args.learning_rate)
        self.KT = bool(self.args.use_kerastuner)
        # -- Assert inputs
        assert (self.usage in ['OFFLINE','HLT']), '****> please select type ("OFFLINE"/"HLT")'
        assert (self.stg in [0,1,2,3]), '****> please choose an strategy from [0,1,2,3]'

        # -- Others
        self.verbose = bool(self.args.verbose)

    def _set_default_IO(self):
        # -- Set InputDir
        # print("train_in", self.args.train_in)
        dflt_DPinputdata_dir = f"{self.args.g_outdir}/NTuple"
        dflt_NNinputdata_dir = f"{self.args.g_outdir}/DataProcessing"
        if (self.stage == "PREPROCESS") and self.args.dp_in is not None:
            self.InDir = self.args.dp_in
        elif self.stage == "PREPROCESS":
            self.InDir = dflt_DPinputdata_dir
            assert os.path.exists(self.InDir), \
                    f"Expected inputdir: {self.InDir} does not exist"
        if (self.stage == "TRAIN") and self.args.train_in is not None:
            self.InDir = self.args.train_in
        if (self.stage == "TEST") and self.args.val_in is not None:
            self.InDir = self.args.val_in
        elif self.stage == "TEST" or self.stage == "TRAIN":
            self.InDir = dflt_NNinputdata_dir
            assert os.path.exists(self.InDir), \
                    f"Expected inputdir: {self.InDir} does not exist"
        # -- Set Output Dir
        if (self.stage == "PREPROCESS") and (self.args.dp_out is None or False):
            self.OutDir = f"{str(self.args.g_outdir)}/DataProcessing"
        elif self.stage == "PREPROCESS":
            self.OutDir = str(self.args.dp_out)
        if (self.stage == "TRAIN") and (self.args.train_out is None or False):
            self.OutDir = f"{str(self.args.g_outdir)}/Train"
        elif self.stage == "TRAIN":
            self.OutDir = str(self.args.train_out)
        elif (self.stage == "TEST") and (self.args.val_out is None or False):
            self.OutDir = f"{str(self.args.g_outdir)}/Val"
        elif self.stage == "TEST":
            self.OutDir = str(self.args.val_out)

    @property
    def _set_out_subdirs(self):
        # -- Set global model out dir
        self.model_dir = f'{self.OutDir}/{self.usage}/stg_{self.stg}/{self.tag}'
        # -- Set sub dirs
        save_model_path = f'{self.model_dir}/saved_models'
        if self.stage == "TRAIN":
            historypath = f'{self.model_dir}/history'
            # -- If directories do not exist make them
            try:
                print('--> Creating Output Folders')
                os.makedirs(self.model_dir)
                print(f'--> model main dir created it is: {self.model_dir}')
                os.makedirs(f'{save_model_path}/checkpoints')
                os.mkdir(historypath)
            except FileExistsError as e:
                print(f'Using existing dirs')

        elif self.stage == "TEST":
            print(self.model_dir)
            self.histo_dir = f'{self.model_dir}/histograms'
            self.roc_dir = f'{self.model_dir}/roc_curves'
            try:
                print('--> Creating Output Folders')
                print(f'--> model main dir created it is: {self.model_dir}')
                os.makedirs(self.histo_dir)
                os.makedirs(self.roc_dir)
            except FileExistsError as e:
                print(e)

    @property
    # TODO: rename this _set_modeltag
    def _set_tag(self):
        """Creates tag for train model"""
        tag_list = []
        traindate = str(date.today().strftime('%Y%m%d'))
        tag_list.append(traindate)
        print(f'today is {traindate}')
        arch_tag='{nck}x{ncl}CL_{ndk}x{ndl}DL_{dr:3.2f}dr'.format(
                    nck=self.nCK, ncl=self.nCL,
                    ndk=self.nDK, ndl=self.nDL,
                    dr=self.dr
                    )
        tag_list.append(arch_tag)
        train_tag=f'{self.nEp}Ep_{self.lr:3.1e}lr'
        if self.isGen:
            train_tag+='_Generator'
        tag_list.append(train_tag)
            # -- join all tags.
        # self.tag = '_'.os.path.join(tag_list)
        self.tag = '_'.join(tag_list)
        if self.KT:
            self.tag += '_dynamic'

    def load_data(self, chunk):
        # -- Init Data instance
        self.data = Data(
                stg=self.stg,
                data_dir=self.InDir,
                usage=self.usage,
                )
        if self.stage == "TRAIN":
            self.data.loadTrainData(chunk);
            self.data.loadValData(chunk);
        elif self.stage == "TEST":
            self.data.loadTestData(chunk);
        else:
            self.data.loadTrainData(chunk);
            self.data.loadValData(chunk);
            self.data.loadTestData(chunk);

    def yield_data(self, batch, chunk):
        # -- Init Data instance
        self.data = Data(
                stg=self.stg,
                data_dir=self.InDir,
                usage=self.usage,
                )
        if self.stage == "TRAIN":
            self.data.yieldTrainData(
                    batch=batch,
                    chunk=chunk,
                    batch_size=self.bt,
                    num_chunks=self.nChk
                    );
            self.data.yieldValData(
                    batch=batch,
                    chunk=chunk,
                    batch_size=self.bt,
                    num_chunks=self.nChk
                    );
        elif self.stage == "TEST":
            self.data.yieldTestData(
                    batch=batch,
                    chunk=chunk,
                    batch_size=self.bt,
                    num_chunks=self.nChk
                    );
        else:
            self.data.yieldTrainData(
                    batch=batch,
                    chunk=chunk,
                    batch_size=self.bt,
                    num_chunks=self.nChk
                    );
            self.data.yieldValData(
                    batch=batch,
                    chunk=chunk,
                    batch_size=self.bt,
                    num_chunks=self.nChk
                    );
            self.data.yieldTestData(
                    batch=batch,
                    chunk=chunk,
                    batch_size=self.bt,
                    num_chunks=self.nChk
                    );

    def assert_reshape(self,obj,expected_shape,debug=False):
        """
        check if object has the expected shape,
        if not then try to fix it.

        # Args::
            obj:
            expected_shape: (tuple)
            debug:

        # Returns::
            obj: object with the expected_shape
        """
        assert isinstance(expected_shape,tuple), "expected_shape should be a tuple"
        if debug: print(f'expected shape is {expected_shape}')
        if obj.shape != expected_shape:
            out = obj.reshape(expected_shape)
            return out
        else:
            return obj


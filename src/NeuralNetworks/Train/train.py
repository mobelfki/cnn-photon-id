#!/usr/bin/env python3
#=============#
# Author: Santiago Noacco Rosende, Mohamed Belfkir
# Contact: snoaccor@cern.ch
# GitLab:https://gitlab.cern.ch/snoaccor
# GitHub:https://github.com/santinoacco
#=============#
import numpy as np
import sys
import contextlib as ctxt
from os.path import join, exists
import json
import pickle
import types
import datetime
import logging as log
from packaging import version
# -- cnn-photon-id libraries
sys.path.append('../../../')
# from ...common.script import Script
# from ..Models import models as M
from src.common.script import Script
import src.NeuralNetworks.Models.models as M
# -- TensorFlow libraries
import tensorflow as tf
from tensorflow.keras import optimizers as opt
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, LearningRateScheduler, ReduceLROnPlateau
from tensorflow.keras.callbacks import TensorBoard, CSVLogger
from tensorflow.keras.utils import plot_model
from tensorflow.keras.preprocessing.image import ImageDataGenerator as IDG
# NOTE: for some reason AUC works when imported from Keras in TF v2,
#       but doesn't work if imported from tf.keras in TF v2.
#       For TF v1 I think it works both ways
from tensorflow.keras import metrics as tf_metrics
from keras import metrics as K_metrics
# -- Keras Tuner
import kerastuner as kt
from kerastuner.tuners import RandomSearch
# from kerastuner.tuners import RandomSearch, Hyperband, BayesianOptimization
# -- SKlearn libraries
from sklearn.utils import shuffle
from sklearn.utils import class_weight as cls_W


def lr_decay(epoch, lr):
    decay_rate = 0.5
    decay_step = 50
    if epoch % decay_step == 0 and epoch:
        return lr * decay_rate
    return lr

class Train(Script):
    """
    Inherits methods and properties from Script class.
    There is a few stuff runing under the hood,
    so check it out.
    """
    seed = 215
    np.random.seed(seed)
    available_metrics={
            'auc':tf_metrics.AUC(name='auc'),
            'accuracy':tf_metrics.BinaryAccuracy(name='accuracy'),
            'precision':tf_metrics.Precision(name='precision'),
            'recall':tf_metrics.Recall(name='recall'),
            # 'confusion_matrix':tf_metrics.
            }
            # # K_metrics.BinaryAccuracy(name='accuracy'),
            # # K_metrics.Precision(name='precision'),
            # # K_metrics.Recall(name='recall'),
            # # K_metrics.AUC(name='auc'),
    available_callbacks={}

    def __init__(self,stage='TRAIN'):
        super().__init__(stage)
        self._set_out_subdirs
        # -- Check TensorFlow version
        current_TF = version.parse(tf.__version__)
        if self.verbose: print(f'\n    TensorFlow version is {current_TF}    \n')
        if self.verbose: print("--> Num GPUs Available: ",\
                len(tf.config.experimental.list_physical_devices('GPU')))
        log.info(f'\n    TensorFlow version is {current_TF}    \n')
        # NOTE: for some reason that I don't understand,
        #       Batch training doesn't work with TF v1
        if  (current_TF > version.parse('1.15')) and (self.TF_v == 1):
            tf.compat.v1.disable_v2_behavior()
            if self.verbose: print('runing TF v1 compatible')
        self.set_available_callbacks()

    @property
    def _cond_2search(self):
        cond = self.KT and self.load_best_hp is None
        return cond

    @property
    def _set_params(self):
        # -- Load other parameters from parent
        super()._set_params
        self.TF_v = int(self.args.tensorflow_version)
        self.save_weights_only = bool(self.args.save_wgts_only)
        self.max_KT_confg = int(self.args.max_kt_models)
        self.load_best_hp = self.args.load_best_hp
        self.multiprocessing = bool(self.args.use_multiprocessing)
        if self.load_best_hp is not None:
            assert exists(self.load_best_hp)
        # # -- Load other parameters from parent
        self.optimizer = opt.Adam(lr=self.lr)

    def set_inputs(self, input_data, calo_shape):
        """
        Prepares the Input data to feed the CNN

        # Args
            input_data: Data attribute (or list of them) to be used as input.
            calo_shape: expected image shape for the input_data
        """
        assert isinstance(calo_shape,tuple)
        # -- Set X, Y for train and validation
        X = input_data
        # -- Set X shapes
        X_shape = (X.shape[0],) + calo_shape + (1,)
        # -- Fix shape if not correct
        X = self.assert_reshape(X, X_shape)
        log.debug(f" X shape: {X.shape}")
        return X

    def set_labels(self, label_data):
        """
        Prepares the Label data to feed the CNN
        label_data: Data attribute to be used as label.
        """
        # -- Set Labels for training and validation
        Y = label_data
        # -- Mask Label values to int
        Y = Y.astype(int)
        # -- Set Y shapes
        Y_shape = (Y.shape[0],1)
        # -- Fix shape if not correct
        Y = self.assert_reshape(Y,Y_shape)

        log.debug(f' Y shape: {Y.shape}')
        return Y

    def gen_input_from_files(self, input_data, label_data, num_batches):
        """
        Since we have `nChk` files,
        we need a batch that has the same amount of data from each file.
        This preserves the distribution of the full sample.

        # Args::
            input_data: Data attribute (or list of them) to be used as input.

        # Returns::
            iterator: (tuple), with the input generator and label generator
        """
        if self.usage == 'OFFLINE':
            raise RuntimeError(f"Training on Generator not implemented for {self.usage} yet")

        # -- Loop until `on_epoch_ends` kills the generator.
        #    If we don't do this,
        #    then the training rans out of data.
        while True:
            batch_cnt = 0
            while batch_cnt <= num_batches:
                # -- Each batch contains a `batch_size` subsample of all files.
                for chk in range(self.nChk):
                    self.yield_data(batch=batch_cnt, chunk=chk)
                    # -- Get generators:
                    #   NOTE: this next lines must be here,
                    #   since at each `chk` we open a different file,
                    #   therefore it's a different instance each time
                    label_gen = getattr(self.data,label_data)
                    input_gen = getattr(self.data,input_data)
                    # -- TO BE TESTED
                    # if isinstance(input_data,list) and self.usage == 'OFFLINE':
                        # input_gen_dict = {x:getattr(self.data,x) for x in input_data}
                    # --
                    # -- Get ready to feed model.
                    labels = self.set_labels(next(label_gen))
                    inputs = self.set_inputs(next(input_gen),self.CaloLr2Shape)
                    inputs, labels = shuffle(inputs, labels, random_state=self.seed)
                    # print("inputs")
                    # print(inputs.shape,type(inputs))
                    # print("labels")
                    # print(labels.shape,type(labels))
                    yield (inputs, labels)
                batch_cnt+=1

    # NOTE TESTING
    def dataset_from_gen(self, input_data, label_data, num_batches):
        dataset = tf.data.Dataset.from_generator(
                # generator=self.gen_input_from_files(),
                # self.gen_input_from_files(input_data,label_data,num_batches),
                self.gen_input_from_files,
                # generator=self.gen_input_from_files(
                    # input_data,
                    # label_data,
                    # num_batches
                    # ),
                # output_types=(tf.float32, tf.uint8),
                output_types=(tf.float32, tf.float32),
                    # (tf.int64,tf.uint8,tf.uint8,tf.float64),
                    # (tf.int64,tf.uint8)
                    # ),
                # output_shapes=
                args=[input_data, label_data, num_batches]
                )
        return dataset

    def load_model_input(self):
        """
        Returns:: it's very important that the return type is TUPLE.

            train_data: (tuple), contains the Images in tuple[0] and the labels in tuple[1] for training.
            val_data: (tuple), contains the Images in tuple[0] and the labels in tuple[1] for validation.
        """
        # -- Set Labels for training and validation
        Y_train = self.set_labels(self.data.Y_Train)
        Y_val = self.set_labels(self.data.Y_Val)

        # -- Set X, Y for train and validation
        X2_train = self.set_inputs(self.data.X2_Train, calo_shape=self.CaloLr2Shape)
        X2_val = self.set_inputs(self.data.X2_Val, calo_shape=self.CaloLr2Shape)

        if self.usage=='HLT':
            X2_train, Y_train = shuffle(X2_train, Y_train, random_state=self.seed)
            X2_val, Y_val = shuffle(X2_val, Y_val, random_state=self.seed)

            # -- NOTE This is a NECESARY step if you like the code to
            #   f*ckin work with both TF_v1 and TF_v2
            if self.TF_v == 1:
                X2_train = np.asarray(X2_train)
                X2_val = np.asarray(X2_val)

                Y_train = np.asarray(Y_train)
                Y_val = np.asarray(Y_val)

            train_data = (X2_train, Y_train);
            val_data = (X2_val, Y_val);

        elif self.usage=='OFFLINE':
            X1_train = self.set_inputs(getattr(self.data,"X1_Train"), calo_shape=self.CaloLr1Shape)
            X1_val   = self.set_inputs(getattr(self.data,"X1_Val"), calo_shape=self.CaloLr1Shape)
            X3_train = self.set_inputs(getattr(self.data,"X3_Train"), calo_shape=self.CaloLr3Shape)
            X3_val   = self.set_inputs(getattr(self.data,"X3_Val"), calo_shape=self.CaloLr3Shape)


            X1_train, X2_train, X3_train, Y_train = shuffle(X1_train, X2_train, X3_train, Y_train, random_state=self.seed)
            X1_val, X2_val, X3_val, Y_val = shuffle(X1_val, X2_val, X3_val, Y_val, random_state=self.seed)

            X_train = {'Input1': X1_train, 'Input2': X2_train, 'Input3':X3_train}
            X_val = {'Input1': X1_val, 'Input2': X2_val, 'Input3':X3_val}

            train_data = (X_train, Y_train)
            val_data   = (X_val, Y_val)

        return  train_data, val_data

    def get_class_weights(self, y_train, verbose=0):
        """ Get class weights to *balance* training """
        cls_wgts = cls_W.compute_class_weight(
                class_weight='balanced',
                classes=np.unique(y_train[:,0]),
                y=y_train[:,0],
                )
        self.class_weights = dict(enumerate(cls_wgts))

        log.info(f'--> the class weights: {self.class_weights}')
        if verbose:
            print(f'--> the class weights: {self.class_weights}')

    def set_available_callbacks(self):
        """
        ** Set callbacks **
        """
        # -- Set training checkpoints
            # WARNING NOTE: if this is removed then no models will be saved!
        checkpoint_path = f'{self.model_dir}/saved_models/checkpoints'
        if self.save_weights_only:
            fpath_loss = 'weights.Ep{epoch:02d}-val_loss{val_loss:.2f}.h5'
            fpath_auc = 'weights.Ep{epoch:02d}-val_auc{val_auc:.2f}.h5'
        else:
            fpath_loss = 'weights.Ep{epoch:02d}-val_loss{val_loss:.2f}'
            fpath_auc = 'weights.Ep{epoch:02d}-val_auc{val_auc:.2f}'
        self.available_callbacks['model_checkpoints'] = ModelCheckpoint(
                join(checkpoint_path,fpath_loss),
                monitor='val_loss',
                mode='min',
                verbose=1,
                save_best_only=True,
                save_weights_only=self.save_weights_only,
                )
        self.available_callbacks['model_checkpoints'] = ModelCheckpoint(
                join(checkpoint_path,fpath_auc),
                monitor='val_auc',
                mode='max',
                verbose=1,
                save_best_only=True,
                save_weights_only=self.save_weights_only,
                )
            # WARNING NOTE: if this is removed then no history will be saved,
            #               unless you're implementing tensorboard.
        historypath = f'{self.model_dir}/history'
        # -- Set CSVLogger, to save metrics results in csv file.
        self.available_callbacks['csvlogger'] = CSVLogger(
                filename=join(historypath,'training.log'),
                )

        # -- Set TensorBoard
        # NOTE tensorboard in general slows the training quite a bit.
        log_dir = f"{historypath}/tensorboard/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        self.available_callbacks['tensorboard'] = TensorBoard(
                log_dir=log_dir,
                # histogram_freq=1    # NOTE this slows the training a lot.
                )

        # -- Set EarlyStopping
        self.available_callbacks['earlystop_loss'] = EarlyStopping(
                monitor='val_loss',     # so we keep model that does not overfit
                patience=10,
                verbose=1,
                mode='min',
                min_delta=1e-4,
                )
        self.available_callbacks['earlystop_auc'] = EarlyStopping(
                monitor='val_auc',
                patience=4,
                verbose=1,
                mode='max',
                min_delta=1e-4,
                )
        # -- Set Learning Rate Scheduler
        self.available_callbacks['lr_scheduler'] = LearningRateScheduler(
                lr_decay,
                verbose=self.verbose
                )
        # -- Set Reduce Learning Rate on Plateu
        self.available_callbacks['reduce_lr'] = ReduceLROnPlateau(
                monitor='val_loss',
                factor=0.2,
                patience=5,
                min_lr=1e-3,
                )

    @property
    def metrics_list(self):
        """
        ** Choose and set what metrics to keep track of. **

        # Args:: None

        # Returns:: list of metrics for training
        """
        m_lst = [self.available_metrics[x] for x in self.args.metrics.keys()\
                if self.args.metrics[x]]
        return m_lst

    @property
    def callbacks_list(self):
        # -- Choose what to use
        call_list = [self.available_callbacks[x] for x in self.args.callbacks.keys()\
                if self.args.callbacks[x]]
        return call_list

    def set_tuner(self, model, kt_objective):
        assert self.args.use_tuner in ['RandomSearch','Hyperband','BayesianOptimization'],\
                "Unsupported tuner class name, please choose from \
                (RandomSearch/Hyperband/BayesianOptimization)"
        if self.args.use_tuner == 'RandomSearch':
            tuner = RandomSearch(
                    hypermodel  = model,
                    objective   = kt_objective,
                    max_trials  = self.max_KT_confg,
                    # executions_per_trial    = 1,     #TODO: add this as input param arg
                    executions_per_trial    = self.args.exe_per_trial,     #TODO: add this as input param arg
                    directory   = f'{self.model_dir}/saved_models',
                    project_name    = 'dynamic'
                    )
        if self.args.use_tuner == 'Hyperband':
            # tuner = Hyperband(
                    # hypermodel = model,
                    # objective   = kt_objective,
                    # max_epochs  = self.nEp,
                    # executions_per_trial    = 1,     #TODO: add this as input param arg
                    # directory   = f'{self.model_dir}/saved_models',
                    # project_name    = 'dynamic'
                    # )
            pass
        if self.args.use_tuner == 'BayesianOptimization':
            raise RuntimeError("Note implemented yet")
            pass

        return tuner

    def set_model(self):
        """
        Loads and compiles the model.
        If `self.load_best_hp` is False it does a KerasTuner Search.
        If `self.load_best_hp` is True it loads the best hps to build a unique model.

        # Args:: None

        # Returns::
            clf:
        """
        if self.usage == "HLT":
            model_input_shape = self.CaloLr2Shape + (1,)
            # -- Set model
            if not self.KT:
                clf = M.HLT_CNN_Clf(
                        input_shape=model_input_shape,
                        max_nConvKernels=self.nCK,
                        max_nDenseKernels=self.nDK,
                        max_nConvLayers=self.nCL,
                        max_nDenseLayers=self.nDL,
                        max_nConvBlocks=self.nCB,
                        max_nDenseBlocks=self.nDB,
                        dropout_rate=self.dr,
                        ConvAct='relu',
                        ).model
                # -- Set compiler
                clf.compile(
                        loss ='binary_crossentropy',
                        optimizer = self.optimizer,
                        metrics = self.metrics_list,
                        )
            else:
                assert self.TF_v >= 2,\
                        "HyperCNN option only posible with TF 2.0+"
                # -- Define models structure
                clf = M.Hyper_HLT_CNN_Clf(
                        input_shape=model_input_shape,
                        max_nConvBlocks=self.nCB,
                        max_nConvLayers=self.nCL,
                        max_nConvKernels=self.nCK,
                        max_nDenseBlocks=self.nDB,
                        max_nDenseLayers=self.nDL,
                        max_nDenseKernels=self.nDK,
                        metrics_list=self.metrics_list,
                        )
                # -- Set the objective
                search_obj = kt.Objective('val_auc',direction='max')
                # -- Set the tuner
                tuner = self.set_tuner(
                        model=clf,
                        kt_objective=search_obj
                        )
                if not self.load_best_hp:
                    print()
                    tuner.search_space_summary()
                    # NOTE: for HyperModel the `tuner` obj is equivalent to the `model` in the regular case.
                    clf = tuner
                # -- Load an specific set of hyperparameters if desired
                else:
                    with open(self.load_best_hp,'rb') as hps_file:
                        loaded_hps = pickle.load(hps_file)
                    clf = tuner.hypermodel.build(loaded_hps)

        if self.usage == "OFFLINE":
            model_input_shape = {
                    "Input1":self.CaloLr1Shape,
                    "Input2":self.CaloLr2Shape,
                    "Input3":self.CaloLr3Shape
                    }
            log.debug(model_input_shape)
            # print(model_input_shape)
            # -- Init model.
            clf = M.CNNHybridClassifier(
                    model_input_shape   =model_input_shape,
                    max_nConvKernels    =self.nCK,
                    max_nDenseKernels   =self.nDK,
                    Noutputs            =self.Nout,
                    dropout_rate        =self.dr,
                    ).model

            # -- Set compiler
            clf.compile(
                    loss=['binary_crossentropy'],
                    optimizer=self.optimizer,
                    metrics=self.metrics_list
                    )

        # yield(clf)
        return clf

    def fit_dynamic(self, clf, input_train, label_train, val_data, batch_size=None, steps_per_epoch=None, validation_steps=None):
        """
        Implements a Keras Tuner Search with the "tuner" `clf`.
        This method allows for both a generator and none-generator fitting.

        # Args::
            clf: (TensorFlowModel)

            input_train: (NumpyArray/dict/generator), data to be used as input for training.
            If a generator, `label_data` must be `None`, and the generator should return:
            (input_train, label_train [, sample_weights])

            label_train: (NumpyArray/None)

            val_data: (tuple/generator)

            batch_size: (int), default is `None` in which case gets a default value from `self.args.batch_size`.

            steps_per_epoch: (int), default is None. It is necessary if training on generator

            validation_steps: (int), default is None. It is necessary if training on generator

        # Returns:: None
        """
        assert input_train is not None
        assert val_data is not None
        if label_train is None:
            assert isinstance(input_train, types.GeneratorType),\
                    "If `label_train` is not passed `input_train` should be a generator"
            if self.verbose: print(f' --> Beginning Dynamic Training on Generator')
            log.info(f' --> Beginning Dynamic Training on Generator')
        else:
            if self.verbose: print(f' --> Beginning Dynamic Training')
            log.info(f' --> Beginning Dynamic Training')
        clf.search(
                x=input_train,
                y=label_train,
                epochs=self.nEp,
                validation_data=val_data,
                batch_size=batch_size,
                steps_per_epoch = steps_per_epoch,
                validation_steps = validation_steps,
                class_weight=self.class_weights,
                callbacks=self.callbacks_list,
                shuffle=True,
                verbose=1,
                use_multiprocessing=self.multiprocessing,
                )

    def fit_unique(self, clf, input_train, label_train, val_data, batch_size=None, steps_per_epoch=None, validation_steps=None):
        """
        Implements a Keras Model Fit with the "model" `clf`.
        This method allows for both a generator and none-generator fitting.

        # Args::
            clf: (TensorFlowModel)

            input_train: (NumpyArray/dict/generator), data to be used as input for training.
            If a generator, `label_data` must be `None`, and the generator should return:
            (input_train, label_train [, sample_weights])

            label_train: (NumpyArray/None)

            val_data: (tuple/generator)

            batch_size: (int), default is `None` in which case gets a default value from `self.args.batch_size`.

            steps_per_epoch: (int), default is None.
            It is necessary if training on generator

            validation_steps: (int), default is None.
            It is necessary if training on generator

        # Returns:: None
        """
        assert input_train is not None
        if batch_size is None:
            batch_size = self.bt
        if label_train is None:
            assert isinstance(input_train, types.GeneratorType),\
                    "If `label_train` is not passed `input_train` should be a generator"
            if self.verbose: print(f' --> Beginning Training on Generator')
            log.info(f' --> Beginning Training on Generator')
        else:
            if self.verbose: print(f' --> Beginning Training')
            log.info(f' --> Beginning Training')
        clf.fit(
                x=input_train,
                y=label_train,
                epochs=self.nEp,
                validation_data=val_data,
                batch_size=batch_size,
                steps_per_epoch = steps_per_epoch,
                validation_steps = validation_steps,
                shuffle=True,
                # shuffle='batch',
                class_weight=self.class_weights,
                callbacks=self.callbacks_list,
                verbose=1,
                use_multiprocessing=self.multiprocessing,
                )

    def fit_4HLT(self, clf, train_data=None, val_data=None):
        """
        Description

        # Args::

        # Returns::
        """
        if self.isGen:
            num_batches_T = int((self.M_T*self.nChk)/self.bt)
            num_batches_V = int((self.M_V*self.nChk)/self.bt)
            log.debug(f'num batches Training : {num_batches_T}')
            log.debug(f'num batches Validation : {num_batches_V}')
            #   -- Dynamic Gen training
            if self._cond_2search:
                self.fit_dynamic(clf,
                        input_train= self.gen_input_from_files(
                            input_data='X2_Train',
                            label_data='Y_Train',
                            num_batches=num_batches_T
                            ),
                        # input_train= self.dataset_from_gen(
                            # input_data='X2_Train',
                            # label_data='Y_Train',
                            # num_batches=num_batches_T
                            # ),
                        label_train=None,
                        val_data= self.gen_input_from_files(
                            input_data='X2_Val',
                            label_data='Y_Val',
                            num_batches=num_batches_V
                            ),
                        # val_data= self.dataset_from_gen(
                            # input_data='X2_Val',
                            # label_data='Y_Val',
                            # num_batches=num_batches_V
                            # ),
                        batch_size=None,
                        steps_per_epoch = num_batches_T,
                        validation_steps = num_batches_V,
                        )
            #   -- Unique Gen training
            else:
                # NOTE: if you not using keras tuner or are loading a set of
                #       kt hyperparameters, use a unique fit
                self.fit_unique(clf,
                        input_train= self.gen_input_from_files(
                            input_data='X2_Train',
                            label_data='Y_Train',
                            num_batches=num_batches_T
                            ),
                        # input_train= self.dataset_from_gen(
                            # input_data='X2_Train',
                            # label_data='Y_Train',
                            # num_batches=num_batches_T
                            # ),
                        label_train= None,
                        val_data= self.gen_input_from_files(
                            input_data='X2_Val',
                            label_data='Y_Val',
                            num_batches=num_batches_V
                            ),
                        # val_data= self.dataset_from_gen(
                            # input_data='X2_Val',
                            # label_data='Y_Val',
                            # num_batches=num_batches_V
                            # ),
                        batch_size=None,
                        steps_per_epoch = num_batches_T,
                        validation_steps = num_batches_V,
                        )
        #   -- Dinamic None-Gen training
        elif self._cond_2search:
            self.fit_dynamic(clf,
                    x=train_data[0],
                    y=train_data[1],
                    val_data=val_data
                    )
        #   -- Unique None-Gen training
        else:
            self.fit_unique(clf,
                    input_train=train_data[0],
                    label_train=train_data[1],
                    val_data=val_data
                    )

        return clf

    # TODO: use generators
    def fit_4OFFLINE(self, clf, train_data, val_data):
        if self.isGen:
            raise RuntimeError('Training on Generator NOT IMPLEMENTED')
            # FIXME: NOT SUPPORTED
            # if self.verbose: print('Training on Generator ...')
            datagen = IDG(
                    rotation_range=180,
                    horizontal_flip=True,
                    validation_split=0.33,
                    featurewise_center=True,
                    featurewise_std_normalization=True
                    )
            train_itr = multiIDG(
                    generator=datagen,
                    X1=X1, X2=X2, X3=X3, Y=Y,
                    batch_size=self.bt,
                    img_h=128,
                    img_W=128,
                    subset='training')
            val_itr = multiIDG(
                    generator=datagen,
                    X1=X1, X2=X2, X3=X3, Y=Y,
                    batch_size=self.bt,
                    img_h=128,
                    img_W=128,
                    subset='validation')

            callbacks_list = [ckpts]

            # FIXME
            history = clf.fit_generator(
                    train_itr,
                    validation_data=val_itr,
                    epochs=self.nEp,
                    shuffle=False,
                    steps_per_epoch=(Y.shape[0]*.66)/self.bt,
                    validation_steps=(Y.shape[0]*.33)/self.bt,
                    class_weight=class_weights,
                    callbacks=callbacks_list
                    )
        else:
            clf.summary()
            clf.fit(
                    x=train_data[0],
                    y=train_data[1],
                    epochs=self.nEp,
                    batch_size=self.bt,
                    shuffle=True,
                    validation_data=val_data,
                    class_weight=self.class_weights,
                    callbacks=self.callbacks_list,
                    use_multiprocessing=self.multiprocessing
                    )

        return clf

    def Execute(self, model):
        if self.verbose: print(f' --> Training for {self.usage}...')
        if self.verbose: print(f' Model tag : {self.tag}')
        log.info(f' --> Training for {self.usage}...')
        log.info(f' Model tag : {self.tag}')

        if self.isGen:
            if self.usage=='HLT':
                model = self.fit_4HLT(model)
            else:
                raise RuntimeError("")
        else:
            assert self.nChk == 1,\
                    "Training in chunks with out implementing generators is discourage"
            # -- Load the Input Data for the correct Usage.
            Train_Data, Val_Data = self.load_model_input()
            # -- Compute class weights
            self.get_class_weights(Train_Data[1],verbose=sef.verbose)

            if self.usage=='OFFLINE':
                model = self.fit_4OFFLINE(
                        clf=model,
                        train_data=Train_Data,
                        val_data=Val_Data
                        )
            elif self.usage=='HLT':
                model = self.fit_4HLT(
                        clf=model,
                        train_data=Train_Data,
                        val_data=Val_Data
                        )

        return model

    def _save_model_summary(self, model):
        """Save summary to file for quick look"""
        summary_path = join(self.model_dir,'modelsummary.txt')
        with open(summary_path, 'w') as f:
            with ctxt.redirect_stdout(f):
                if self.KT and not self.load_best_hp:
                    model.results_summary()
                else:
                    model.summary()

    def _save_model_graph(self, model):
        """ Save a graph of the model """
        tf.keras.utils.plot_model(
            model,
            to_file=join(self.model_dir,'model.png'),
            show_shapes=True,
            show_layer_names=True,
            rankdir="TB",
            expand_nested=False,
            dpi=96,
            )

    def _save_model_arch(self, model, ext=None):
        """ Save model arch as .json """
        model_json = model.to_json()
        if ext is None:
            filepath = join(f'{self.model_dir}/saved_models','model.json')
        else:
            filepath = join(f'{self.model_dir}/saved_models',ext)
        with open(filepath, "w") as json_file:
            json_file.write(model_json)

    def _save_best_hyperparameters(self, tuner, num_trials):
        """
        Saves the best 'num_trials' hyperparameters found by the 'tuner'

        # Args::
            tuner:
            num_trials:

        # Returns:: None
        """
        best_hp = tuner.get_best_hyperparameters(num_trials)
        for idx, hps in enumerate(best_hp):
            filepath = join(f'{self.model_dir}/saved_models',f'bestHP_{idx+1}.pkl')
            with open(filepath,'wb') as f:
                pickle.dump(hps, f)

    def save_model_data(self, model):
        """
        ** Saves model's graph as .png, summary as .txt and architecture as .json (if save_weights_only == True) in OutDir **

            # Args::

                model: the trained model

            # Returns:: None
        """

        log.info(f" --> saving models summary")
        self._save_model_summary(model)
        if not self.KT:
            # -- Save the model architecture
            log.info(f" --> saving models architecture")
            if self.save_weights_only:
                self._save_model_arch(model)
            self._save_model_graph(model)
        elif not self.load_best_hp:
            self._save_best_hyperparameters(
                    tuner=model,
                    num_trials=self.args.kt_num_best_hp
                    )
            for idx, trial in enumerate(
                    model.get_best_models(num_models=self.args.kt_num_best_hp)
                    ):
                self._save_model_arch(trial, ext=f'model_{idx+1}.json')
                log.debug(f" --> saving {idx+1} best model as 'json'")

def main():
    """
    The main function of train
    """
    # -- Config logging
    log.basicConfig(
            filename='training.log',
            filemode='w',
            # level=log.INFO
            level=log.DEBUG
            )
    train = Train(stage='TRAIN')
    log.info(f' --> files {train.args.num_chunks}')
    if train.verbose:
        print(f' --> files {train.args.num_chunks}')
    model = train.set_model()
    if train.isGen:
        train.load_data(chunk=0)
        # -- Load 1st chunk data to get info
        Train_Data, Val_Data = train.load_model_input()
        train.M_T = Train_Data[1].shape[0]
        train.M_V = Val_Data[1].shape[0]
        log.info(f' --> chunk subsample sizes (M_T:{train.M_T}, M_V:{train.M_V})')
        if train.verbose:
            print(f' --> chunk subsample sizes (M_T:{train.M_T}, M_V:{train.M_V})')
        # -- Compute class weights
        train.get_class_weights(Train_Data[1],verbose=1)
        model = train.Execute(model)
    else:
        train.load_data(chunk=chk)
        model = train.Execute(model)
        # for chk in range(train.args.num_chunks):
            # train.load_data(chunk=chk)
            # print(f'--> loading file {chk + 1}-{train.args.num_chunks}')
            # model = train.Execute(model)

    train.save_model_data(model)
    log.info('** Successful Training ** ')
    if train.verbose:
        print('** Successful Training ** ')

if __name__ == '__main__':
    main()

#!/usr/bin/env python3
#=============#
# Author: M. Belfkir; S. Noacco Rosende
# Contact: snoaccor@cern.ch
# GitLab:https://gitlab.cern.ch/snoaccor
# GitHub:https://github.com/santinoacco
#=============#
import tensorflow as tf
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Dense, Activation, Dropout, BatchNormalization, LSTM, concatenate, Input, Conv2D, MaxPooling2D, Flatten
from tensorflow.keras import initializers
from tensorflow.keras.optimizers import Adam
# Install Keras Tuner if TF version >= 2.0
from kerastuner import HyperModel
from kerastuner.engine.hyperparameters import HyperParameters


#Classifier class

class Classifier():
    """
    Classifier class

    Args:
            name : name of the classifier
            n_inputs : number of inputs features
            n_outputs : number of outputs
            drop_rate : dropout rate
    """

    def __init__(self, name, n_inputs, n_outputs, drop_rate, n_neuros):
        self.NInt = n_inputs;
        self.NOut = n_outputs;
        self.nN   = n_neuros;
        self.DropRate = drop_rate;
        self.Sequential = Sequential()
        self.Sequential.add(LSTM(self.nN, activation='selu',recurrent_activation='sigmoid'))
        self.Sequential.add(BatchNormalization())
        self.Sequential.add(Dense(512, activation='selu'));
        self.Sequential.add(Dropout(self.DropRate));
        self.Sequential.add(Dense(512, activation='selu'));
        #self.Sequential.add(Dropout(self.DropRate));
        self.Sequential.add(Dense(256, activation='selu'));
        self.Sequential.add(Dropout(self.DropRate));
        self.Sequential.add(Dense(128, activation='selu'));
        #self.Sequential.add(Dense(4, activation='elu', kernel_initializer=initializers.VarianceScaling(scale=1.0, mode='fan_in', distribution='uniform')));
        self.Sequential.add(Dense(self.NOut, activation='sigmoid'));

class MClassifier():
    def __init__(self, name, n_inputs_1, n_inputs_2, n_inputs_3, n_out, lstm1, lstm2, lstm3, n1, n2, n3, n, d1, d2, d3, d):

        self.name = name;
        self.Lr1Size = 112;
        self.Lr2Size = 77;
        self.Lr3Size = 44;

        self.nI1 = n_inputs_1;
        self.nI2 = n_inputs_2;
        self.nI3 = n_inputs_3;

        self.NLSTM1 = lstm1;
        self.NLSTM2 = lstm2;
        self.NLSTM3 = lstm3;

        self.N1 = n1;
        self.N2 = n2;
        self.N3 = n3;
        self.N  = n;

        self.D1 = d1;
        self.D2 = d2;
        self.D3 = d3;
        self.D  =  d;

        self.Nout = n_out;

        """
        DNN1-Lr1
        """

        input_N1 = Input(shape=(self.nI1,self.Lr1Size), name='Input1');
        #x_1 = input_N1
        x_1 = BatchNormalization()(input_N1);
        x_1 = LSTM(self.NLSTM1, activation='relu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), name='LSTM1' )(x_1)
        #x_1 = Dense(self.N1,    activation='relu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), name='D11'   )(x_1)
        #x_1 = Dropout(self.D1)(x_1)
        #x_1 = Dense(self.N1,    activation='relu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), name='D12'   )(x_1)
        #x_1 = Dense(self.N1,    activation='relu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), name='D13'   )(x_1)
        out_1 = Dense(self.N1,  activation='relu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), name='D14'   )(x_1)

        """
        DNN2-Lr2
        """
        input_N2 = Input(shape=(self.nI2,self.Lr2Size), name='Input2');
        #x_2 = input_N2
        x_2 = BatchNormalization()(input_N2);
        x_2 = LSTM(self.NLSTM2, activation='relu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), name='LSTM2' )(x_2)
        #x_2 = Dense(self.N2,    activation='relu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), name='D21'   )(x_2)
        #x_2 = Dropout(self.D2)(x_2)
        #x_2 = Dense(self.N2,    activation='relu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), name='D22'   )(x_2)
        #x_2 = Dense(self.N2,    activation='relu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), name='D23'   )(x_2)
        out_2 = Dense(self.N2,  activation='relu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), name='D24'   )(x_2)

        """
        DNN3-Lr3
        """
        input_N3 = Input(shape=(self.nI3,self.Lr3Size), name='Input3');
        #x_3 = input_N3
        x_3 = BatchNormalization()(input_N3);
        x_3 = LSTM(self.NLSTM3, activation='relu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), name='LSTM3' )(x_3)
        #x_3 = Dense(self.N3,    activation='relu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), name='D31'   )(x_3)
        #x_3 = Dropout(self.D3)(x_3)
        #x_3 = Dense(self.N3,    activation='relu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), name='D32'   )(x_3)
        #x_3 = Dense(self.N3,   activation='relu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), name='D33'   )(x_3)
        out_3 = Dense(self.N3,  activation='relu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), name='D34'   )(x_3)


        """
        DNN-Photon
        """

        x = concatenate([out_1,out_2,out_3]);
        x = Dense(self.N,    activation='relu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), name='D1'   )(x)
        x = Dropout(self.D)(x)
        x = Dense(self.N,    activation='relu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), name='D2'   )(x)
        x = Dense(4,   activation='relu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), name='D3'   )(x)
        out = Dense(self.Nout,  activation='sigmoid', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), name='Output')(x)

        self.model = Model(inputs=[input_N1, input_N2, input_N3], outputs=out)

class SSClassifier():
    """
    Classifier class

    Args:
            name : name of the classifier
            n_inputs : number of inputs features
            n_outputs : number of outputs
            drop_rate : dropout rate
    """

    def __init__(self, name, n_inputs, n_outputs, drop_rate, n_neuros):

        self.name = name;
        self.NInt = n_inputs;
        self.NOut = n_outputs;
        self.nN       = n_neuros;
        self.DropRate = drop_rate;

        self.Sequential = Sequential()
        self.Sequential.add(BatchNormalization())
        self.Sequential.add(Dense(n_neuros, activation='selu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), bias_initializer='zeros'));
        self.Sequential.add(Dropout(self.DropRate));
        self.Sequential.add(Dense(n_neuros, activation='selu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), bias_initializer='zeros'));
        #self.Sequential.add(Dense(n_neuros, activation='selu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), bias_initializer='zeros'));
        #self.Sequential.add(Dropout(self.DropRate));
        #self.Sequential.add(Dense(n_neuros, activation='selu', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), bias_initializer='zeros'));
        self.Sequential.add(Dense(self.NOut, activation='sigmoid', kernel_initializer=initializers.VarianceScaling(scale=2.0, mode='fan_in', distribution='uniform'), bias_initializer='zeros'));

class Base_CNN(object):
    def __init__(
            self, input_shape,
            max_nConvKernels, max_nConvLayers,
            max_nDenseKernels, max_nDenseLayers,
            ):
        assert isinstance(input_shape, tuple),\
                "'input_shape' must be a tuple"
        self.input_shape = input_shape
        assert isinstance(max_nConvLayers, int),\
                "'max_nConvLayers' must be int"
        self.nCL = max_nConvLayers
        assert isinstance(max_nConvKernels, int),\
                "'max_nConvKernels' must be int"
        self.nCK = max_nConvKernels
        assert isinstance(max_nDenseLayers, int),\
                "'max_nDenseLayers' must be int"
        self.nDL = max_nDenseLayers
        assert isinstance(max_nDenseKernels,int),\
                "'max_nDenseKernels must be int"
        self.nDK = max_nDenseKernels

class HLT_CNN_Clf(Base_CNN):
    def __init__(
            self, input_shape,
            max_nConvKernels, max_nConvLayers, max_nConvBlocks,
            max_nDenseKernels, max_nDenseLayers, max_nDenseBlocks,
            # max_nConvKernels, max_nConvLayers, max_nDenseKernels, max_nDenseLayers, max_nConvBlocks, max_nDenseBlocks,
            dropout_rate, ConvAct,
            ):
        super().__init__(input_shape, max_nConvKernels, max_nConvLayers, max_nDenseKernels, max_nDenseLayers)
        assert isinstance(max_nConvBlocks,int),\
                "'max_nConvBlocks must be int"
        self.nCB = max_nConvBlocks
        assert isinstance(max_nDenseBlocks,int),\
                "'max_nDenseBlocks must be int"
        self.nDB = max_nDenseBlocks
        self.dr = dropout_rate
        self.ConvAct=ConvAct
        self.k_init = initializers.VarianceScaling(
                scale=2.0,
                mode='fan_in',
                distribution='uniform'
                )
        # -- Build Model
        self.build()

    def build(self):
        self.model = Sequential()
        self.model.add(
                    Conv2D(
                        self.nCK,
                        kernel_size=2,
                        activation=self.ConvAct,
                        input_shape=self.input_shape
                        )
                    )
        for lyr in range(self.nCL-1):
            self.model.add(
                        Conv2D(
                            self.nCK,
                            kernel_size=2,
                            activation=self.ConvAct,
                            )
                        )
        self.model.add(MaxPooling2D((2, 2)))
        # == 2nd Conv Block ==
        self.model.add(
                Conv2D(
                    32,
                    kernel_size=2,
                    activation=self.ConvAct,
                    )
                )
        self.model.add(Flatten())

        # NOTE: is a Block of M layers and 1 Dropout
        # -- Dense 1st block
        for lyr in range(self.nDL):
            self.model.add(
                        Dense(
                            self.nDK,
                            activation='relu',
                            kernel_initializer=self.k_init,
                            bias_initializer='zeros'
                            )
                        )
        self.model.add(Dropout(self.dr));
        # -- Dense 2nd block
        self.model.add(
                    Dense(
                        64,
                        activation='relu',
                        kernel_initializer=self.k_init,
                        bias_initializer='zeros'
                        )
                    )

        # -- Output probabilities
        self.model.add(
                Dense(
                    1,
                    activation='sigmoid',
                    kernel_initializer=self.k_init,
                    bias_initializer='zeros'
                    )
                )

class Hyper_HLT_CNN_Clf(Base_CNN,HyperModel):
    def __init__(
            self, input_shape,
            max_nConvKernels, max_nConvLayers,
            max_nDenseKernels, max_nDenseLayers,
            max_nConvBlocks, max_nDenseBlocks,
            metrics_list
            ):
        super().__init__(input_shape, max_nConvKernels, max_nConvLayers, max_nDenseKernels, max_nDenseLayers)

        # -- Feature extraction
        self.nCB = max_nConvBlocks
        self.nDB = max_nDenseBlocks
        self.act_list = ['relu','selu']
        # self.dr_list = [0.1, 0.2, 0.3, 0.4, 0.5]
        # self.lr_list = [1e-2, 1e-3, 1e-4]
        self.dr_list = [0.1]
        self.lr_list = [1e-3]
        self.metrics = metrics_list
        assert self.nCK % 16 == 0, "max_nConvKernels should be a multiple of 16"
        assert self.nDK % 32 == 0, "max_nConvKernels should be a multiple of 32"

        # self._set_hp()

    def _set_hp(self):
        self.hp = HyperParameters()
        # -- Set HiperParams
        self.hp.Int(
                name='n_Conv_blocks',
                min_value=0,
                max_value=self.nCB,
                step=1,
                )
        self.hp.Int(
                name='n_Conv_layers',
                min_value=0,
                max_value=self.nCL,
                step=1,
                )
        self.hp.Choice(
                name='Conv_Activation',
                values=self.act_list,
                )

        self.hp.Int(
                name='n_Dense_blocks',
                min_value=1,
                max_value=self.nDB,
                step=1,
                default=1,
                )
        self.hp.Int(
                name='n_Dense_layers',
                min_value=1,
                max_value=self.nDL,
                step=1,
                )

        self.hp.Choice(
                name='dropout_rate',
                values=self.dr_list,
                )
        self.hp.Choice(
                'learning_rate',
                values=self.lr_list,
                )

    def get_hp(self):
        return self.hp

    # def build(self, hp=self.hp):
    def build(self, hp):
        # -- Set HiperParams
        hp_nCB = hp.Int(
                name='n_Conv_block',
                min_value=0,
                max_value=self.nCB,
                step=1,
                )
        hp_nCL = hp.Int(
                name='n_Conv_layer',
                min_value=0,
                max_value=self.nCL,
                step=1,
                )
        hp_Conv_act = hp.Choice(
                name='Conv_Activation',
                values=self.act_list,
                )

        hp_nDB = hp.Int(
                name='n_Dense_blocks',
                min_value=1,
                max_value=self.nDB,
                step=1,
                default=1,
                )
        hp_nDL = hp.Int(
                name='n_Dense_layers',
                min_value=1,
                max_value=self.nDL,
                step=1,
                )

        hp_dr = hp.Choice(
                name='dropout_rate',
                values=self.dr_list,
                )
        hp_lr = hp.Choice(
                'learning_rate',
                values=self.lr_list,
                )

        # -- Set type of model
        model = Sequential()
        # -- INPUT LAYER - FIXED
        model.add(
                Conv2D(
                    filters = hp.Int(
                        name='filters',
                        min_value=16,
                        max_value=self.nCK,
                        step=16,
                        ),
                    kernel_size=2,
                    activation='relu',
                    input_shape=self.input_shape
                    )
                )
        # -- Blocks of Convolutional Layers
        for blk in range(hp_nCB):
            for lyr in range(hp_nCL):
                model.add(
                        Conv2D(
                            filters=hp.Int(
                                name='filters_B'+str(blk+1)+'xL'+str(lyr+1),
                                min_value=16,
                                max_value=self.nCK,
                                step=16,
                                ),
                            kernel_size=2,
                            activation=hp_Conv_act,
                            )
                        )
            model.add(MaxPooling2D((2,2)))
        model.add(Flatten())
        # -- Blocks of Dense Layers
        for blk in range(hp_nDB):
            for lyr in range(hp_nDL):
                model.add(
                        Dense(
                            units=hp.Int(
                                # name='units_'+str(blk)'x'{lyr}',
                                name=f'units_{blk}x{lyr}',
                                min_value=32,
                                max_value=self.nDK,
                                step=32,
                                ),
                            activation='relu',
                            )
                        )
            model.add(Dropout(hp_dr))
        # -- Predict probabilities
        model.add(Dense(1, activation='sigmoid'))

        print(model.summary())
        # -- Compile
        model.compile(
            optimizer=Adam(hp_lr),
            loss='binary_crossentropy',
            metrics=self.metrics
            )

        return model


# class CNNHybridClassifier(Base_CNN):
    # def __init__(self, max_nConvKernels, max_nDenseKernels, Noutputs, dropout_rate):
        # super().__init__( max_nConvKernels, max_nConvLayers, max_nDenseKernels,  max_nDenseLayers)
class CNNHybridClassifier(object):

    # def __init__(self, max_nConvKernels, max_nDenseKernels, Noutputs, dropout_rate):
    def __init__(self,
            model_input_shape, max_nConvKernels, max_nDenseKernels,
            Noutputs, dropout_rate
            ):
        assert isinstance(model_input_shape,dict) and len(model_input_shape) == 3
        assert isinstance(max_nConvKernels, int)
        self.nCK = max_nConvKernels,
        assert isinstance(max_nDenseKernels, int)
        self.nDK = max_nDenseKernels,
        print(f'types: nCK is {type(self.nCK)}, nDK is {type(self.nDK)}')
        print(f'ex: nCK is {self.nCK}, nDK is {self.nDK}')
        assert isinstance(Noutputs,int), "Noutputs must be type 'int'"
        self.Noutputs = Noutputs;
        self.dr = dropout_rate
        self.kinit =initializers.VarianceScaling()
        # self.name = name

        # -- Build model
        # self.build()

    # def build(self):
        input1_shape = model_input_shape['Input1'] + (1,)
        input2_shape = model_input_shape['Input2'] + (1,)
        input3_shape = model_input_shape['Input3'] + (1,)

        print(f'shapes: {input1_shape}, {input2_shape}, {input3_shape}')
        # input1 = Input(shape=(2,56,1), name='Input1');
        # input1 = Input(shape=input1_shape, name='Input1');
        # # X1 = Conv2D(N_CNN_neurons,  kernel_size=(2,2), activation='selu',kernel_initializer=self.kinit, name='Cov11')(input1)
        # X1 = Conv2D(self.nCK[0],  kernel_size=(2,2), activation='selu',kernel_initializer=self.kinit, name='Cov11')(input1)
        # # X1 = Conv2D(filters=self.nCK,  kernel_size=(2,2), activation='selu',kernel_initializer=self.kinit, name='Cov11')(input1)
        # X1 = MaxPooling2D((1, 2), name='MaxP11')(X1)
        # # X1 = Conv2D(N_CNN_neurons,  kernel_size=(1,2), activation='selu',kernel_initializer=self.kinit, name='Cov12')(X1)
        # X1 = Conv2D(self.nCK[0],  kernel_size=(1,2), activation='selu',kernel_initializer=self.kinit, name='Cov12')(X1)
        # X1 = MaxPooling2D((1, 2), name='MaxP12')(X1)
        # X1 = Flatten(name='Flat1')(X1)

        # # input2 = Input(shape=(11,7,1), name='Input2');
        # input2 = Input(shape=input2_shape, name='Input2');
        # X2 = Conv2D(self.nCK[0],  kernel_size=(2,2), activation='selu',kernel_initializer=self.kinit, name='Cov21')(input2)
        # X2 = MaxPooling2D((2, 1), name='MaxP21')(X2)
        # X2 = Conv2D(self.nCK[0],  kernel_size=(2,1), activation='selu',kernel_initializer=self.kinit, name='Cov22')(X2)
        # X2 = MaxPooling2D((2, 1), name='MaxP22')(X2)
        # X2 = Flatten(name='Flat2')(X2)

        # input3 = Input(shape=input3_shape, name='Input3');
        # # input3 = Input(shape=(11,4,1), name='Input3');
        # X3 = Conv2D(self.nCK[0],  kernel_size=(2,2), activation='selu',kernel_initializer=self.kinit, name='Cov31')(input3)
        # X3 = MaxPooling2D((2, 1), name='MaxP31')(X3)
        # X3 = Conv2D(self.nCK[0],  kernel_size=(2,1), activation='selu',kernel_initializer=self.kinit, name='Cov32')(X3)
        # X3 = MaxPooling2D((2, 1), name='MaxP32')(X3)
        # X3 = Flatten(name='Flat3')(X3)

        # X = concatenate([X1,X2,X3], name='Concatenate');
        X = concatenate(
                [
                    self._input1(input1_shape),
                    self._input2(input2_shape),
                    self._input3(input3_shape)
                    ],
                name='Concatenate'
                );

        X = Dense(self.nDK[0], activation='selu', kernel_initializer=self.kinit, name='D1')(X)
        X = Dense(self.nDK[0], activation='selu', kernel_initializer=self.kinit, name='D2')(X)
        X = Dropout(self.dr, name='DO1')(X)
        X = Dense(self.nDK[0], activation='selu', kernel_initializer=self.kinit, name='D3')(X)
        out = Dense(self.Noutputs,  activation='sigmoid', kernel_initializer=self.kinit, name='Output')(X)

        # self.model = Model(inputs=[input1,input2,input3], outputs=out)
        self.model = Model(
                inputs=[
                    self.input1,
                    self.input2,
                    self.input3
                    ],
                outputs=out)

    def _input1(self,shape):
        input1 = Input(shape=shape, name='Input1');
        X1 = Conv2D(self.nCK[0],  kernel_size=(2,2), activation='selu',kernel_initializer=self.kinit, name='Cov11')(input1)
        X1 = MaxPooling2D((1, 2), name='MaxP11')(X1)
        X1 = Conv2D(self.nCK[0],  kernel_size=(1,2), activation='selu',kernel_initializer=self.kinit, name='Cov12')(X1)
        X1 = MaxPooling2D((1, 2), name='MaxP12')(X1)
        X1 = Flatten(name='Flat1')(X1)

        self.input1 = input1
        return X1

    def _input2(self,shape):
        input2 = Input(shape=shape, name='Input2');
        # input2 = Input(shape=(11,7,1), name='Input2');
        X2 = Conv2D(self.nCK[0],  kernel_size=(2,2), activation='selu',kernel_initializer=self.kinit, name='Cov21')(input2)
        X2 = MaxPooling2D((2, 1), name='MaxP21')(X2)
        X2 = Conv2D(self.nCK[0],  kernel_size=(2,1), activation='selu',kernel_initializer=self.kinit, name='Cov22')(X2)
        X2 = MaxPooling2D((2, 1), name='MaxP22')(X2)
        X2 = Flatten(name='Flat2')(X2)

        self.input2 = input2
        return X2

    def _input3(self,shape):
        # input3 = Input(shape=(11,4,1), name='Input3');
        input3 = Input(shape=shape, name='Input3');
        X3 = Conv2D(self.nCK[0], kernel_size=(2,2), activation='selu', kernel_initializer=self.kinit, name='Cov31')(input3)
        X3 = MaxPooling2D((2, 1), name='MaxP31')(X3)
        X3 = Conv2D(self.nCK[0],  kernel_size=(2,1), activation='selu',kernel_initializer=self.kinit, name='Cov32')(X3)
        X3 = MaxPooling2D((2, 1), name='MaxP32')(X3)
        X3 = Flatten(name='Flat3')(X3)

        self.input3 = input3
        # return X3
        return X3




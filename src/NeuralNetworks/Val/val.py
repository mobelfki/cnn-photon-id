#!/usr/bin/env python3
#=============#
# Authors: M. Belfkir, S. Noacco Rosende
# Contact: snoaccor@cern.ch
# GitLab:https://gitlab.cern.ch/snoaccor
# GitHub:https://github.com/santinoacco
#=============#
import numpy as np
import pandas as pd
import numba
# import matplotlib.pyplot as plt
import sys
import os
from datetime import date
import logging as log
sys.path.append('../../../')
# -- cnn-photon-id libraries
from src.common.script import Script
import src.NeuralNetworks.Models.models as M
import val_mod as Mod
from Plot import plot_mod as P
# -- Models
import tensorflow as tf
from tensorflow.keras import optimizers as opt
from tensorflow.keras import metrics
from tensorflow.keras.models import model_from_json
# -- Calls


seed = 215
np.random.seed(seed)

#### NUMBA COMPILED ROUTINES ####
@numba.njit
def get_Lr2HLT_passcut_fast(ETA, ph_eta, ph_pT, ph_Reta, ph_Rhad, ph_Eratio, threshold=25, ph_F1=None):
    """
     Set trigger cuts for eta bins according to a given threshold.
     For now it's only Medium Cuts

    # Args::
        ETA:

        ph_eta:

        ph_pT:

        ph_Rhad:

        ph_Reta:

        ph_Eratio:

        ph_F1:

        threshold:


    # Returns::
    """
    abs_ph_eta = np.abs(ph_eta)

    trigger_F1Cut = [0.005]
    if (20. <= threshold < 30):
        trigger_Rhad_trsd   = np.asarray([0.071, 0.062, 0.075, 0.060, 0.051, 0.057, 0.075, 0.072, 0.051])
        trigger_Reta_trsd   = np.asarray([0.819375, 0.819375, 0.800375, 0.828875, 0.7125, 0.805125, 0.843125, 0.824125, 0.700625])
        trigger_Eratio_trsd = -999*np.ones(9)

    if (30. <= threshold < 40):
        trigger_Rhad_trsd   = np.asarray([0.071, 0.062, 0.075, 0.060, 0.051, 0.057, 0.075, 0.072, 0.051])
        trigger_Reta_trsd   = np.asarray([0.819375, 0.819375, 0.800375, 0.828875, 0.7125, 0.805125, 0.843125, 0.824125, 0.700625])
        # trigger_Eratio_trsd = [-999., -999., -999., -999., -999., -999., -999., -999., -999.]
        trigger_Eratio_trsd = -999*np.ones(9)

    IBIN = []
    # -- Get index to check cut given by the ph_eta value.
    # NOTE: THERE MUST BE A WAY TO MAKE THIS MORE EFFICIENTLY
    for i in range(ETA.shape[0]-1):
        for j in range(ph_eta.shape[0]):
            cond = (ETA[i] <= abs_ph_eta[j]) and (abs_ph_eta[j] < ETA[i+1])
            if cond:
                # -- Get etabin given ph_eta
                IBIN.append(i)

    # log.debug(f'IBIN size = {len(IBIN)}', f'ex IBIN: {IBIN[:30]}')

    # -- Map cuts to ph
    trig_Reta_Cut = np.asarray([trigger_Reta_trsd[i] for i in IBIN])
    trig_Rhad_Cut = np.asarray([trigger_Rhad_trsd[i] for i in IBIN])
    trig_Eratio_Cut = np.asarray([trigger_Eratio_trsd[i] for i in IBIN])

    # --Apply Reta cut
    RetaCut = np.greater_equal(ph_Reta,trig_Reta_Cut)
    # --Apply Rhad cut
    RhadCut = np.less_equal(ph_Rhad,trig_Rhad_Cut)
    # --Apply Eratio cut
    EratioCut = np.greater_equal(ph_Eratio,trig_Eratio_Cut)

    # NOTE: element wize ops.
    # inCrack = np.abs(ph_eta) > 2.37 or (1.37 < np.abs(ph_eta) < 1.52)
    fst_incrack = np.greater(abs_ph_eta, 2.37)
    snd_incrack = np.logical_and(np.greater(abs_ph_eta,1.37),np.less(abs_ph_eta,1.52))
    inCrack =np.logical_or(fst_incrack, snd_incrack)

    # -- Set pass condition as fullfilling all cuts plus not being in a crack region
    PassCond = RetaCut & EratioCut & RhadCut

    return PassCond

@numba.njit
def _optimized_masking_confusion_mat(y_true,z_true):
    """ Set mask for conditional values """
    mask_TP = (y_true == 1)&(z_true == 1)
    mask_TN = (y_true == 0)&(z_true == 0)
    mask_FP = (y_true == 0)&(z_true == 1)
    mask_FN = (y_true == 1)&(z_true == 0)

    TP = y_true[mask_TP].shape[0]
    TN = y_true[mask_TN].shape[0]
    FP = y_true[mask_FP].shape[0]
    FN = y_true[mask_FN].shape[0]

    return TP,TN,FP,FN
#################################

class Validation(Script):

    def __init__(self,stage='TEST',verbose=True):
        """
        The following implies the init of all the methods
        and properties set in Script.__init__()

        verbose: default is TRUE
            False: Silent
            True: Print basic info.
        """
        super().__init__(stage)
        self.verbose = verbose
        self._set_out_subdirs
        self._set_containers

    @property
    def _set_params(self):
        super()._set_params
        assert os.path.exists(self.args.path_2arch) and isinstance(self.args.path_2arch,str),\
                    "File doesn't exist or was defined incorrectly"
        if self.args.num_files2read is None:
            self.nf2r = self.args.num_chunks
        else:
            assert isinstance(self.args.num_files2read,int),\
                    'Number of files to read shall be type "int" '
            assert self.args.num_files2read <= self.args.num_chunks,\
                    f'Number of files to read shall be smaller or equal to \
                    {self.args.num_chunks}'
            self.nf2r = self.args.num_files2read
        self.path = str(self.args.path_2arch)
        self.path_to_model_wgts = str(self.args.path_2weights)
        self.load_full_model = not bool(self.args.save_wgts_only)
        if self.load_full_model==False:
            assert self.path_to_model_wgts is not None,\
                    "Expecting weights file path,\
                    set it in `config.ini` or pass it through '-Pwgt'."

        self.PT = [10,20,30,40,60,80,100,2000]
        if self.usage == 'OFFLINE':
            # NOTE: IF the following arr are gonna be unmutable
            #       then it would be better if they are sets or tuples.
            self.ETA = [0.,0.4,0.8,1.2,1.37,1.52,1.8,2.,2.2,2.5]
        elif self.usage == 'HLT':
            self.ETA = np.asarray([0., 0.6, 0.8, 1.15, 1.37, 1.52, 1.81, 2.01, 2.37, 2.47])   # <-- This is the REAL ARRAY WE SHOULD USE.

    @property
    def _set_containers(self):
        """
        WP_dict: (dict) it keeps the WorkingPoints data according to type(Binned/Inclusive) and usage

        ROC_dict: (dict) contains PandasDataFrames for the efficiecies to compute the ROCs

        # Args::None

        # Returns::None
        """
        # -- Set containers
        self.WP_dict = {
                'Inclusive':{
                    'Tight':{
                        'Sig_Eff':0.,
                        'Bkg_Rej':0.,
                        }
                    }
                }
        if self.usage == 'HLT':
            self.WP_dict['Inclusive'].update({
                    'HLT_FastReco':{
                        # 'Sig_Eff':[],
                        # 'Bkg_Rej':[],
                        'Sig_Eff':0.,
                        'Bkg_Rej':0.,
                        }
                    })
        for eta in self.ETA:
            for pt in self.PT:
                self.WP_dict[f'({eta};{pt})'] = {}
                for usage in self.WP_dict['Inclusive'].keys():
                    self.WP_dict[f'({eta};{pt})'][usage] = {
                            # 'Sig_Eff':[],
                            # 'Bkg_Rej':[],
                            'Sig_Eff':0.,
                            'Bkg_Rej':0.,
                            }

        self.ROC_dict = {'Inclusive': pd.DataFrame()}
        for eta in self.ETA:
            for pt in self.PT:
                self.ROC_dict[f'({eta};{pt})'] = pd.DataFrame()

        self.AUC_dict = {'Inclusive':{}}
        for eta in self.ETA:
            for pt in self.PT:
                self.AUC_dict[f'({eta};{pt})'] = {}

        # -- Init matrices
        mat_dim = (len(self.PT)-1,len(self.ETA)-1,3)
        # self.Auc_Inc = np.nan*np.ones(mat_dim)
        self.Auc_Inc = np.zeros(mat_dim) # the zeros let you sum lists

        # TODO: implement these ones! to do so 'maxEff' and 'scanEff' should work.
        # Bkg_Inc = np.zeros(mat_dim)
        # Sig_Inc = np.zeros(mat_dim)
        # CNN_Bkg_Inc = np.zeros(mat_dim)
        # CNN_Sig_Inc = np.zeros(mat_dim)
        # Bkg_Imp_Inc = np.zeros(mat_dim)
        # Sig_Imp_Inc = np.zeros(mat_dim)


    @property
    def _set_tag(self):
        """Uses Trained Tag which should be passed by the args.path_2arch arg"""
        full_path_arr = self.path.split('/')
        try:
            for p in full_path_arr:
                if 'CL' in p:
                # -- NOTE: I am using a pair of letters that should be in the train tag.
                    self.tag = p
            if self.verbose: print('CHECKING TAG',self.tag)
            if self.tag is None:
                raise FileNotFoundError
        except FileNotFoundError:
            print("Can't find model")

    def _crack_cond(self, eta):
        if 1.37<= eta < 1.52:
            return True
        else:
            return False

    def _not_enough_phs(self, y_true_masked, eta, eta_next, pt, pt_next):
        if y_true_masked.shape[0] == 0:
            log.warning(f'**> No photons in range ({eta},{eta_next})x({pt},{pt_next})... skipping')
            return True
        else:
            return False

    def _not_enough_representation(self,y_true_masked):
        unique_categories =  np.unique(y_true_masked)
        if unique_categories.shape[0] < 2:
            log.warning('**> No samples of both categories, can not compute ROC... skipping')
            return True
        else:
            return False

    def load_model(self):
        """
        Loads the model from the saved files,
        two options available:
        >   Load Full Model: if the model was saved under the TF_SavedModel format.
        >   Load Weights: if the model's architecture as saved as a .json file and the model's weights as a .h5 file.
        """
        if self.load_full_model:
            clf = tf.keras.models.load_model(filepath=self.path)
        else:
            with open(self.path,'r') as json_file:
                loaded_from_json = json_file.read()
                clf = tf.keras.models.model_from_json(loaded_from_json)
                clf.load_weights(self.path_to_model_wgts)

        log.info(' --> Model Reloded ')
        if self.verbose: print(' --> Model Reloded ')
        return clf

    def _get_Xtest(self):
        # -- Get Test Data
        X2_test = self.data.X2_Test
        # -- Check shapes are fine
        X2_test_shape = (X2_test.shape[0],) + self.CaloLr2Shape + (1,)
        X2_test = self.assert_reshape(X2_test,X2_test_shape)
        if self.usage == 'OFFLINE':
            # --Get Test Data
            X1_test = self.data.X1_Test
            X3_test = self.data.X3_Test
            # -- Check shapes are fine
            X1_test_shape = (X1_test.shape[0],) + self.CaloLr1Shape + (1,)
            X3_test_shape = (X3_test.shape[0],) + self.CaloLr3Shape + (1,)
            X1_test = self.assert_reshape(X1_test,X1_test_shape)
            X3_test = self.assert_reshape(X3_test,X3_test_shape)

            X_test = {'Input1': X1_test, 'Input2': X2_test, 'Input3':X3_test}
        elif self.usage == 'HLT':
            X_test = X2_test
        else:
            raise RuntimeError("No testing data available")
        return X_test

    def _get_predictions(self, model):
        # -- Get predictions from model
        X_Test = self._get_Xtest()
        Y_pred = model.predict(X_Test)
        Y_pred = Y_pred.reshape(Y_pred.shape[0],1)
        return Y_pred

    def _get_predicted_WPs(self):
        """
        Gets the WorkingPoints values for the dataset

        # Args::None

        # Returns::
            WP_pred: (dict) keys are `usage`, it stores the WorkingPoints data as NumpyArrays.
        """
        WP_pred = {}
        WP_pred['Tight'] = self.data.Z_Test['isEMTightIncOff'].to_numpy();
        if self.usage == 'HLT':
            # NOTE: If usage is HLT then for now you need the following variables to get the WP
            ph_pT  = self.data.Z_Test['pt'].to_numpy()
            ph_eta = self.data.Z_Test['eta'].to_numpy()
            Rhad   = self.data.Z_Test['Rhad'].to_numpy()
            Reta   = self.data.Z_Test['Reta'].to_numpy()
            f1     = self.data.Z_Test['f1'].to_numpy()
            Eratio = self.data.Z_Test['Eratio'].to_numpy()

            # -- Get array of WP cut results for data.
            HLT_pred = self.get_Lr2HLT_passcut(
                    ph_eta=ph_eta,
                    ph_pT=ph_pT,
                    ph_Reta=Reta,
                    ph_Rhad=Rhad,
                    ph_Eratio=Eratio
                    )
            WP_pred['HLT_FastReco'] = HLT_pred;
            # -- I'm not sure if this is deletion is necessary.
            del ph_pT, ph_eta, Rhad, Reta, f1, Eratio
        return WP_pred

    def set_input(self,model):
        # -- Set variables for execution
        Y_true = self.data.Y_Test
        Y_pred = self._get_predictions(model)
        WP_pred = self._get_predicted_WPs()
        return Y_true, Y_pred, WP_pred

    def compute_SigBkg_Eff_MCdata(self, y_true, z_true):
        """
        y_ture: (NumpyArray), labels true value
        z_true: (NumpyArray), WorkingPoints true values.

        ## Returns:

            Sig_Eff: of the WPs

            Bkg_Rej: of the WPs
        """
        TP,TN,FP,FN = _optimized_masking_confusion_mat(y_true,z_true)
        Sig, Bkg = Mod._fix_ZeroDiv(TP,TN,FP,FN)
        return Sig, Bkg

    def get_Lr2HLT_passcut(self, ph_eta, ph_pT, ph_Reta, ph_Rhad, ph_Eratio):
        """ wrapper to call the optimized `get_Lr2HLT_passcut_fast` method """
        out = get_Lr2HLT_passcut_fast(
                self.ETA, ph_eta, ph_pT,
                ph_Reta, ph_Rhad, ph_Eratio)
        return out

    # -------------------- #
    def _compute_ALL(self, y_true, y_pred, wp_pred, wp_dict, roc_df, auc_dict, bins=None):
        """
        computes the ROC curve and its AUC,
        it appends the respective WPs values

        # Args::
            wp_pred: (dict), contains the data we need to compute the WPs.
            wp_dict: (dict), assuming it has this shape: {<usage>:{'Sig_Eff':[] or float?}},
                it's the container where the binned WP result will be saved.

        # Returns::
            roc_df: (PandasDataFrames)
            auc_dict: (dict)
        """
        for usage in wp_dict.keys():
            wp_dict[usage]['Sig_Eff'] += self.compute_SigBkg_Eff_MCdata(
                    y_true=y_true,
                    z_true=wp_pred[usage]
                    )[0]
            wp_dict[usage]['Bkg_Rej'] += self.compute_SigBkg_Eff_MCdata(
                    y_true=y_true,
                    z_true=wp_pred[usage]
                    )[1]
        # -- Compute the ROC data
        roc_df, auc_dict = Mod.compute_ROC(
                y_true      = y_true,
                y_pred      = y_pred,
                roc_df      = roc_df,
                auc_dict    = auc_dict
                )

        return roc_df, auc_dict

    # TODO: profile and check if numba can improve this method.
    def _mat_additem(self, matrix, auc_data, row, col):
        elem = [
                auc_data['ROC_AUC_ErrL'],
                auc_data['ROC_AUC'],
                auc_data['ROC_AUC_ErrH']
                ]
        # NOTE
        # we use a += because in the case the matrix is already filled we don't want to
        # delete its content, we will later take the average
        matrix[row,col] += elem

    def _get_masked_arr(self,y_true,y_pred,wp_pred,etabin,ptbin):
        """
        Applies a cut in eta and pT to mask the containers so that we only use the data that belongs to (etabin,ptbin).

        # Args::
            y_true:
            y_pred:
            wp_pred:
            etabin:
            ptbin:

        # Returns::
            Y_true_masked:
            Y_pred_masked:
            WP_pred_masked: (dict), for each usage it holds the WorkingPoints values according
            to the (etabin,ptbin) being consider.
        """
        WP_pred_masked={}
        mask = Mod._pT_Eta(
                PT=self.PT,
                ETA=self.ETA,
                ptbin=ptbin,
                etabin=etabin,
                data=self.data,
                )

        # -- Set masked variables
        Y_true_masked= y_true[mask];
        Y_pred_masked= y_pred[mask];
        WP_pred_masked['Tight'] = wp_pred['Tight'][mask];
        if self.usage == 'HLT':
            WP_pred_masked['HLT_FastReco'] = wp_pred['HLT_FastReco'][mask];

        return Y_true_masked, Y_pred_masked, WP_pred_masked

    def execute(self, Y_true, Y_pred, WP_pred):
        """
        Executes all computation for each file (subsample) of data.
        It first computes in "Inclusive" mode and then in "Binned".

        # Args::
            Y_true:
            Y_pred:
            WP_pred:
            verbose:

        # Returns:: None
        """
        # -- Inclusive
        log.info(" --> Executing Inclusive ")
        self.ROC_dict['Inclusive'], self.AUC_dict['Inclusive'] = self._compute_ALL(
                Y_true, Y_pred, WP_pred,
                self.WP_dict['Inclusive'],
                self.ROC_dict['Inclusive'],
                self.AUC_dict['Inclusive']
                )
        # FIXME
        print(" *** Testing scanEff ***")
        # print(Y_true.shape[0],type(Y_true))
        CNN_SigEff_Tight, CNN_BkgRej_Tight, CNN_cut = Mod.scanEff(
                y_true=Y_true,
                y_pred=Y_pred,
                # y_true=np.asarray(Y_true),
                # y_pred=np.asarray(Y_pred),
                true_Sig_Eff=WP_pred['Tight'],
                )
        # -- Binned
        WP_pred_masked={}
        for (etabin, eta) in enumerate(self.ETA):
            if self.verbose: print(' --> reading eta bin',etabin)
            if self._crack_cond(eta):
                log.warning(f'** Crack Region -> skipping bin {etabin} **')
                continue
            for (ptbin, pt)  in enumerate(self.PT):
                if etabin+1 >= len(self.ETA) or ptbin+1 >= len(self.PT):
                    continue
                if self.verbose: print(' --> Masking ({etabin};{ptbin})')
                Y_true_masked, Y_pred_masked, WP_pred_masked = self._get_masked_arr(
                        Y_true, Y_pred, WP_pred,
                        etabin, ptbin
                        )

                # -- Adress some Issues:
                if self._not_enough_phs(
                        Y_true_masked,
                        eta, self.ETA[etabin+1],
                        pt, self.PT[ptbin+1]
                        ):
                    continue
                if self._not_enough_representation(Y_true_masked):
                    continue

                log.info(f' --> Filling AUC matrix for ptbin={ptbin}, etabin={etabin}')
                if self.verbose:
                    print(f' --> Filling AUC matrix for ptbin={ptbin}, etabin={etabin}')
                # -- Compute WPs efficiency for each (pT,eta) bin
                self.ROC_dict[f'({eta};{pt})'], self.AUC_dict[f'({eta};{pt})'] = self._compute_ALL(
                        Y_true_masked, Y_pred_masked, WP_pred_masked,
                        self.WP_dict[f'({eta};{pt})'],
                        self.ROC_dict[f'({eta};{pt})'],
                        self.AUC_dict[f'({eta};{pt})']
                        )

                self._mat_additem(
                        matrix=self.Auc_Inc,
                        auc_data=self.AUC_dict[f'({eta};{pt})'],
                        row=ptbin, col=etabin
                        )

                # print(f"Sig Eff : {CNN_Sig_Eff_Tight}, Bkg Rej : {CNN_Bkg_Eff_Tight}, Cut : {CNN_cut}, ATLAS Eff : {Sig_Eff_Tight}, ATLAS Rej : {Bkg_Eff_Tight}")
                # FIXME
                # Z, X, S, B = Mod.maxEff(y_true=Y_true, y_pred=Y_pred)
                # P.plot_scan(Z, X, S, B, 'Inclusive',self.model_dir)


    # -- SAVING METHODS -- #
    def _save_SigBkg_Eff_MCdata(self, Sig, Bkg, WP_label, sample_tag, bins=None):
        """"""
        outdir = f'{self.roc_dir}/{sample_tag}/saved_data'
        try:
            os.makedirs(outdir)
        except FileExistsError:
            pass

        if bins is None:
            fname = os.path.join(outdir,f'full_{WP_label}')
        else:
            fname = os.path.join(
                    outdir,
                    'eta{eta:1.2f}xpT{pt:1.1f}_{label}'.format(
                        eta=bins[0],
                        pt=bins[1],
                        label=WP_label
                        )
                    )

        np.save(arr=Sig,file=f'{fname}_SigEff',allow_pickle=True)
        np.save(arr=Bkg,file=f'{fname}_BkgRej',allow_pickle=True)

    def _save_ROC(self, save_DF, sample_tag, bins=None):
        outdir = f'{self.roc_dir}/{sample_tag}/saved_data'
        if bins is None:
            fname = os.path.join(outdir,'full_outROC')
        else:
            fname = os.path.join(
                    outdir,
                    'eta{eta:1.2f}xpT{pt:1.1f}_outROC'.format(
                        eta=bins[0],
                        pt=bins[1],)
                    )
        Mod.save_data(
                data=save_DF,
                out_file_path=fname,
                fmt='NPY'
                )

    def save_AUC_mat(self, mat, sample_tag):
        outdir = f'{self.roc_dir}/{sample_tag}/saved_data'
        fname = os.path.join(outdir,f'BinnedAUC_Matrix.npy')
        np.save(arr=mat,
                file=fname,
                allow_pickle=True)

    def save_results(self, wp_dict, roc_df, sample_tag, bins):
        for usage in wp_dict.keys():
            self._save_SigBkg_Eff_MCdata(
                    Sig = wp_dict[usage]['Sig_Eff'],
                    Bkg = wp_dict[usage]['Bkg_Rej'],
                    WP_label=usage,
                    sample_tag=sample_tag,
                    # OutDir=self.roc_dir,
                    bins=bins
                    )
        # -- Save ROC data to binaries
        self._save_ROC(
                save_DF = roc_df,
                sample_tag = sample_tag,
                bins = bins,
                )

    def save_ALL(self, sample_tag):
        self.save_results(
                wp_dict=self.WP_dict['Inclusive'],
                roc_df=self.ROC_dict['Inclusive'],
                sample_tag= sample_tag,
                bins=None
                )
        for (etabin, eta) in enumerate(self.ETA):
            for (ptbin, pt)  in enumerate(self.PT):
                if self.nf2r > 1:
                    self.average_ROC_results(
                            roc_df = self.ROC_dict[f'({eta};{pt})'],
                            num_subsamples = num_subsamples
                            )
                if self.ROC_dict[f'({eta};{pt})'].shape[0] == 0:
                    continue

                log.info(f" --> Saving ({eta};{pt}) data ")
                if self.verbose:
                    print(f" --> Saving ({eta};{pt}) data ")
                self.save_results(
                        wp_dict=self.WP_dict[f'({eta};{pt})'],
                        roc_df=self.ROC_dict[f'({eta};{pt})'],
                        sample_tag=sample_tag,
                        bins=(eta,pt)
                        )
        # -- Save the AUC_Matrix
        log.info(f" --> Saving AUC_Matrix ")
        if self.verbose:
            print(f" --> Saving AUC_Matrix ")
        self.save_AUC_mat(
                mat=self.Auc_Inc,
                sample_tag=sample_tag
                )

    def plot_ALL(self, Y_true, Y_pred, WP_pred, sample_tag, AUC_plotsty='heatmap'):
        # -- ROC curves
        log.info(" --> Ploting Inclusive ROC curve")
        if self.verbose: print(" --> Ploting Inclusive ROC curve")
        P.plot_roc_curve(
                roc_data=self.ROC_dict['Inclusive'],
                auc_dict=self.AUC_dict['Inclusive'],
                wp_dict=self.WP_dict['Inclusive'],
                sample_tag=sample_tag,
                OutDir=self.roc_dir,
                )
        for (etabin, eta) in enumerate(self.ETA):
            for (ptbin, pt)  in enumerate(self.PT):
                if self.ROC_dict[f'({eta};{pt})'].shape[0] == 0:
                    continue

                P.plot_roc_curve(
                        roc_data=self.ROC_dict[f'({eta};{pt})'],
                        auc_dict=self.AUC_dict[f'({eta};{pt})'],
                        wp_dict=self.WP_dict[f'({eta};{pt})'],
                        sample_tag=sample_tag,
                        OutDir=self.roc_dir,
                        bins=(eta,pt)
                        )

        # -- Sig&Bkg Histograms for CNN
        P.plot_CNN_output(
                y_true = Y_true,
                y_pred = Y_pred,
                sample_tag = 'MC_data',
                OutDir = f'{self.histo_dir}',
                )

        # -- Set plot Labels for AUC matrix
        EtaLabels = ['[{0:.2f},{1:.2f})'.format(self.ETA[i],self.ETA[i+1])\
                for i in range(len(self.ETA)-1)]
        PTLabels = ['[{0:.0f},{1:.0f})'.format(self.PT[i],self.PT[i+1])\
                for i in range(len(self.PT)-1)]

        # -- AUC Matrix
        P.plot_AUC_matrix(
                data=self.Auc_Inc,
                row_labels=PTLabels,
                col_labels=EtaLabels,
                title='AUC_Mode2',
                plotstyle=AUC_plotsty,
                cbarlabel='norm mean AUC',
                OutDir=f'{self.roc_dir}/Inclusive/plots',
                )

        # TODO: implement
        # P.plot_matrix(
                # matrix=Sig_Inc,
                # title='Sig_Eff_Inclusive',
                # OutDir=f'{self.roc_dir}/Inclusive/plots')
        # plot_matrix(CNN_Bkg_Inc, 'CNN_Bkg_Rej_Inclusive')
        # plot_matrix(CNN_Sig_Inc, 'CNN_Sig_Eff_Inclusive')
        # plot_matrix(Bkg_Imp_Inc, 'Bkg_Imp_Inclusive')
        # plot_matrix(Sig_Imp_Inc, 'Sig_Imp_Inclusive')


def main():
    # -- Config logging
    log.basicConfig(
            filename='validation_run.log',
            filemode='w',
            level=log.INFO
            )
    # logger = log.getLogger(__name__)
    log.info(" --> Starting Validation")
    val = Validation()
    # -- Load model
    model = val.load_model()
    num_subsamples = val.nf2r

    for chk in range(0, val.nf2r):
        log.info(f'--> loading file {chk + 1}/{val.nf2r}')
        if val.verbose: print(f'--> loading file {chk + 1}/{val.nf2r}')
        # --------------- #
        val.load_data(chunk=chk)
        Y_true, Y_pred, WP_pred = val.set_input(model)
        val.execute(Y_true, Y_pred, WP_pred)

    log.info(" --> Saving all data ")
    val.save_ALL(sample_tag='Inclusive')
    log.info(" --> Plotting all data ")
    val.plot_ALL(Y_true, Y_pred, WP_pred, sample_tag='Inclusive')
    log.info(" --> ** Successfull Validation ended **")
    if val.verbose:
        print(" --> ** Successfull Validation ended **")

if __name__ == '__main__':
    main()


    # log.info(" --> averaging results over chunks")
    # if val.nf2r > 1:
        # val.average_ROC_results(
                # roc_df=val.ROC_dict['Inclusive'],
                # num_subsamples=num_subsamples
                # )
        # val.normalice_AUC_results(
                # AUC_Matrix=val.Auc_Inc,
                # num_subsamples=num_subsamples
                # )

    # def average_ROC_results(self, roc_df, num_subsamples):
        # # -- Compute the mean between values with same index
        # roc_df = roc_df.groupby(
                # by=roc_df.index,
                # level=0
                # ).mean()
        # # -- std should be mean(std)/sqrt(num_chunks)
        # roc_df.loc[
                # ['ROC_fprErrL','ROC_fprErrH','ROC_tprErrL','ROC_tprErrH']
                # ]/=np.sqrt(num_subsamples)

    # def normalice_AUC_results(self, AUC_matrix, num_subsamples):
        # """
        # Assuming AUC_matrix is filled with AUC data from 'num_subsamples' different files.
        # This method normilices the values to be interpreted as:
        # (meanAUC, lowerStdAUC, higerStdAUC)
        # """
        # # first element shuld be the mean, at this stage we are missing the normalization so.
        # AUC_matrix[:,:,0] /= num_subsamples
        # # second and theird shuold be the standar deviation lower and upper limits
        # AUC_matrix[:,:,1:] /= np.sqrt(num_subsamples)

        # return AUC_matrix


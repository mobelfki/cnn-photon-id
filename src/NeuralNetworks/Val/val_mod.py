#!/usr/bin/env python3
"""
Validation and Analysis Tools Module
"""
import numpy as np
import ROOT as r
import matplotlib.pyplot as plt
import pandas as pd
import sys
import os
import math
import numba
sys.path.append('../../')
import Plot.plot_mod as P
# -- Calls
# from sklearn.metrics import roc_curve, roc_auc_score #, auc
# from sklearn.metrics import roc_auc_score #, auc
from roc_w_errors import ROC_wErrorBars

# --Set global parameters
lst= ['Inc', 'UnConv', 'Conv']

def _pT_Eta(PT, ETA, ptbin, etabin, data):
    """
    Checks whether phs_pT is in [ptbin,ptbin+1] and phs_eta is in [etabin,etabin+1]

    Returns::
        mask: (bool) True if phs_pT is in [ptbin,ptbin+1] and phs_eta is in [etabin,etabin+1]
    """
    pt  = data.Z_Test['pt'].to_numpy()*1e-3 # --> to GeV
    eta = np.abs(data.Z_Test['eta'].to_numpy())

    Eta_min = ETA[etabin]
    Eta_max = ETA[etabin+1]
    # -- Set eta bounderies
    mask_ETA_min = eta > Eta_min
    mask_ETA_max = eta <= Eta_max
    ETA_mask = mask_ETA_min & mask_ETA_max

    PT_min = PT[ptbin]
    PT_max = PT[ptbin+1]
    # -- Set pT bounderies
    mask_PT_min = pt > PT_min
    mask_PT_max = pt <= PT_max
    PT_mask = mask_PT_min & mask_PT_max

    # -- Set global mask
    mask = PT_mask & ETA_mask

    return mask

def _fix_ZeroDiv(TP,TN,FP,FN):
    """
    Description

    # Args::
        TP: True Positive Rate
        TN: True Negative Rate
        FP: False Positive Rate
        FN: False Negative Rate

    # Returns::
        Sig: Signal efficiency
        Bkg: Background efficiency
    """
    try:
        Sig = float(TP/(TP+FN))     # TruePositiveRate
    except ZeroDivisionError:
        Sig = 1e-3
    try:
        Bkg = float(TN/(TN+FP))     # TrueNegativeRate
    except ZeroDivisionError:
        Bkg = 1e-3
    return Sig, Bkg

def save_data(data, out_file_path, fmt='all'):
    """
    Saves data in many formats.

    # Args::
        data: (dict-like/DataFrame)

        fmt: (str)

    # Returns:: None
    """
    formats = ['ROOT','JSON','CSV','NPY']
    if fmt == 'ROOT':
        raise RuntimeError("ROOT saving format NOT implemented yet!")
        # TODO
        # create OutTree
        # create a branch for each column
        pass
    if fmt == 'JSON':
        data.to_json(
                path_or_buf=out_file_path,
                orient='columns',
                )
    if fmt == 'NPY':
        # create an OutFolder
        try:
            os.makedirs(out_file_path)
        except FileExistsError:
            pass
        # -- Save a .npy file for each column
        for col in data.columns:
            # print(f'--> Saving {col} to NPY file')
            fname = os.path.join(out_file_path,f'{col}.npy')
            np.save(arr=data[col],
                    file=fname,
                    allow_pickle=True)

def compute_ROC(y_true, y_pred, roc_df=None, auc_dict=None):
    """
    Computes the ROC curve (tpr,fpr) from the given arrays `y_true` and `y_pred` using `roc_w_errors`.
    It also returns the AUC for said ROC.

    # Args::
        y_true: (NumpyArray), the expected value of labels. It only supports binary values {0;1}.

        y_pred: (NumpyArray), the predicted valueof the labels. It only supports binary classificationin [0.;1.].

    # Returns::
        dfROC: (PandasDataFrame)
    """
    if roc_df is None:
        roc_df = pd.DataFrame()
    if auc_dict is None:
        auc_dict = {
                'ROC_AUC':[],
                'ROC_AUC_ErrL':[],
                'ROC_AUC_ErrH':[]
                }

    # -- Compute ROC and AUCROC
    roc_obj = ROC_wErrorBars(
            y_true=y_true,
            y_pred=y_pred,
            )
    tpr, fpr, threshold = roc_obj.compute()
    auc, auc_errL, auc_errH = roc_obj.get_AUCwErrorBars()

    aux_ROC = pd.DataFrame()
    # FalsePositiveRate
    aux_ROC['ROC_fprCenter']  =fpr[0];
    aux_ROC['ROC_fprErrL']    =fpr[1];
    aux_ROC['ROC_fprErrH']    =fpr[2];
    # TruePositiveRate
    aux_ROC['ROC_tprCenter']  =tpr[0];
    aux_ROC['ROC_tprErrL']    =tpr[1];
    aux_ROC['ROC_tprErrH']    =tpr[2];
    # Threshold
    aux_ROC['ROC_thr']        =threshold;

    roc_df = roc_df.append(aux_ROC);
    del(aux_ROC)

    auc_dict['ROC_AUC']=auc;
    auc_dict['ROC_AUC_ErrL']=auc_errL;
    auc_dict['ROC_AUC_ErrH']=auc_errH;

    return roc_df, auc_dict

#### TODO ####

# FIXME
def scanEff(y_true, y_pred, true_Sig_Eff):
    """
    Description

    # Args::
        y_true

    # Returns::
    """
    P = np.arange(0.,1.0,0.001)
    X = 0
    Y = 0
    Z = 0
    for p in P:
        mask_TP = (y_true == 1)*(y_pred > p)
        mask_TN = (y_true == 0)*(y_pred <= p)
        mask_FP = (y_true == 0)*(y_pred > p)
        mask_FN = (y_true == 1)*(y_pred <= p)

        # -- Compute total of TP,TN,FP,FN in sample
        TP = y_true[mask_TP].shape[0]
        TN = y_true[mask_TN].shape[0]
        FP = y_true[mask_FP].shape[0]
        FN = y_true[mask_FN].shape[0]

        Sig_Eff = float(TP/(TP+FN))
        Bkg_Rej = float(TN/(TN+FP))

        if Sig_Eff < 0.001 or Bkg_Rej < 0.001 :
            continue

        # return_cond = abs(float(format(true_Sig_Eff, '.3f')) - float(format(Sig_Eff, '.3f')))
        # # print(return_cond)
        # # TODO, NOTE : what does this thresholds mean
        # if return_cond < 0.01:
            # return Sig_Eff, Bkg_Rej, p
        # # if return_cond < 0.02:
        # elif return_cond < 0.02:
            # X = Sig_Eff;
            # Y = Bkg_Rej;
            # Z = p

    # return X, Y, Z

        # TODO test if this works
        if abs(float(format(true_Sig_Eff, '.3f')) - float(format(Sig_Eff, '.3f'))) < 0.01:
                return Sig_Eff, Bkg_Eff, p
        if abs(float(format(true_Sig_Eff, '.3f')) - float(format(Sig_Eff, '.3f'))) < 0.02:
                X = Sig_Eff; Y = Bkg_Eff; Z = p
    return X, Y, Z

# FIXME
def maxEff(y_true, y_pred, sig_eff_thr=0.9):
    """
    """
    # P:: threshold step array
    P = np.arange(0.,1.0,0.001)
    # S:: significance
    S = []
    X = []
    Bkg= []
    Sig= []
    for p in P:
        # --Mask conditions for confuison matrix
        mask_TP = (y_true == 1)*(y_pred > p)
        mask_TN = (y_true == 0)*(y_pred <= p)
        mask_FP = (y_true == 0)*(y_pred > p)
        mask_FN = (y_true == 1)*(y_pred <= p)

        TP = y_true[mask_TP].shape[0]
        TN = y_true[mask_TN].shape[0]
        FP = y_true[mask_FP].shape[0]
        FN = y_true[mask_FN].shape[0]

        Sig_Eff = float(TP/(TP+FN))
        Bkg_Rej = float(TN/(TN+FP))

        N_Sig = float(Sig_Eff*(TP+FN))
        N_Bkg = float((1-Bkg_Rej)*(TN+FP))

        if N_Sig < 1.:
                continue
        if N_Bkg < 1.:
                continue
        if Sig_Eff < sig_eff_thr:
                continue

        s = N_Sig/math.sqrt(N_Bkg+N_Sig)

        S.append(s)
        X.append(p)
        Bkg.append(Bkg_Rej)
        Sig.append(Sig_Eff)

    # -- Save to NPY format file
    return np.array(S), np.array(X), np.array(Sig), np.array(Bkg)

# TODO move this to Plot/ once it is working
def plot_scan(Z, X, S, B, sample_tag, OutDir):
    max_Z = Z.argmax()
    plt.figure()
    plt.plot(X,Z)
    plt.plot(X[max_Z], Z[max_Z], 'or')
    plt.legend(['Scan', 'WP ('+str(format(X[max_Z], '.3f'))+','+str(format(Z[max_Z], '.3f'))+')'])
    plt.xlabel('probability cut')
    plt.ylabel('Significance S/sqrt(S+B)')
    fname = os.path.join(OutDir,f'Z_{sample_tag}.png')
    plt.savefig(fname)
    plt.close()

    plt.figure()
    plt.plot(X,S)
    plt.plot(X[max_Z], S[max_Z], 'or')
    plt.legend(['Scan', 'WP ('+str(format(X[max_Z], '.3f'))+','+str(format(S[max_Z], '.3f'))+')'])
    plt.xlabel('probability cut')
    plt.ylabel('Sig Eff')
    fname = os.path.join(OutDir,f'S_{sample_tag}.png')
    plt.savefig(fname)
    plt.close()

    plt.figure()
    plt.plot(X,B)
    plt.plot(X[max_Z], B[max_Z], 'or')
    plt.legend(['Scan', 'WP ('+str(format(X[max_Z], '.3f'))+','+str(format(B[max_Z], '.3f'))+')'])
    plt.xlabel('probability cut')
    plt.ylabel('Bkg rejection')
    fname = os.path.join(OutDir,f'B_{sample_tag}.png')
    plt.savefig(fname)
    plt.close()

##############

#### UNUSED ####

def merge_TVT(model, usage, data):
    X2_Test = data.Img_Lr2_Test
    X2_Train = data.Img_Lr2_Train
    X2_Val = data.Img_Lr2_Val

    if usage == 'OFFLINE':
        X1_Test = data.Img_Lr1_Test
        X3_Test = data.Img_Lr3_Test

        X1_Train = data.Img_Lr1_Train
        X3_Train = data.Img_Lr3_Train

        X1_Val = data.Img_Lr1_Val
        X3_Val = data.Img_Lr3_Val

        X1_Test, X2_Test, X3_Test    = reshape(X1_Test, X2_Test, X3_Test)
        X1_Train, X2_Train, X3_Train = reshape(X1_Train, X2_Train, X3_Train)
        X1_Val, X2_Val, X3_Val       = reshape(X1_Val, X2_Val, X3_Val)

        pred_test  = model.predict([X1_Test,X2_Test,X3_Test]);
        pred_train = model.predict([X1_Train,X2_Train,X3_Train]);
        pred_val   = model.predict([X1_Val,X2_Val,X3_Val]);

    else:
        pred_test  = model.predict(X2_Test);
        pred_train = model.predict(X2_Train);
        pred_val   = model.predict(X2_Val);


    plt.figure()

    plt.hist(pred_train, bins = 50, histtype='step',  density='True')
    plt.hist(pred_test, bins = 50, histtype='step',  density='True')
    plt.hist(pred_val, bins = 50, histtype='step',  density='True')

    plt.legend(['Train', 'Test', 'Validation'])
    plt.savefig(OutDir+'model_merge_TVT.png')
    plt.close()

def reshape(X1, X2, X3):
    X1 = X1.reshape(X1.shape[0], 2, 56, 1)
    X2 = X2.reshape(X2.shape[0], 11, 7, 1)
    X3 = X3.reshape(X3.shape[0], 11, 4, 1)
    return X1, X2, X3



#set input '/home/santi_noacco/Desktop/PhD/QualificationTask/BelfkirCode/test/outputs/Array/' 

#./val_offline.py -I '/home/santi_noacco/Desktop/PhD/QualificationTask/BelfkirCode/test/outputs/Array/' -O 'output' -T 'HLT'
#datainput '/home/santi_noacco/Desktop/PhD/QualificationTask/BelfkirCode/test/outputs/Array/' 
#./val_offline.py -I '/home/santi_noacco/Desktop/PhD/QualificationTask/BelfkirCode/test/outputs/Array/' -O 'output' -p '/home/santi_noacco/Desktop/PhD/QualificationTask/BelfkirCode/test/outputs/Train/saved_models' -CNNn 64 -DNNn 32
#./val.py -I '/home/santi_noacco/Desktop/PhD/QualificationTask/BelfkirCode/test/outputs/Array/' -O 'output' -U 'HLT' -P '../Train/output/HLT/stg_1/2020-07-27_64x2CL_32x2DL_0.08dr_30Ep_1.0e-04lr/saved_models/checkpoints/Model_Ep_23-0.52.weights'
#./val.py -I '/home/santi_noacco/Desktop/PhD/QualificationTask/BelfkirCode/test/outputs/Array/' -O 'output' -U 'HLT' -P '../Train/output/HLT/stg_1/2020-07-27_64x2CL_32x2DL_0.08dr_30Ep_1.0e-04lr/saved_models/checkpoints/Model_Ep_23-0.52.weights' -nCK 64 -nDK 32
#./val.py -I '/home/santi_noacco/Desktop/PhD/QualificationTask/BelfkirCode/test/outputs/Array/' -O 'output' -U 'HLT' -P '../Train/output/HLT/stg_1/2020-08-05_128x2CL_256x2DL_0.08dr_15Ep_1.0e-04lr/saved_models/checkpoints/Model_Ep_06-0.49.weights' -nCK 128 -nDK 256 -nCL 2 -nDL 2
#./val.py -I 'datainput/' -O 'output' -U 'HLT' -P 'trainoutput/HLT/stg_1/2020-08-06_16x1CL_32x1DL_0.08dr_15Ep_1.0e-04lr/saved_models/checkpoints/Model_Ep_12-0.55.weights' -nCK 16 -nCL 1 -nDK 32 -nDL 1 -LFM 'False'
#./val.py -I 'datainput/' -O 'output' -U 'HLT' -P 'trainoutput/HLT/stg_1/2020-08-06_16x1CL_32x1DL_0.08dr_15Ep_1.0e-04lr/saved_models' -nCK 16 -nCL 1 -nDK 32 -nDL 1 -LFM 0
#./val.py -I 'datainput/' -O 'output' -U 'HLT' -P 'trainoutput/HLT/stg_1/2020-08-06_16x1CL_32x1DL_0.08dr_15Ep_1.0e-04lr/saved_models/model.json' -LFM 0 -Pwgt 'trainoutput/HLT/stg_1/2020-08-06_16x1CL_32x1DL_0.08dr_15Ep_1.0e-04lr/saved_models/checkpoints/'
#./val.py -I 'datainput/' -O 'output' -U 'HLT' -P 'trainoutput/HLT/stg_1/2020-08-12_32x1CL_64x1DL_0.08dr_30Ep_1.0e-04lr/saved_models/model.json' -Pwgt 'trainoutput/HLT/stg_1/2020-08-12_32x1CL_64x1DL_0.08dr_30Ep_1.0e-04lr/saved_models/checkpoints/weights.Ep20-val_loss0.51.h5' -LFM 0

#./val.py -I 'datainput/' -O 'output' -U 'HLT' -P 'trainoutput/HLT/stg_1/2020-08-13_32x1CL_64x1DL_0.08dr_3Ep_1.0e-04lr/saved_models/checkpoints/weights.Ep01-val_loss0.56/' -LFM 1

# Testing Validation with Keras Tuner Saved models
#./val.py -I 'datainput/' -O 'output' -U 'HLT' -P 'trainoutput/HLT/stg_1/2020-08-14_dynamic/saved_models/CNN_trials/trial_90135dfae7ac08051116c0f1d896d8a5/checkpoints/' -LFM 1
#./val.py -I 'datainput/' -O 'output' -U 'OFFLINE' -P 'trainoutput/OFFLINE/stg_1/Stg1_OFFLINECNN_Nep_3_CNNn_32_DNNn_128_lr_0.0001_dr_0.08_bt_128/saved_models/' -LFM 0

# TEST ON RUNNING AND SAVING
# --> HLT WGTS ONLY (SUCCESSFUL)
#./val.py -I 'datainput/' -O 'output' -U 'HLT' -Pwgt 'trainoutput/HLT/stg_1/2020-09-24_16x1CL_32x1DL_0.08dr_3Ep_1.0e-04lr/saved_models/checkpoints/weights.Ep01-val_loss0.59.h5' -P 'trainoutput/HLT/stg_1/2020-09-24_16x1CL_32x1DL_0.08dr_3Ep_1.0e-04lr/saved_models/model.json' -LFM 0

# --> OFFLINE WGTS ONLY ()
./val.py -I 'datainput/' -O 'output' -U 'OFFLINE' -Pwgt 'trainoutput/OFFLINE/stg_1/20201001_256x2CL_128x2DL_0.08dr_2Ep_1.0e-04lr/saved_models/checkpoints/weights.Ep02-val_loss0.90.h5' -P 'trainoutput/OFFLINE/stg_1/20201001_256x2CL_128x2DL_0.08dr_2Ep_1.0e-04lr/saved_models/model.json' -LFM 0


# --> HLT FULL MODEL()
#./val.py -I 'datainput/' -O 'output' -U 'HLT' -P 'trainoutput/HLT/stg_1/20201007_16x1CL_32x1DL_0.08dr_50Ep_1.0e-04lr/saved_models/checkpoints/weights.Ep29-val_loss0.53/' -LFM 1


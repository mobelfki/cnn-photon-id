 #!/usr/bin/env sh


NTUPLE_IN="/home/projects/test/outputs/ZllAthDerivation/NewPhotonIDSamples_FF/"     # this expects to hold two dirs: Bkg/ and Sig/
NTUPLE_OUT="${GLOBAL_OUTDIR}/NTuple"

# strategy
STG=1
# first event to read
FSTEV=0
# max number of events
MAXEVS=0    # same as ALL events
# -- Run NTuple for Signal
./src/NTuple/runCleanAndGet "${NTUPLE_IN}/Sig" "${NTUPLE_OUT}/Sig" $STG $FSTEV $MAXEVS GammaJet &&
# -- Run NTuple for Background
./src/NTuple/runCleanAndGet "${NTUPLE_IN}/Bkg" "${NTUPLE_OUT}/Bkg" $STG $FSTEV $MAXEVS DiJet &&

echo " --->> NTuple output generated";


# TODO: remove commmented lines
#set INPUTDIR=~/projects/test/outputs/ZllAthDerivation/NewPhotonIDSamples_FF;
#set OUTPUTDIR=~/projects/cnn-photon-id/test/outputs/NTuple;

#echo ${OUTPUTDIR};

 #-- Get Signal data
#runCleanAndGet /eos/user/m/mobelfki/NewPhotonIDSamples/Sig ${OUTPUTDIR}/Sig 1 0 500000 GammaJet
#runCleanAndGet /eos/user/m/mobelfki/NewPhotonIDSamples/Sig ~/projects/cnn-photon-id/test/outputs/NTuple/Sig 1 0 500000 GammaJet
#runCleanAndGet /eos/user/m/mobelfki/NewPhotonIDSamples/Sig ~/projects/cnn-photon-id/test/outputs/NTuple/Sig 1 0 1000000 GammaJet
#runCleanAndGet /eos/user/m/mobelfki/NewPhotonIDSamples/Sig /eos/user/m/mobelfki/MLPhotonNTuple 1 0 0 GammaJet

# -- Get Background data
#runCleanAndGet ~/projects/test/outputs/ZllAthDerivation/NewPhotonIDSamples_FF/Bkg ~/projects/cnn-photon-id/test/outputs/NTuple/local/Bkg 1 0 500000 DiJet
#runCleanAndGet /eos/user/m/mobelfki/NewPhotonIDSamples/Bkg ~/projects/cnn-photon-id/test/outputs/NTuple/Bkg 1 0 0 DiJet
#runCleanAndGet /eos/user/m/mobelfki/NewPhotonIDSamples/Bkg ~/projects/cnn-photon-id/test/outputs/NTuple/Bkg 1 0 10000000 DiJet
#runCleanAndGet /eos/user/m/mobelfki/NewPhotonIDSamples/Bkg /eos/user/m/mobelfki/MLPhotonNTuple 1 0 0 DiJet



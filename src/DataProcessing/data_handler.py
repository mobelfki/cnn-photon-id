#!/usr/bin/env python3
#=============#
# Author: S. Noacco Rosende; M. Belfkir
# Contact: snoaccor@cern.ch
# GitLab:https://gitlab.cern.ch/snoaccor
# GitHub:https://github.com/santinoacco
#=============#
import numpy as np
import pandas as pd
import os

class Data(object):
    """
    Data class for data pre-processing
    Training type CNN
    usage: (string), either 'OFFLINE' or 'HLT', intended use of the CNN model.
    OFFLINE uses the 3 layers of EMCalo, HLT only layer 2.
    """
    def __init__(
            self, stg=1, usage='OFFLINE', data_dir=None,
            ):
        """
        Description

        # Args::
            stg:
            usage:
            data_dir:

        # Returns::
        """
        self.data_dir = data_dir;
        self.usage = usage;
        self.stg = stg;

    def _getName(self,treename,sample,var,chunk,fmt):
        """
        # Retruns the file name at '{self.data_dir}/{self.stg}/{treename}/{sample}_IsoTight_{var}.{fmt}'
        Retruns the file name at '{self.data_dir}/{self.stg}/{treename}/{sample}_IsoTight_{var}_{chunk}.{fmt}'
        works with NTupleToImg output

        # Args::

        """
        path = f'{self.data_dir}/stg_{self.stg}/{treename}/'
        filename = f'{sample}_IsoTight_{var}_{chunk}.{fmt}'
        out = os.path.join(path,filename)
        return out

    def _appendSamples(self,treename,var,chunk,fmt):
        """
        Appends the Signal 'var' data from 'treename' with its respective Background 'var'

        # Args::
            treename: (str) the name of the TTree dir in NTupleToImage output to load files from.
            var: (str) name of the variable in the file to load
            fmt: (str) format of the file to load, it is use as the file extension.
                If fmt is 'npy' then it loads a numpy array,
                if fmt is 'h5' then it loads a pandas DataFrame.

        # Returns::
            out_var: (obj) contains date of sample Signal and Background of the same type,
                which depends on the 'fmt' argument.
        """
        filepath_sig = self._getName(treename,'GammaJet',var,chunk,fmt)
        filepath_bkg = self._getName(treename,'DiJet',var,chunk,fmt)

        if fmt == 'npy':
            sig_arr = np.load(filepath_sig, allow_pickle=True)
            bkg_arr = np.load(filepath_bkg, allow_pickle=True)
            out_var = np.append(
                    arr=sig_arr,
                    values=bkg_arr,
                    axis=0,
                    )
        elif fmt == 'h5':
            sig_Z_df = pd.read_hdf(filepath_sig, f'Z_{chunk}')
            bkg_Z_df = pd.read_hdf(filepath_bkg, f'Z_{chunk}')
            out_var = sig_Z_df.append(
                    bkg_Z_df,
                    ignore_index=True,
                    verify_integrity=True
                    )
        return out_var

    def loadTrainData(self,chunk=0):
        """
        Loads all necessary data for a given usage to do the training,
        from an entire file.

        # Args::
            chunk: (int), chunk file to read and load

        # Returns:: None
        """
        print(f'Loading Training Data for {self.usage} usage ...')
        self.X2_Train = self._appendSamples(treename="Tree_Train",var="Lr2Img",fmt="npy",chunk=chunk)
        self.Y_Train = self._appendSamples(treename="Tree_Train",var="Y",fmt="npy",chunk=chunk)
        self.Z_Train = self._appendSamples(treename="Tree_Train",var="Z",fmt="h5",chunk=chunk)
        if self.usage == 'OFFLINE':
            self.X1_Train = self._appendSamples(treename="Tree_Train",var="Lr1Img",fmt="npy",chunk=chunk)
            self.X3_Train = self._appendSamples(treename="Tree_Train",var="Lr3Img",fmt="npy",chunk=chunk)

    def loadValData(self,chunk=0):
        """
        Loads all necessary data for a given usage to do the validation,
        from an entire file.

        # Args::
            chunk: (int), chunk file to read and load

        # Returns:: None
        """
        print(f'Loading Validation Data for {self.usage} usage ...')
        self.X2_Val = self._appendSamples(treename="Tree_Val",var="Lr2Img",fmt="npy",chunk=chunk)
        self.Y_Val = self._appendSamples(treename="Tree_Val",var="Y",fmt="npy",chunk=chunk)
        self.Z_Val = self._appendSamples(treename="Tree_Val",var="Z",fmt="h5",chunk=chunk)
        if self.usage == 'OFFLINE':
            self.X1_Val = self._appendSamples(treename="Tree_Val",var="Lr1Img",fmt="npy",chunk=chunk)
            self.X3_Val = self._appendSamples(treename="Tree_Val",var="Lr3Img",fmt="npy",chunk=chunk)

    def loadTestData(self,chunk=0):
        """
        Loads all necessary data for a given usage to do the testing,
        from an entire file.

        # Args::
            chunk: (int), chunk file to read and load

        # Returns:: None
        """
        print(f'Loading Test Data for {self.usage} usage ...')
        self.X2_Test = self._appendSamples(treename="Tree_Test",var="Lr2Img",fmt="npy",chunk=chunk)
        self.Y_Test = self._appendSamples(treename="Tree_Test",var="Y",fmt="npy",chunk=chunk)
        self.Z_Test = self._appendSamples(treename="Tree_Test",var="Z",fmt="h5",chunk=chunk)
        if self.usage == 'OFFLINE':
            self.X1_Test = self._appendSamples(treename="Tree_Test",var="Lr1Img",fmt="npy",chunk=chunk)
            self.X3_Test = self._appendSamples(treename="Tree_Test",var="Lr3Img",fmt="npy",chunk=chunk)

    def _get_begin_end(self, batch, num_chunks, batch_size, subsample_size, arr):
        """
        Yields the indeces of the slices that need to be made so that the generated subsample chunks
        fit the expected batch size.

        # Args::
            batch: (int), number of batch we are iterating.
            num_chunks: (int), total number of chunks that make the full sample.
            batch_size: (int), expected (or given) size of the batch.
            subsample_size: The size of a portion of Signal sample + Background sample from one chunk.
            arr: Signal or Background data array chunk.

        # Yields::
            slices: (tuple),
        """
        num = len(arr)*batch_size
        den = num_chunks*subsample_size
        subsample_size_file = int(num/den)
        # print(f' ---* n = {subsample_size_file}')
        _begin = int(batch*subsample_size_file)
        _end = int((batch+1)*subsample_size_file)
        yield (_begin, _end)

    # def _appendSamples_generator(self, treename, var, batch_size, chunk, fmt):
    def _appendSamples_generator(self, treename, var, fmt,
            batch, batch_size,
            chunk, num_chunks):
        """
        Appends the Signal 'var' data from 'treename' with its respective Background 'var'

        # Args::

            treename: (str) the name of the TTree dir in NTupleToImage output to load files from.
            var: (str) name of the variable in the file to load
            fmt: (str) format of the file to load, it is use as the file extension.
                If fmt is 'npy' then it loads a numpy array,
                if fmt is 'h5' then it loads a pandas DataFrame.

        # Returns::

            chunk_sample: (generator) contains data of sample Signal and Background of the same type,
                which depends on the 'fmt' argument.
        """
        filepath_sig = self._getName(treename,'GammaJet',var,chunk,fmt)
        filepath_bkg = self._getName(treename,'DiJet',var,chunk,fmt)

        if fmt == 'npy':
            # -- Load the "chunk" file for each sample.
            # with open(filepath_sig,'rb') as fsig:
                # sig_arr = np.load(
                        # fsig,
                        # allow_pickle=True,
                        # mmap_mode='r')
            # with open(filepath_bkg,'rb') as fbkg:
                # bkg_arr = np.load(
                        # fbkg,
                        # allow_pickle=True,
                        # mmap_mode='r')

            sig_arr = np.load(
                    filepath_sig,
                    allow_pickle=True,
                    mmap_mode='r')
            bkg_arr = np.load(
                    filepath_bkg,
                    allow_pickle=True,
                    mmap_mode='r')


            # -- Set chunk-sample of size "subsample_size",
            #    with the "chunk*subsample_size" elements.
            m = len(sig_arr) + len(bkg_arr)
            # print(f' ---* m: {m} ')

            _begin_s, _end_s = next(self._get_begin_end(batch, num_chunks, batch_size, m, sig_arr))
            _begin_b, _end_b = next(self._get_begin_end(batch, num_chunks, batch_size, m, bkg_arr))
            chunk_sample = np.append(
                    arr=sig_arr[_begin_s:_end_s],
                    values=bkg_arr[_begin_b:_end_b],
                    axis=0,
                    )
            # close(filepath_sig)
            # close(filepath_bkg)
        elif fmt == 'h5':
            sig_Z_df = pd.read_hdf(filepath_sig, f'Z_{chunk}')
            bkg_Z_df = pd.read_hdf(filepath_bkg, f'Z_{chunk}')
            # with open(filepath_sig,'rb') as fsig:
                # sig_Z_df = pd.read_hdf(fsig, f'Z_{chunk}')
            # with open(filepath_bkg,'rb') as fbkg:
                # bkg_Z_df = pd.read_hdf(fbkg, f'Z_{chunk}')

            _begin_s, _end_s = next(self._get_begin_end(batch, num_chunks, batch_size, sig_arr))
            _begin_b, _end_b = next(self._get_begin_end(batch, num_chunks, batch_size, bkg_arr))
            chunk_sample = sig_Z_df[_begin_s:_end_s].append(
                    bkg_Z_df,
                    start = _begin_b,
                    stop = _end_b,
                    ignore_index = True,
                    verify_integrity = True,
                    iterator = False
                    )
            # close(filepath_sig)
            # close(filepath_bkg)
        yield chunk_sample
        # -- Clean memory after retrieving chunk_sample
        del chunk_sample
        # close(filepath_sig)
        # close(filepath_bkg)

    def yieldTrainData(self,batch,batch_size,chunk,num_chunks):
        """
        Sets Training X,Y,Z as generator properties of the Data class.

        # Args::
            batch_size:
            chunk:

        # Returns::
            None
        """
        self.X2_Train = self._appendSamples_generator(
                treename="Tree_Train",
                var="Lr2Img",
                batch=batch,
                batch_size=batch_size,
                fmt="npy",
                chunk=chunk,
                num_chunks=num_chunks
                )
        self.Y_Train = self._appendSamples_generator(
                treename="Tree_Train",
                var="Y",
                batch=batch,
                batch_size=batch_size,
                fmt="npy",
                chunk=chunk,
                num_chunks=num_chunks
                )
        self.Z_Train = self._appendSamples_generator(
                treename="Tree_Train",
                var="Z",
                batch=batch,
                batch_size=batch_size,
                fmt="h5",
                chunk=chunk,
                num_chunks=num_chunks
                )
        if self.usage == 'OFFLINE':
            self.X1_Train = self._appendSamples_generator(
                    treename="Tree_Train",
                    var="Lr1Img",
                    batch=batch,
                    batch_size=batch_size,
                    fmt="npy",
                    chunk=chunk,
                    num_chunks=num_chunks
                    )
            self.X3_Train = self._appendSamples_generator(
                    treename="Tree_Train",
                    var="Lr3Img",
                    batch=batch,
                    batch_size=batch_size,
                    fmt="npy",
                    chunk=chunk,
                    num_chunks=num_chunks
                    )

    def yieldValData(self,batch,batch_size,chunk,num_chunks):
        """
        Sets Validation X,Y,Z as generator properties of the Data class.

        # Args::
            batch_size:
            chunk:

        # Returns::
            None
        """
        self.X2_Val = self._appendSamples_generator(
                treename="Tree_Val",
                var="Lr2Img",
                batch=batch,
                batch_size=batch_size,
                fmt="npy",
                chunk=chunk,
                num_chunks=num_chunks
                )
        self.Y_Val = self._appendSamples_generator(
                treename="Tree_Val",
                var="Y",
                batch=batch,
                batch_size=batch_size,
                fmt="npy",
                chunk=chunk,
                num_chunks=num_chunks
                )
        self.Z_Val = self._appendSamples_generator(
                treename="Tree_Val",
                var="Z",
                batch=batch,
                batch_size=batch_size,
                fmt="h5",
                chunk=chunk,
                num_chunks=num_chunks
                )
        if self.usage == 'OFFLINE':
            self.X1_Val = self._appendSamples_generator(
                    treename="Tree_Val",
                    var="Lr1Img",
                    batch=batch,
                    batch_size=batch_size,
                    fmt="npy",
                    chunk=chunk,
                    num_chunks=num_chunks
                    )
            self.X3_Val = self._appendSamples_generator(
                    treename="Tree_Val",
                    var="Lr3Img",
                    batch=batch,
                    batch_size=batch_size,
                    fmt="npy",
                    chunk=chunk,
                    num_chunks=num_chunks
                    )

    def yieldTestData(self,batch,batch_size,chunk,num_chunks):
        """
        Sets Testing X,Y,Z as generator properties of the Data class.

        # Args::
            batch_size:
            chunk:

        # Returns::
            None
        """
        self.X2_Test = self._appendSamples_generator(
                treename="Tree_Test",
                var="Lr2Img",
                batch=batch,
                batch_size=batch_size,
                fmt="npy",
                chunk=chunk,
                num_chunks=num_chunks
                )
        self.Y_Test = self._appendSamples_generator(
                treename="Tree_Test",
                var="Y",
                batch=batch,
                batch_size=batch_size,
                fmt="npy",
                chunk=chunk,
                num_chunks=num_chunks
                )
        self.Z_Test = self._appendSamples_generator(
                treename="Tree_Test",
                var="Z",
                batch=batch,
                batch_size=batch_size,
                fmt="h5",
                chunk=chunk,
                num_chunks=num_chunks
                )
        if self.usage == 'OFFLINE':
            self.X1_Test = self._appendSamples_generator(
                    treename="Tree_Test",
                    var="Lr1Img",
                    batch=batch,
                    batch_size=batch_size,
                    fmt="npy",
                    chunk=chunk,
                    num_chunks=num_chunks
                    )
            self.X3_Test = self._appendSamples_generator(
                    treename="Tree_Test",
                    var="Lr3Img",
                    batch=batch,
                    batch_size=batch_size,
                    fmt="npy",
                    chunk=chunk,
                    num_chunks=num_chunks
                    )



    # def _load_arr_from_files(self,):
        # filepath_sig = self._getName(treename,'GammaJet',var,chunk,fmt)
        # filepath_bkg = self._getName(treename,'DiJet',var,chunk,fmt)

        # if fmt == 'npy':
            # # -- Load the "chunk" file for each sample.
            # sig_arr = np.load(
                    # filepath_sig,
                    # allow_pickle=True,
                    # mmap_mode='r')
            # bkg_arr = np.load(
                    # filepath_bkg,
                    # allow_pickle=True,
                    # mmap_mode='r')
        # elif fmt == 'h5':
            # sig_Z_df = pd.read_hdf(filepath_sig, f'Z_{chunk}')
            # bkg_Z_df = pd.read_hdf(filepath_bkg, f'Z_{chunk}')

        # return


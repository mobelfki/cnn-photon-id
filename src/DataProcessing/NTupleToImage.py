#!/usr/bin/env python3
#=============#
# Author: Mohamed Belfkir, Santiago Noacco Rosende
# Contact: snoaccor@cern.ch
# GitLab:https://gitlab.cern.ch/snoaccor
# GitHub:https://github.com/santinoacco
#=============#
"""
This script aims to load all data from a number of files,
get the relevant arrays and save them in a more usefull format for
later trainning.
"""
# import uproot as pr
import pandas as pd
import numpy as np
import numba
import sys
import uproot4
import awkward1
import h5py
# try:
    # import uproot4
    # import awkward1
# except ModuleNotFoundError:
    # !pip install uproot4 --user;
    # !pip install awkward1 --user;
    # sys.path.append("")

sys.path.append('../../')
# sys.path.append('../')
import os
from src.common.parser import set_parser
from src.common.script import Script


# TODO add logging

@numba.njit
def opt_reshape(X,Eta,Phi):
    """
    Reshapes the array X to a new shape = (Phi,Eta,1)
    The reason to use this instead of a built-in function,
    is that we have greater control.

    # Args::
        X: (list/array)

        Eta:

        Phi:

    """
    mtrx = np.zeros(shape=(Phi,Eta))
    for phi in range(0,Phi):
        for eta in range(0,Eta):
            mtrx[phi][eta] = X[phi + Phi*eta]
    # -- up to here mtrx has a shape (Phi,Eta)
    #    however since we need (Phi,Eta,1) for the CNNs we might as well do it here.
    return mtrx.reshape(Phi,Eta,1)
    # return mtrx

class NTupleToImg(Script):
    # def __init__(self, stage, sample_name, treename):
    def __init__(self, stage, treename):
        self.treename = treename
        super().__init__(stage)
        # print(self.stage)
        # print(self.args.dp_in)
        self._get_samples_names(self.args)
        self._load_features(self.args)
        self._set_out_subdirs

    def _load_features(self,args):
        # -- Read features file names
        features_file = args.features_file
        with open(features_file,'r') as features_file:
            self.features = features_file.read().splitlines();
            assert isinstance(self.features,list)

    def _get_samples_names(self,args):
        # -- Read sample file names
        with open(args.samples_file,'r') as samples_file:
            self.sample_name = samples_file.read().splitlines()[0]
            # log.debug(self.sample_name)

    @property
    def _set_out_subdirs(self):
    # def set_outdirs(self):
        # -- get stg name
        stgname = self._getOutName_Stg(self.sample_name)
        # --Create dir outdir/stgname/treename
        self.fulloutdir = f'{self.OutDir}/{stgname}/{self.treename}'
        # log.debug('fulloutdir exists?',os.path.exists(self.fulloutdir))
        # print('fulloutdir exists?',os.path.exists(self.fulloutdir))
        try:
            os.makedirs(self.fulloutdir)
            print(f' --> new dir {self.fulloutdir}')
        except FileExistsError:
            print(f' --> dir {self.fulloutdir} already exists')

    def _getOutName_Stg(self, sample):
        """
        sets strategy name for sample to output
        """
        # TODO make this less specific,
        #      right now it's too related to IsoTight.
        if "Strategy_IsoTight_1" in sample:
            return 'stg_1';
        if "Strategy_IsoTight_2" in sample:
            return 'stg_2';
        if "Strategy_IsoTight_3" in sample:
            return 'stg_3';
        if "Strategy_IsoTight_0" in sample:
            return 'stg_0';

    def _getOutName_Selection(self, sample):
        """
        sets selection name for sample to output.
        It's use for labeling
        """
        if "DiJet" in sample: return 'DiJet_IsoTight';
        if "GammaJet" in sample: return 'GammaJet_IsoTight';

    def set_to_img(self,data_DF,Eta,Phi,Debug=False):
        """
        Transform data_df -> np.Array,
        then it normalize the array by the total sum of the array energy.
        Finally it reshapes the Array into a matrix like form with dims:
        (Eta,Phi,1).
        """
        X = data_DF.to_numpy(copy=True) # we might not need this anymore?
        if Debug: print('X shape before:',X.shape)
        # -- Sum the energy of each array.
        tot_E=np.asarray([x.sum(axis=0) for x in X])
        # tot_E=np.asarray([np.abs(x.sum(axis=0)) for x in X])
        # -- Assert no 0 sum
        Zero_E = np.where(any(tot_E)==0.)
        assert Zero_E is not None, f'Arrays {Zero_E} have a total energy of 0.'

        # -- Normalize
        X/=tot_E
        # -- Reshape
        X_r = np.asarray([opt_reshape(x,Eta,Phi) for x in X])
        # X_r = [opt_reshape(x,Eta,Phi) for x in X]
        # X_r = np.asarray([x.reshape(Eta,Phi,1) for x in X])

        return X_r

    def old_save_data(self,data,data_name,sample,fmt='NPY'):
        selecname = self._getOutName_Selection(sample)
        # --Def helper dict
        if fmt=='NPY':
            ofn = f'{selecname}_{data_name}.npy'
            path = os.path.join(self.fulloutdir,ofn)
            np.save(file=path,arr=data)
        if fmt=='HDF5':
            ofn = f'{selecname}_{data_name}.h5'
            path = os.path.join(self.fulloutdir,ofn)
            data.to_hdf(
                    path,
                    key=f'{data_name}',
                    # format='table',
                    # mode='a'
                    )

    def save_data(self, data, data_name, sample):
        """
        helper method to store data in NPY format into default output.
        Run this method after `execute` otherwise arrays will be empty.

        # Parameters:

            sample: the desired sample

        # Return: None
        """
        # TODO: check if saving everything in h5 has any inconvinient,
        #       because it might be useful to have everything in the same format to run at the grid.

        selecname = self._getOutName_Selection(sample)
        ofn = f'{selecname}_{data_name}.h5'
        path = os.path.join(self.fulloutdir,ofn)
        if isinstance(data,pd.DataFrame):
            data.to_hdf(
                    path,
                    key=f'{data_name}',
                    format='table',
                    mode='a'
                    )

        if isinstance(data,np.ndarray):
            with h5py.File(path,'a') as f:
                f.create_dataset(
                        data_name,
                        data=data,
                        shape=data.shape,
                        dtype='i',
                        )

    def execute_lazy(self,data_frame,sample,chunk_num):
        """
        """
        X2_df = data_frame.pop('clusterCellsLr2E7x11')
        X1_df = data_frame.pop('clusterCellsLr1E7x11')
        X3_df = data_frame.pop('clusterCellsLr3E7x11')
        # -- defines labels for MC
        Y_df = data_frame.pop('isTruthMatch')
        # -- keep all other relevant vars in Z.
        Z_df = data_frame
        # -- Save non-training variables
        self.old_save_data(
                data=Z_df,
                data_name=f'Z_{chunk_num}',
                sample=sample,
                fmt='HDF5'
                )
        # self.save_data(
                # data=Z_df,
                # data_name=f'Z_{chunk_num}',
                # sample=sample,
                # )
        del Z_df
        # -- Save labels
        self.old_save_data(
                data=Y_df,
                data_name=f'Y_{chunk_num}',
                sample=sample,
                )
        # self.save_data(
                # data=Y_df,
                # data_name=f'Y_{chunk_num}',
                # sample=sample,
                # )
        del Y_df
        # -- Set and Save X1
        X1 = self.set_to_img(
                data_DF=X1_df,
                Eta=self.args.calo['Lr1']['num_eta_cells'],
                Phi=self.args.calo['Lr1']['num_phi_cells'],
                )
        del X1_df
        # print('---- X1')
        # print(type(X1), X1.shape)
        self.old_save_data(
                data=X1,
                data_name=f'Lr1Img_{chunk_num}',
                sample=sample,
                )
        # self.save_data(
                # data=X1,
                # data_name=f'Lr1Img_{chunk_num}',
                # sample=sample,
                # )

        # -- Set and Save X2
        X2 = self.set_to_img(
                data_DF=X2_df,
                Eta=self.args.calo['Lr2']['num_eta_cells'],
                Phi=self.args.calo['Lr2']['num_phi_cells'],
                )
        del X2_df
        # print('---- X2')
        self.old_save_data(
                data=X2,
                data_name=f'Lr2Img_{chunk_num}',
                sample=sample,
                fmt='NPY'
                )
        # self.save_data(
                # data=X2,
                # data_name=f'Lr2Img_{chunk_num}',
                # sample=sample,
                # )

        # -- Set and Save X3
        X3 = self.set_to_img(
                data_DF=X3_df,
                Eta=self.args.calo['Lr3']['num_eta_cells'],
                Phi=self.args.calo['Lr3']['num_phi_cells'],
                )
        del X3_df
        # print('---- X3')
        self.old_save_data(
                data=X3,
                data_name=f'Lr3Img_{chunk_num}',
                sample=sample,
                )
        # self.save_data(
                # data=X3,
                # data_name=f'Lr3Img_{chunk_num}',
                # sample=sample,
                # )

    def loadTree_lazy(self, filepath=None, num_chunks=1):
        """
        For every file in "filepath",
        this will load the data of the TTree,
        for the specified features in "features.txt".
        It will save it in chunks.

        # Args:
            filepath: path to locate .root files
            num_chunks: total number of files to read. Default is 1
        """
        if filepath is None:
            filepath = f'{self.InDir}/{self.sample_name}'
        # log.debug(filepath)
        # -- Set an index iter.
        idx=0;
        # -- Get chunk_size for the corresponding Tree
        chunk_size = round(len(uproot4.lazy(f'{filepath}:{self.treename}'))/num_chunks)
        for arrays in uproot4.iterate(
                files=f'{filepath}:{self.treename}',
                filter_name=self.features,
                step_size=chunk_size,
                library='np',   # we can do this because we know there won't be issues
                ):
            # -- NOTE: we use data frames because "set_to_img" expects them.
            df = pd.DataFrame(data=arrays)
            # -- Save in chunks and in the correct format
            self.execute_lazy(
                    data_frame=df,
                    sample=self.sample_name,
                    chunk_num=idx
                    )
            idx+=1

def main():
    """
    The main function of NTupleToNumpy
    """

    trees = ('Tree_Train','Tree_Val','Tree_Test')

    for tree in trees:
        print(f'--> Reading TTree: {tree}')
        NTuple2Img = NTupleToImg(
                stage='PREPROCESS',
                treename=tree,
                );
        NTuple2Img.loadTree_lazy(
                num_chunks=NTuple2Img.args.num_chunks,
                )
        # -- Free some memory, quite important step!
        del NTuple2Img

if __name__ == '__main__':
    main()

# class NTupleToImg(object):

    # def __init__(self, sample_name, outdir, treename):
        # self.args = set_parser()
        # self.sample_name = sample_name
        # self.outdir = outdir
        # self.treename = treename
        # self._load_features(self.args)
        # # -- Build dirs.
        # self.set_outdirs()

# def main():
    # args=set_parser()
    # # -- Set default output dir
    # if not args.dp_out:
        # outputdir = f"{args.g_outdir}/DataProcessing"
        # assert os.path.exists(outputdir), \
                # f"Expected outputdir: {outputdir} does not exist"
    # else:
        # outputdir = args.dp_out
    # # -- Set default input dir
    # if not args.dp_in:
        # inputdir = f'{args.g_outdir}/NTuple'
        # assert os.path.exists(inputdir), \
                # f"Expected inputdir: {inputdir} does not exist"
    # else:
        # inputdir = args.dp_in

    # # -- Read sample file names
    # with open(args.samples_file,'r') as samples_file:
        # samples_name = samples_file.read().splitlines();

    # # -- As default create output dir
    # try:
        # os.makedirs(outputdir)
        # print(f'--> output dir {outputdir} created')
    # # -- if exist pass
    # except: pass
    # trees = ('Tree_Train','Tree_Val','Tree_Test')

    # # samples_file_path = samples_name[0]
    # for tree in trees:
        # print(f'--> Reading TTree: {tree}')
        # NTuple2Img = NTupleToImg(
                # # sample_name=samples_file_path,
                # stage='PREPROCESS',
                # # outdir=outputdir,
                # treename=tree,
                # );
        # NTuple2Img.loadTree_lazy(
                # # filepath=f'{NTuple2Img.InDir}/{samples_file_path}',
                # # filepath=f'{inputdir}/{samples_file_path}',
                # num_chunks=NTuple2Img.args.num_chunks,
                # )
        # # -- Free some memory, quite important step!
        # del NTuple2Img


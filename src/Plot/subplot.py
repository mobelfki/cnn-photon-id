#!/usr/bin/env python3
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


t = 'Conv'

b = 7

def get_TightEff(t):
    if t=='Inc':
            return [0.93,0.71]
    if t=='UnConv':
            return [0.91,0.72]
    if t=='Conv':
            return [0.96,0.70]

def load(t,b):

    SS_Eff  = np.load('/home/belfkir/MLPhotonID/Train/Training/SS/SS_WithConv__Eff_'+t+'_bin_'+str(b)+'.npy')
    SS_Rej  = np.load('/home/belfkir/MLPhotonID/Train/Training/SS/SS_WithConv__Rej_'+t+'_bin_'+str(b)+'.npy')
    CNN_Eff = np.load('/home/belfkir/MLPhotonID/Train/Training/HybridCNN2_256/CNN_Adam_256_128_10Ep_lr0000_Bt32_Eff_'+t+'_bin_'+str(b)+'.npy')
    CNN_Rej = np.load('/home/belfkir/MLPhotonID/Train/Training/HybridCNN2_256/CNN_Adam_256_128_10Ep_lr0000_Bt32_Rej_'+t+'_bin_'+str(b)+'.npy')

    return [SS_Eff, SS_Rej, CNN_Eff, CNN_Rej]


plt.figure()

plt.plot(load(t,b)[0], load(t,b)[1], lw=2)
plt.plot(load(t,b)[2], load(t,b)[3], lw=2)
#plt.plot(get_TightEff(t)[0], get_TightEff(t)[1], 'or')

plt.legend(['SS-DNN', 'CNN'])
plt.xlim((0.8,1))
plt.xlabel('Signal Eff')
plt.ylabel('Background Rej')
plt.title('CNN vs DNN : ' + t + ', bin : '+ str(b))
#plt.show()
plt.savefig('CNN_vs_DNN_'+t+'_bin_'+str(b)+'.png')

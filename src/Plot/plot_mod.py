#!/usr/bin/env python3
#=============#
# Author: Santiago Noacco Rosende
# Contact: snoaccor@cern.ch
# GitLab:https://gitlab.cern.ch/snoaccor
# GitHub:https://github.com/santinoacco
#=============#
"""Plot module"""
import matplotlib.pyplot as plt
import numpy as np
import copy
import os

_default_cmap = plt.get_cmap('tab20c')

_default_style={
        'plot':{
            'marker':'o',
            'mfc':'none',
            'color':'DarkBlue',
            'linestyle':'--',
            'linewidth':1.5,
            },
        'date':{
            'marker':'H',
            'mfc':'none',
            'color':'green',
            'linestyle':'--',
            'linewidth':3,
            },
        'hist':{
            'density':True,            #--> normalized histogram
            'align':'mid',             #--> place ticks in the middle
            'histtype':'stepfilled',   #--> type
            'color':'DarkBlue',        #--> linecolor
            'linestyle':'-',
            'linewidth':2,
            'alpha':0.5,               #--> how strong the shading of the filling is
            'edgecolor':'DarkBlue',
            'facecolor':'LightBlue',   #--> color of the shading
            },
        'pie':{
            'shadow':True,
            'autopct':'%1.1f%%',
            'radius':1.0,
            'wedgeprops':{'width':0.75,'edgecolor':'w'}
            },
        'heatmap':{
            }
        }

def get_sty():
    new_dict = copy.deepcopy(_default_style)
    return new_dict

def mplot(ax,x,y,label,yerr=np.nan,xerr=np.nan,style_dict=None):
    if style_dict is None:
        style_dict = _default_style['plot']
    ax.errorbar(
            x=x,
            y=y,
            label=label,
            yerr=yerr,
            xerr=xerr,
            **style_dict,
            )

def mhist(ax,x,label,bins=None,bin_num=None,yerr=None,xerr=None,style_dict=None):
    """My default histogram"""
    if bin_num is None:
        bin_num=10

    if bins is None:
        max_bin = max(x)
        min_bin = min(x)
        step = (max_bin - min_bin)/bin_num
        print(f'max={max_bin},min={min_bin},step={step}')
        bins = np.arange(
                start=min_bin,
                stop=max_bin,
                step=int(step)
                )

    if style_dict==None:
        style_dict = _default_style['hist']

    # -- Build histogram
    entries,bins,patches = ax.hist(
            x=x,
            label=label,
            bins=bins,
            **style_dict
            )

    # print(entries)

    # -- Add errorbar
    bin_centers = 0.5*(bins[:-1] + bins[1:])
    # print(bin_centers)
    ax.errorbar(
            bin_centers, entries, yerr=yerr,xerr=xerr,
            label='Error bar',
            fmt='r.',
            ecolor='Orange',
            )

    return entries,bins,patches

def plot_roc_curve(roc_data, auc_dict, wp_dict, sample_tag, OutDir, bins=None):
    """
        sample_tag :: if Inclusive, Converted or UnConverted
        wp_dict:: container with the WorkingPoint Signal Eff and Background Rej.
    """

    # print('P.plot_roc_curve -->',roc_data.keys())

    y=roc_data['ROC_fprCenter']
    yEl=roc_data['ROC_fprErrL']
    yEh=roc_data['ROC_fprErrH']
    x=roc_data['ROC_tprCenter']
    xEl=roc_data['ROC_tprErrL']
    xEh=roc_data['ROC_tprErrH']
    threshold=roc_data['ROC_thr'];
    # it's saved as an array but its an scalar,
    # so take any of them they are all the same
    # auc=roc_data['ROC_AUC'][0];
    auc=auc_dict['ROC_AUC'];

    # -- Init figure
    fig, ax_roc = plt.subplots()
    roc_label = f'Models ROC curve (area = {auc:0.4f})'
    # -- Get and set the style
    sty_1 = get_sty()
    sty_1['plot'].update({'color':'DarkOrange','marker':'.'})

    # -- Draw Model ROC
    mplot(ax=ax_roc,
            x=x,
            y=y,
            yerr=[yEl,yEh],
            xerr=[xEl,xEh],
            label=roc_label,
            style_dict=sty_1['plot']
            )
    # -- Draw WPs
    wp_clrs= ['DarkBlue','DarkGreen']
    i=0
    for wp in wp_dict.keys():
        # sty_2 = P.get_sty()
        sty_2 = get_sty()
        sty_2['plot'].update({'linestyle':None,'color':wp_clrs[i]})
        # P.mplot(ax=ax_roc,
        mplot(ax=ax_roc,
                x=wp_dict[wp]['Sig_Eff'],
                y=wp_dict[wp]['Bkg_Rej'],
                label=wp,
                style_dict=sty_2['plot']
                )
        i+=1
    # -- Axes settings
    if bins is None:
        ax_title = f'MC data - {sample_tag}'
    else:
        ax_title = f'MC data - {sample_tag}@eta{bins[0]}xpT{bins[1]}'
    ax_roc.set_xlabel('Signal Efficiency')
    ax_roc.set_ylabel('Background Rejection')
    ax_roc.set_title(ax_title)
    ax_roc.set_xlim([0., 1.1])
    ax_roc.set_ylim([0., 1.1])
    ax_roc.legend()

    # -- Save
    outdir = f'{OutDir}/{sample_tag}/plots'
    try:
        os.makedirs(outdir)
    except FileExistsError:
        pass

    if bins is None:
        fname = os.path.join(outdir,'full_outROC.png')
    else:
        fname = os.path.join(
                outdir,
                'eta{eta:1.1f}xpT{pt:1.1f}_outROC.png'.format(
                    eta=bins[0],
                    pt=bins[1],)
                )
    fig.savefig(fname)
    plt.close()

def plot_CNN_output(y_true, y_pred, sample_tag, OutDir):
    """
    Plots the histograms for the predicted values of Signal and Background by the CNN model.

    # Args::None

    # Returns:: None
    """
    # -- Init figure
    fig, ax_histSB = plt.subplots()

    Sig_mask = y_true == 1
    Bkg_mask = y_true == 0

    # sty_1 = P.get_sty()
    sty_1 = get_sty()
    sty_1['hist'].update({'color':'DarkOrange'})
    sty_1['hist'].update({'facecolor':'Yellow'})
    # sty_2 = P.get_sty()
    # sty_2['hist'].update('linestyle':None)

    # P.mhist(ax=ax_histSB,
    mhist(ax=ax_histSB,
            x=y_pred[Bkg_mask],
            label='Background',
            bins=160,
            # bin_num=80,
            style_dict=sty_1['hist'])
    # P.mhist(ax=ax_histSB,
    mhist(ax=ax_histSB,
            x=y_pred[Sig_mask],
            label='Signal',
            bins=50,
            # bin_num=80,
            style_dict=None)

    # -- Axes settings
    # ax_histSB.set_xlabel('Signal Efficiency')
    ax_histSB.set_ylabel('Normalized entries')
    ax_histSB.set_title(f'NN Ouput {sample_tag}')
    ax_histSB.set_xlim([0., 1.0])
    # ax_histSB.set_ylim([0., 1.0])
    ax_histSB.legend()
    fname = os.path.join(OutDir,f'P_{sample_tag}.png')
    fig.savefig(fname)
    plt.close()

def plot_AUC_matrix(data,col_labels,row_labels,title,
                    plotstyle='RGB',OutDir=".",ax=None,cbarlabel=None,
                    kwargs=None,cbar_kw=None,textkw=None):
    """
    Description

    # Args::
        plotstyle:
            'RGB' or 'HEATMAP' (case insensitive)

    # Returns::
    """
    if not ax:
        fig, ax = plt.subplots(figsize=(12,8))
    if not kwargs:
        kwargs = {'cmap':plt.get_cmap('Purples')}
    if not cbar_kw:
        cbar_kw = {}
    if not textkw:
        textkw = {}
    plotstyle = plotstyle.upper()

    config_ax_heatmap(ax,data,col_labels,row_labels)

    if plotstyle == 'RGB':
        im = ax.imshow(data, **kwargs)
        # NOTE: This plot can not have a color bar because it should be a color surface.
        # -- Add text
        for i in range(data.shape[0]):
            for j in range(data.shape[1]):
                txt_elm_c = np.round(data[i,j,1]*100,2)
                txt_elm_L = np.round(data[i,j,0]*100,2)
                txt_elm_H = np.round(data[i,j,2]*100,2)
                # NOTE: the following string sets `txt_elm_c` as main number,
                #       and `txt_elm_L`(`txt_elm_H`) as subindex(superindex).
                txt_elm = r'${0:.2f}^{{+{2:.2f}}}_{{-{1:.2f}}}$'.format(txt_elm_c,
                                                                        txt_elm_L,
                                                                        txt_elm_H)
                plt.text(x=j, y=i,
                         s=txt_elm,
                         ha="center", va="center", color="k")
    elif plotstyle == 'HEATMAP':
        im = ax.imshow(data[:,:,1], **kwargs)
        # Create colorbar
        cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
        cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")
        text = annotate_heatmap(
                im,
                data=data,
                textcolors=("black", "white"),
                threshold=None,
                **textkw
                )

    fig.tight_layout()
    # -- Save and close
    fname = os.path.join(OutDir,f'{title}_{plotstyle}.png')
    plt.savefig(fname)
    plt.close()

def config_ax_heatmap(ax,data,col_labels,row_labels,title=None):
    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)

    # Rotate the tick labels and set their alignment.
    plt.setp(
        ax.get_xticklabels(),
        rotation=30,
        ha="right",
        rotation_mode="anchor")

    # Turn spines off and create white grid.
    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    ax.set_xlabel(r'$|\eta|$ bin')
    ax.set_ylabel(r'$p_T$ bin')
    ax.set_title(title)

def plot_matrix(data, title, col_labels, row_labels, ax=None, OutDir=None,
                rm_crackbin=False, **kwargs):
    """
    Plots the content of data to a "heatmap"-like matrix,
    data is expected to have (N,M,C) so de elements in the C-dim are treated as RGB component.

    # Args::
        data: (NumpyArray) expected dimensions are (N,M,C). For instance if plotting the AUC with its errors the array would have a shape (N,M,3), where each (N,M)-element is an array with [AUC, ErrLow, ErrHigh] (order depends).


    # Returns::
        None
    """
    if not ax:
        fig, ax = plt.subplots()
        # ax = plt.gca()

    if not OutDir:
        OutDir = '.'

    # -- here we remove the crackbin from the matrix
    if rm_crackbin:
        crackbin=4
        data = np.delete(data[:,:], crackbin, 1)

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # NOTE: This plot can not have a color bar because it should be a color surface.

    config_ax_heatmap(ax,data)

    # -- Set text for each matrix element
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            txt_elm_c = np.round(data[i,j,1]*100,2)
            txt_elm_L = np.round(data[i,j,0]*100,2)
            txt_elm_H = np.round(data[i,j,2]*100,2)
            # NOTE: the following string sets `txt_elm_c` as main number,
            #       and `txt_elm_L`(`txt_elm_H`) as subindex(superindex).
            txt_elm = r'${0:.2f}^{{+{2:.2f}}}_{{-{1:.2f}}}$'.format(txt_elm_c,
                                                                    txt_elm_L,
                                                                    txt_elm_H)
            plt.text(x=j, y=i,
                     s=txt_elm,
                     ha="center", va="center", color="k")

    # -- Save and close
    fname = os.path.join(OutDir,f'{title}.png')
    plt.savefig(fname)
    plt.close()

def heatmap(data, row_labels, col_labels, ax=None,
            cbar_kw={}, cbarlabel="", **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.
    Lightly modified from [Matplotlib][https://matplotlib.org/3.3.2/gallery/images_contours_and_fields/image_annotated_heatmap.html#sphx-glr-gallery-images-contours-and-fields-image-annotated-heatmap-py]

    Parameters
    ----------
    data
        A 3D numpy array of shape (N, M, C).
    row_labels
        A list or array of length N with the labels for the rows.
    col_labels
        A list or array of length M with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data[:,:,1], **kwargs)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # # Let the horizontal axes labeling appear on top.
    # ax.tick_params(top=True, bottom=False,
                   # labeltop=True, labelbottom=False)

    config_ax_heatmap(ax,data,col_labels,row_labels)

    return im, cbar

def annotate_heatmap(im, data=None,
                     textcolors=("black", "white"),
                     threshold=None, **textkw):
    """
    A function to annotate a heatmap.
    Lightly modified from [Matplotlib][https://matplotlib.org/3.3.2/gallery/images_contours_and_fields/image_annotated_heatmap.html#sphx-glr-gallery-images-contours-and-fields-image-annotated-heatmap-py]

    Parameters
    ----------
    im
        The AxesImage to be labeled.
    data
        Data used to annotate.  If None, the image's data is used.  Optional.
    textcolors
        A pair of colors.  The first is used for values below a threshold,
        the second for those above.  Optional.
    threshold
        Value in data units according to which the colors from textcolors are
        applied.  If None (the default) uses the middle of the colormap as
        separation.  Optional.
    **kwargs
        All other arguments are forwarded to each call to `text` used to create
        the text labels.
    """

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    # -- Set text for each matrix element
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            txt_elm_c = np.round(data[i,j,1]*100,2)
            txt_elm_L = np.round(data[i,j,0]*100,2)
            txt_elm_H = np.round(data[i,j,2]*100,2)
            # NOTE: the following string set `txt_elm_c` as main number,
            #       and `txt_elm_L`(`txt_elm_L`) as subindex(superindex).
#             kw.update(color=textcolors[int(im.norm(data[i, j][1]) > threshold)])
            kw.update(color=textcolors[int(im.norm(data[i, j,1]) > threshold)])
            txt_elm = r'${0:.2f}^{{+{2:.2f}}}_{{-{1:.2f}}}$'.format(txt_elm_c,txt_elm_L,txt_elm_H)
            text = im.axes.text(
                    x=j,y=i,
                    s=txt_elm,
                    ha="center", va="center",
                    **kw,
                    # color="k"
                    )
            texts.append(text)

    return texts

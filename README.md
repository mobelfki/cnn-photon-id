# Introduction

The **src/** directory contains the code used to develop the photonID using images from the EM layers (sampling 1, 2 and 3). \
Further information such as documentation,
can be found in the **[docs](docs/)**.
Deprecated code that might be needed for references is store in **old_code/**.

## Setup

- Install dependencies:
    1. `python3 -m install pip --upgrade --user` 
    2. `python3 -m pip install -r py_requirements --user` \
    or simply run
    3. `make`
- Modify the [config file](run/config.ini) to set the way you want to run the package.

## The packages

### common/

**Contains**

- `parser.py`: \
    Here is where we set the arguments to parse through command line,
    and also the configuration for the config file parser.
    We are using _ConfigArgParse_ library,
    which merges into one parser the options to read env variables,
    config files and command line arguments.
- `script.py`: \
    Python class to use as a common tool for building the `ntupletonumpy`, `train` and `val` scripts,
    it holds the loading of the data and parsing of options.


### [DataProcessing/](docs/DataProcessing.md)

Python class manipulate data and input for the training, validation and testing.

**Contains**

-   `data_handler.py`: 
        
-   `NTupleToImage.py`: 

- [`GetNtuple/NTuple/`](docs/NTuple.md):
    AnalysisBase framework to apply strategy (0,1,2 and 3) and signal/background selections,
    runs as :

    `runCleanAndGet Input_Dir Ouput_Dir Strategy Evt_first Evt_last Selection`


### NeuralNetworks/

#### Models/:
Python class where the CNN models are _defined_ (multiple models are defined as a class)

#### [Train/](doc/Train.md):
Magic is done here!

**Contains**

-   `reweighting.py`: \
    Code to apply re weighting to remove discrepancy between signal and background

-   `train.py`: \
    Training code.

-   `toONNX.py`: \
    Convert trained CNN model to ONNX,
    run as `./toONNX.py -args`

#### [Val/](doc/Val.md):
Validation with test data set of the trained model is done here.

### Plot/:
Contains all plotting routines.


## Recommended work flow - How To:

1.  Get the raw data from [ZllyAthDerivation](https://gitlab.cern.ch/mobelfki/zllyathderivation),
    defining what you will use as Signal and Background,
    also choose where to use Data or MC samples.
    It is advisable to have one sample to use to get Signal and other for Background.
    At the end of this stage you should have at least two output ROOT files with 4 `TTrees`.
2.  With the output of stage 1. you want to run **NTuple**.
3.  Open a fresh new session at lxplus, 
    because setting ATHENA will create inconsistencies between ROOT and Python.
    You can also at this stage run the package at your local machine.
    Before moving forward with the preprocessing at this stage is worth taking a look at your samples distributions,
    you can plot many distributions from `Z.h5` file using `...`. 
    Note that the samples might be bias towards some values,
    or that could be unbalanced.
    At this point **_set the environment_** following the instructions on how to run @lxplus.
4.  At this stage is where most if not all the remaining preprocessing is done.
    Note that this package is aimed to work with two usages in mind (at least for now),
    one being **OFFLINE** and the other one **HLT**.
    Due to the rather different proposals they require slightly different sets of data.
    For the HLT case, we are only interested on contrasting this method with the Fast-Reconstruction process,
    which only uses Calo-Layer2 data,
    therefore, we only require this dataset for this usage.
    On the other hand, OFFLINE uses data from the 3 Calo Layers.
5.  Before you can start training you need to define the CNN model and all the metrics that you want to keep track of.
    The model's architecture definition must be done at [models.py](src/NeuralNetworks/Models/models.py),
    but you can use an existing architecture and change its HyperParameters at the command line.
    You can add callbacks in the [train.py](src/NeuralNetworks/Train/train.py),
6.  Validation and Plotting
7.  Deploy using `toONNX` 

### Links to Indico talks
- https://indico.cern.ch/event/979552/


### TODO

- Extend generator training to support multithreading.
- Plot sample dist. in eta and pt ranges.
- Implement feature extraction
- Running on the Grid

# Packages required

-   Python3.6+    
-   ROOT 6
-   python dependencies in py_requirements.txt

# [Run](docs/run.md) 

Details on how to run @lxplus, @HTcondor or @grid.

**Enjoy !**

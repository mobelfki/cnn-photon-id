# Adapted from a script written by Jonas Neundorf

import QFramework
from QFramework import TQHistogramUtils, INFO, TQTaggable

from ROOT import TMath

import argparse
import scipy.optimize as spopt

mixingVals = [0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0]

# ====================================================================================================

def makeSignificanceGraph(signals, significance, args):
	""" Plot significance vs |V_lN| """

	global mixingVals

	from ROOT import TGraph, TLegend, TCanvas
	from ROOT import kRed, kBlue, kGreen, kViolet, kYellow, kCyan, kBlack, kOrange, kPink

	can = TCanvas("", "", 1000, 700)
	graphs = {}
	for sig in signals:
		g = TGraph()
		for i, point in enumerate(mixingVals):
			#g.SetPoint(i, point, significance[sig][i])
			g.SetPoint(i, point ** 2, significance[sig][i])
			graphs[sig] = g

	g_mN500GeV = graphs["mN500GeV"]
	g_mN1TeV   = graphs["mN1TeV"]
	g_mN3TeV   = graphs["mN3TeV"]
	g_mN5TeV   = graphs["mN5TeV"]
	g_mN10TeV  = graphs["mN10TeV"]

	g_mN500GeV.SetLineColor(kRed)
	g_mN1TeV.SetLineColor(kGreen)
	g_mN3TeV.SetLineColor(kBlue)
	g_mN5TeV.SetLineColor(kBlack)
	g_mN10TeV.SetLineColor(kCyan)

	g_mN500GeV.SetMarkerColor(kRed)
	g_mN1TeV.SetMarkerColor(kGreen)
	g_mN3TeV.SetMarkerColor(kBlue)
	g_mN5TeV.SetMarkerColor(kBlack)
	g_mN10TeV.SetMarkerColor(kCyan)

	leg = TLegend(0.3, 0.6, 0.5, 0.8)
	leg.AddEntry(g_mN500GeV, "m_{N} = 0.5 TeV", "L")
	leg.AddEntry(g_mN1TeV,   "m_{N} = 1 TeV", "L")
	leg.AddEntry(g_mN3TeV,   "m_{N} = 3 TeV", "L")
	leg.AddEntry(g_mN5TeV,   "m_{N} = 5 TeV", "L")
	leg.AddEntry(g_mN10TeV,  "m_{N} = 10 TeV", "L")

	g_mN500GeV.Draw("AL")
	g_mN1TeV.Draw("same")
	g_mN3TeV.Draw("same")
	g_mN5TeV.Draw("same")
	g_mN10TeV.Draw("same")

	leg.Draw("same")

	# do all cosmetic configuration on h_mN500GeV, as it is drawn first
	g_mN500GeV.GetYaxis().SetTitle("Significance")
	g_mN500GeV.GetXaxis().SetTitle("|V_{lN}|^{2}")
	g_mN500GeV.GetYaxis().SetRangeUser(0, max(significance["mN500GeV"])* 1.1)
	#g_mN500GeV.GetXaxis().SetRangeUser(mixingVals[0], mixingVals[len(mixingVals)-1])
	g_mN500GeV.GetXaxis().SetRangeUser(mixingVals[0] ** 2, mixingVals[len(mixingVals)-1] ** 2)

	can.SaveAs(args.outputDir + "/" + args.saveName)

# ====================================================================================================

def limitFunction(m, b, errb, s, sigma):
	""" function to be solved for 0. Sigma is significance for CI (default 95% CI) """

	# mixing scales cross section (i.e number of signal events) by power of 4
	signalScaled = s * (m[0] ** 4)
	significance = getSignificance(b, errb, signalScaled)
	return significance - sigma

# ====================================================================================================

def getSignificance(b, errb, s):
	return TQHistogramUtils.getPoissonWithError(b, errb, s)

# ====================================================================================================

def main(args, histFile_abs):

	global mixingVals

	# open up sample folder
	INFO("Reading file {file}".format(file = args.inputFile))
	sampleFileName = args.inputFile + ":samples"
	sampleFile = QFramework.TQSampleFolder.loadLazySampleFolder(sampleFileName)
	sampleFileReader = QFramework.TQSampleDataReader(sampleFile)

	cut = args.cutName

	bkgUnc = args.bkgUnc
	INFO("Using relative bkg uncertainty of {bkgUnc}".format(bkgUnc=bkgUnc))

	# signal pathe
	signals = ["mN500GeV", "mN1TeV", "mN3TeV", "mN5TeV", "mN10TeV"]

	# retrieve background paths
	channel = args.channel
	campaign = args.campaign
	from QFramework import TQDefaultPlotter
	plotter = TQDefaultPlotter(sampleFile)
	if plotter.importProcessesFromFile(histFile_abs):
		QFramework.INFO("reading histogram processes from  file '{:s}'".format(histFile_abs))

	# retrieve bkg paths, filter out signal and data
	bkgPaths = []
	for process in plotter.getListOfProcessNames():
		path = plotter.getProcessPath(process.GetName())
		tags = TQTaggable(plotter.getProcessTags(process.GetName()))
		if tags.getTagBoolDefault(".isBackground", False):
			bkgPaths.append(path)
	
	# now format the bkg path correctly
	bkgPath = "+".join([v.Data() for v in bkgPaths])

	bkgPath = bkgPath.replace("$(channel)", channel)
	bkgPath = bkgPath.replace("$(campaign)", campaign)
	#print bkgPath

	# retrieve signal/background yields
	b = sampleFileReader.getCounter(bkgPath, cut).getCounter()
	errb = sampleFileReader.getCounter(bkgPath, cut).getError()
	errb = TMath.Sqrt( TMath.Power(errb, 2) + TMath.Power(b * bkgUnc, 2) )

	# optimise 
	for sig in signals:
		s = sampleFileReader.getCounter("sig/{channel}/{campaign}/HeavyN/{sig}".format(sig=sig, channel = channel, campaign = campaign), cut).getCounter()
		init = 1.
		sol = spopt.root(limitFunction, [init], (b, errb, s, args.significance), tol = 0.001)
		INFO("|V|^2 > {sol} excluded at significance of {significance} for {mass}.".format(sol=sol.x[0] ** 2, significance=args.significance, mass=sig))
	
	# calculate significance for different mixing vals
	significance = {}
	for sig in signals:
		significance[sig] = []
		for mixing in mixingVals:
			s = sampleFileReader.getCounter("sig/{channel}/{campaign}/HeavyN/{sig}".format(sig=sig, channel = channel, campaign = campaign), cut).getCounter()
			s *= TMath.Power(mixing, 4)
			significance[sig] += [TQHistogramUtils.getPoissonWithError(b, errb, s)]
	
	# plot graph
	makeSignificanceGraph(signals, significance, args)

# ====================================================================================================

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--input", "-i", metavar = "samples.root:samples", type = str, help = "Name of the sample folder that was output from the analyze stage", dest = "inputFile", required = True)
	parser.add_argument("--outputDir", "-o", metavar = "outputdir", type = str, help = "Name of the output directory to save results", dest = "outputDir", required = True)
	parser.add_argument("--cutName", metavar = "cutname", type = str, help = "cut to calculate significance at", dest = "cutName", default = "CutDYjj_SR")
	parser.add_argument("--bkgUnc", metavar = "bkgUnc", type = float, help = "relative bkg uncertainty", dest = "bkgUnc", default = 0.15)
	parser.add_argument("--significance", metavar = "significance", type = float, help = "significance", dest = "significance", default = 2.)
	parser.add_argument("--channel", metavar = "channel", type = str, help = "channel", dest = "channel", default = "mm")
	parser.add_argument("--campaign", metavar = "campaign", type = str, help = "campaign", dest = "campaign", default = "[mc16a+mc16d+mc16e]")
	parser.add_argument("--saveName", metavar = "saveName", type = str, help = "filename (with file extension) for significance graph", dest = "saveName", default = "significance_mixing.pdf")
	parser.add_argument("--histFile", metavar = "histFile", type = str, help = "histogram processes definition file", dest = "histFile", default = "config/visualization/processes/0nuBB/0nuBB-histogramProcesses.txt")
	args = parser.parse_args()

	import ROOT
	ROOT.gROOT.SetBatch(True)

	# check input file exists before proceeding
	import os.path
	if not os.path.isfile(args.inputFile):
		QFramework.BREAK("Please check that the input file exists")

	os.system("mkdir -p {outputDir}".format(outputDir=args.outputDir))

	# check histogram file can be retrieved
	from CommonAnalysisHelpers import common
	histFile_abs = common.findConfigPath(args.histFile)
	if not os.path.isfile(histFile_abs):
		QFramework.BREAK("Could not find histogram processes file ${histfile}".format(histfile=histFile_abs))

	main(args, histFile_abs)

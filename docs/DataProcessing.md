# Documentation on DataProcessing

## `data_handler.py`

Contains `Data` class to handle the outputs of `NTupleToImage`.
Given a specific *usage* this class will append the Signal and Background data to the same container,
and will create different containers for the differents steps of the training,
being *training*, *validation* and *testing*.

## `NTupleToImage.py`

Input:: `.root` files from `NTuple` output.
These are expected to have 3 different TTrees (Train, Val and Test).

Output:: `.npy` and `.h5` binary files for an easy manipulation of data.

Given a file it loads it's TTree content into a `PandasDataFrame` from which it extracts different features.
By the way, all relevant features that want to be loaded should be written in `features.txt`.

- Takes a `features` file, where one specifies what variables you want to read.
- Takes a `sample` file, where one specifies the name of the sample file to read, that are located at `Input`. If many files have a common string in their name, that can be use to load many files at a time, for example if you have: `file_1`, `file_2`, ..., etc you can write `file_*` to load all those files.
- Basically this script takes a root file and loads its data, taking into account the features specified.
With that data creates 4 NumpyArrays: X1, X2, X3 that are the Layer N Calo cells array; and Y the array of labels and other variables (such as pT, SSvars, etc).
- Each array is stored in a binary file with format NPY.


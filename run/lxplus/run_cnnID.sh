#!/bin/sh

PYTHONVERSION=6
REQUIREMENTS="py_requirements.txt"
# -- SET OUTPUT DIRS
#set GLOBAL_OUTDIR='your_eos'
GLOBAL_OUTDIR="/home/santi_noacco/Desktop/WorkingFolder/BelfkirCode/test/outputs"
NTUPLE_OUT="${GLOBAL_OUTDIR}/NTuple"
DATAPROCESSING_OUT="${GLOBAL_OUTDIR}/DataProcessing"
NN_TRAIN_OUT="${GLOBAL_OUTDIR}/Train"
NN_VAL_OUT="${GLOBAL_OUTDIR}/Val"
#set PLOT_OUT

# -- SET INPUT DIRS
NTUPLE_IN="/home/projects/test/outputs/ZllAthDerivation/NewPhotonIDSamples_FF/"
DATAPROCESSING_IN="${NTUPLE_OUT}/local"

# -- SET GLOBAL PARAMETERS
STG=1
MAXEVS=0    # same as ALL events
FEATURES_FILE='src/DataProcessing/features_ex.txt' 
SAMPLES_FILE='src/DataProcessing/samples_ex.txt' 
USAGE='HLT'
BATCHSIZE=128
EPOCHS=50
TF_V=2      # TensorFlow Version


Set_venv(){
    #python3.$PYTHONVERSION -m venv cnn-photon-id/cnn_phID_env;
    #cd cnn_phID_env;
    #source ./cnn_phID_env/activate;

    #python3 -m pip install --upgrade pip;
    #python3 -m pip install -r $REQUIREMENTS
    
    # -- Works @lxplus.
    python3 -m pip install --upgrade pip --user &&
    python3 -m pip install -r $REQUIREMENTS --user;
}

Run_NTuple(){
# -- Run NTuple for Signal
./src/NTuple/runCleanAndGet "${NTUPLE_IN}/Sig" "${NTUPLE_OUT}/Sig" $STG 0 $MAXEVS GammaJet &&
# -- Run NTuple for Background
./src/NTuple/runCleanAndGet "${NTUPLE_IN}/Bkg" "${NTUPLE_OUT}/Bkg" $STG 0 $MAXEVS DiJet &&

echo " --->> NTuple output generated";
}

Run_DataProcessing(){
# -- Run NTupleToImage
./src/DataProcessing/NTupleToImage.py -i $DATAPROCESSING_IN -o $DATAPROCESSING_OUT -s $SAMPLES_FILE -f $FEATURES_FILE &&
echo " --->> Images generated";
}

Train(){
./train.py -I $DATAPROCESSING_OUT -O $NN_TRAIN_OUT -U $USAGE \
    -b $BATCHSIZE -nEp $EPOCHS -V $TF_V -Swgt 0
}

Validate(){
./val.py -I $DATAPROCESSING_OUT -O $NN_VAL_OUT -U $USAGE -P 'trainoutput/HLT/stg_1/20201007_16x1CL_32x1DL_0.08dr_50Ep_1.0e-04lr/save  d_models/checkpoints/weights.Ep29-val_loss0.53/' -LFM 1

}

Execute(){
# -- Set the environment
# if first time set env, else activate env.

#Run_NTuple &&
Run_DataProcessing &&
Train 
#Validate
}

Execute


#./src/NTuple && ./src/Array && ./src/CellsToImage && ./src/Train

# -- Train
